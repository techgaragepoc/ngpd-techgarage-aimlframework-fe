import { Observable } from 'rxjs/Observable';
import { Component, OnInit, ViewEncapsulation, OnDestroy, ChangeDetectionStrategy } from '@angular/core';

import { SharedService, AuthenticationService } from './shared/service';
import { User } from './shared/models';
import { Utilities } from './shared/utils';

@Component({
  selector: 'mercer-app',
  templateUrl: './app.component.html',
  styles: ['./app.component.scss', '../styles/styles.scss', '../styles/styles-ie.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})
export class AppComponent implements OnInit {

  constructor(
    private sharedService: SharedService,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    this.getLoggedInUser();
  }

  getLoggedInUser(): void {
    // console.log('app.component: before getLoggedInUser');
    const localUser = this.authService.getUserInfo();
    const isTokenValid = new Utilities().ValidateUserToken(localUser);

    if (isTokenValid) {
      // console.log('getLoggedInUser:' + JSON.stringify(localUser));
      this.sharedService.assignUserInfo(localUser);
    } else {
      this.sharedService.clearUserInfo();
    }
  }
}
