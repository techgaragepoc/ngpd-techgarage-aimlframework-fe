import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Location } from '@angular/common';

import { AuthenticationService, SharedService } from '../shared/service';
import { User } from '../shared/models';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private sharedService: SharedService,
        private authenticationService: AuthenticationService,
        private location: Location) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.getUserInfo();
        let isTokenValid = false;
        let isAdminUser = false;

        // console.log('state', state.toString());

        if (currentUser !== null && currentUser.token) {
            let jwtHelper = new JwtHelper();

            // If the token is expired, this function will return true
            isTokenValid = (jwtHelper.isTokenExpired(currentUser.token) === false);
            isAdminUser = currentUser.admin;

            // console.log('decode token:' + JSON.stringify(jwtHelper.decodeToken(currentUser.token)));

            // clean up the object
            jwtHelper = null;
        }

        // console.log('isTokenValid:' + isTokenValid);
        // console.log(state.url ? '?returnUrl=' + state.url : '');

        // Force logout and redirection to Login Page if token is invalid
        if (!isTokenValid) {
            this.authenticationService.logout();
            this.sharedService.clearUserInfo();

            // console.log(this.location.prepareExternalUrl(state.url || '/'));

            // not signed in so redirect to login page with the return url
            window.location.href = '/login' + (state.url ? '?returnUrl=' + state.url : '');
            // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        } else {
            // check if the specified route is open for access to the logged_in user
            if (isAdminUser === false) {
                const authRoute = this.sharedService.SiteLinks.filter(x => x.url === state.url && x.isPublic);
                // (x.allowNavigation || x.showInHeader === false));

                // console.log('authRoute', authRoute, isAdminUser);

                const accessAllowed = (authRoute !== null && authRoute.length > 0);

                // redirect non admin user to home page
                if (!accessAllowed) {
                    this.router.navigateByUrl('/');
                }

                return accessAllowed;
            }

            return isAdminUser;
        }

        return isTokenValid;
    }
}
