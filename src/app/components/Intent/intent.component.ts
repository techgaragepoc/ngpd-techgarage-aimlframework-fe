﻿import { Component, OnInit, NgModule } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

import { IntentService } from './intent.service';
import { IIntent } from './intents';
import { TextClassificationService, AuthenticationService } from '../../shared/service';

@Component({
    selector: 'app-root',
    templateUrl: './intent.component.html',
})
export class IntentComponent implements OnInit {
    errorMessage: string;
    articles: any;
    JsonTree: any;
    inputIntent: string;
    keys: any;
    IntentScoreModel: any[];
    modelName: string;

    constructor
        (
        private sanitizer: DomSanitizer,
        private route: ActivatedRoute,
        private textClassificationService: TextClassificationService,
        private authService: AuthenticationService
        ) { }

    ngOnInit(): void {
        this.route
            .queryParams
            .subscribe(params => {
                // Defaults to 0 if no query param provided.
                this.modelName = params['modelName'];
                this.inputIntent = params['utterance'];
            });

        // Get POS and other related information for the specified utterance
        this.getIntents();
    }
    get UserName() {
        const userInfo = this.authService.getUserInfo();
        return userInfo != null ? userInfo.username : 'NA';
    }
    getIntents(): void {
        // console.log("Input " + this.inputIntent);
        const self = this;
        this.articles = '';
        this.IntentScoreModel = [];

        self.textClassificationService.getIntentsByUser(this.inputIntent, this.modelName, this.UserName).subscribe
            (
            response => {
                this.articles = JSON.parse(response.message);
                this.keys = Object.keys(this.articles);
                // Object.keys(this.articles).forEach(key => {
                this.keys.forEach(key => {
                    if (key.toLowerCase() === 'intents') {
                        // Array of intents expected
                        const item = this.articles[key];

                        Object.keys(item).forEach(key1 => {
                            // console.log("tesdt", item[key1]);
                            const item1 = item[key1];

                            Object.keys(item1).forEach(key2 => {
                                // const datamodel: any = {};
                                // datamodel.key = key2;
                                // datamodel.value = item1[key2];
                                this.IntentScoreModel.push({ key: key2, value: item1[key2] });
                                // console.log("tesdsdt", item1[key2]);
                            });
                        });
                    }
                });
                console.log('IntentScoreModel', this.IntentScoreModel.sort(function (a, b) {
                    if (a.value > b.value) {
                        return -1;
                    }
                    if (a.value < b.value) {
                        return 1;
                    }
                    return 0;
                }));
                this.IntentScoreModel = this.IntentScoreModel.slice(0, 3);
                // console.log("IntentScoreModel sliced", this.IntentScoreModel);

            },
            error => this.errorMessage = <any>error
            );

        this.getTree();
    }
    public getTree(): void {
        // console.log("Input " + this.inputIntent);
        const self = this;
        this.articles = '';

        self.textClassificationService.getTreeByUser(this.inputIntent, this.modelName, this.UserName).subscribe
            (
            response => {
                this.JsonTree = this.transform(response.toString());
                // console.log("JSonTree", this.JsonTree);
            },
            error => this.errorMessage = <any>error
            );

    }
    transform(html) {
        return this.sanitizer.bypassSecurityTrustHtml(html);
    }
    public getKeys(data) {
        this.keys = Object.keys(data);
        return true;
    }
}
