﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntentComponent } from './intent.component';

const routes: Routes = [{ path: '', component: IntentComponent }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class IntentRoutingModule { }
