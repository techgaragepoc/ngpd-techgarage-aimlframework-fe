﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { MatRippleModule, MatTabsModule } from '@angular/material';

import { IntentRoutingModule } from './intent-routing.module';
import { IntentComponent } from './intent.component';
import { IntentService } from './intent.service';
// import { PageHeaderModule } from '../../shared';

@NgModule({
    imports: [CommonModule, IntentRoutingModule],
    declarations: [],
    providers: [IntentService]
})
export class IntentModule { }
