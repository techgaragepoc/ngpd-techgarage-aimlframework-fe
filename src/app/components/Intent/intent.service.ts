﻿import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';

import {
    Http,
    Headers,
    RequestOptions,
    Response
} from '@angular/http';

@Injectable()
export class IntentService {

    // intentJson:Observable<any>;
    constructor(private http: HttpClient) {
    }

    getJSON(inputIntent: string): Observable<any> {
        return this.http.get('http://usdfw11as667v:5003/api/getPOS/testmodel/' + inputIntent);
    }
}
