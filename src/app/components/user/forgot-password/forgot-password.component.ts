import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, UserService, AuthenticationService } from '../../../shared/service';
import { Utilities } from '../../../shared/utils';

@Component({
    moduleId: module.id,
    templateUrl: 'forgot-password.component.html'
})

export class ForgotPasswordComponent {
    model: any = {};
    loading = false;

    constructor(
        private _router: Router,
        private _userService: UserService,
        private _alertService: AlertService,
        private _authService: AuthenticationService) {

        // const currentUser = this._authService.getUserInfo();
        // this.model.username = currentUser && currentUser.username;
        // this.model.id = currentUser && currentUser._id;
    }

    submitForm() {
        this.loading = true;
        this._alertService.clear();

        this._userService.resetPassword(this.model)
            .subscribe(
            data => {
                this._alertService.success('Password generated successful');
                this._router.navigate(['/login']);
            },
            error => {
                const msg = (error.error && error.error.message ? error.error.message : error.error) || error.message;
                this._alertService.error(msg);
                this.loading = false;
            });
    }

    cancelForm() {
        this._router.navigate(['/login']);
    }
}
