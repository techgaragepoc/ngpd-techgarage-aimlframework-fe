import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, UserService } from '../../../shared/service';

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html'
})

export class RegisterComponent {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService) { }

    doRegister() {
        this.loading = true;
        this.alertService.clear();
        // console.log(JSON.stringify(this.model));

        this.userService.create(this.model)
            .subscribe(
            data => {
                this.alertService.success('Registration successful');
                this.router.navigate(['/login']);
            },
            error => {
                this.alertService.error('Registration failed!!!');
                this.loading = false;
            });
    }

    doCancel() {
        this.router.navigate(['/login']);
    }
}
