import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, UserService, AuthenticationService } from '../../../shared/service';
import { Utilities } from '../../../shared/utils';

@Component({
    moduleId: module.id,
    templateUrl: 'change-password.component.html'
})
export class ChangePasswordComponent {
    model: any = {};
    loading = false;

    constructor(
        // private route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService,
        private _alertService: AlertService,
        private _authService: AuthenticationService) {

        const currentUser = this._authService.getUserInfo();
        this.model.username = currentUser && currentUser.username;
        this.model.id = currentUser && currentUser._id;
    }

    submitForm() {
        this.loading = true;
        // console.log(JSON.stringify(this.model));
        this._userService.changePassword(this.model.id, this.model.currentpassword, this.model.newpassword)
            .subscribe(
            data => {
                this._alertService.success('Password changed successfully');
                this._authService.logout();
                window.location.href = '/';
            },
            error => {
                this._alertService.error(error);
                this.loading = false;
            });
    }

    cancelForm() {
        this._router.navigate(['/']);
    }
}
