import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Observable } from 'rxjs/Observable';

import { AlertService, AuthenticationService, SharedService } from '../../../shared/service';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
    // styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private sharedService: SharedService) { }

    ngOnInit() {
        // reset login status
        this.sharedService.clearUserInfo();
        this.authenticationService.logout();
        // this.alertService.clear();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        // console.log(this.returnUrl);
    }

    login() {
        this.loading = true;
        this.alertService.clear();

        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
            data => {
                // console.log(data);
                if (data && data.success && data.result) {
                    // console.log(JSON.stringify(data));
                    this.alertService.success('Login Successful');
                    this.sharedService.assignUserInfo(data);
                    window.location.href = this.location.prepareExternalUrl(this.returnUrl);
                } else {
                    this.alertService.error('Login failed');
                }

                this.loading = false;
            },
            error => {
                this.loading = false;
                // const errmsg = error && (error.message || error.error.message);
                this.alertService.error('Login failed');
            });
    }

    signUp() {
        this.router.navigateByUrl('/register');
    }

    resetPassword() {
        this.router.navigateByUrl('/resetpassword');
    }
}
