import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mercer-nocontent',
  moduleId: module.id.toString(),
  templateUrl: 'no-content.component.html'
})
export class NoContentComponent implements OnInit {
  slides = [];

  ngOnInit() {
    for (let i = 0; i < 10; i++) {
      this.slides.push(200 + i);
    }
  }
}
