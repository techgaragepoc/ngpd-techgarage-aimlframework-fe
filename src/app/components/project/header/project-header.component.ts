import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy } from '@angular/core';
import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'project-header',
  moduleId: module.id.toString(),
  templateUrl: 'project-header.component.html'
})
export class ProjectHeaderComponent extends MosHeaderComponent implements OnDestroy {

  public mosConfig: MosConfigurationService;

  public userProfileUrl: string = null;
  public userAvatar: string;
  public userFullName: string;
  public userEmail: string;

  userSubscription: Subscription;
  authSubscription: Subscription;

  private isAuthenticated: boolean;

  constructor(mosConfigParam: MosConfigurationService) {
    super();
    this.mosConfig = mosConfigParam;
  }

  ngOnDestroy() {
  }

  isUserAuthenticated(): boolean {
    return false;
  }

  doLogout() {
    //   const result = this._authService.signOutUser().then((res) => {
    //     // console.log('LogOut: ' + res);
    //     if (res) {//} && res.REDIRECTION_URL) {
    //       return Promise.resolve(res);
    //     }
    //   })
    //     .then(url => {
    //       this._sharedService.clearUserInfo();
    //       //window.location.href = url;
    //     });

    window.location.href = '/';
  }

  doLogin() {
    // const result = this._authService.signInUser().then((res) => {
    //   if (res) {//} && res.REDIRECTION_URL) {
    //     window.location.href = res.REDIRECTION_URL;
    //   }
    // });
  }
}
