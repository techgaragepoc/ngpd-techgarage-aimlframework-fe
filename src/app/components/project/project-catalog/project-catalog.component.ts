import { Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnInit } from '@angular/core';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';

import { AlertService, ProjectService, ModelService, SharedService } from '../../../shared/service';
import { Project, Category, ProjectOption, UIOption, IFormData, IProject } from '../../../shared/models';

@Component({
  selector: 'mercer-project-catalog',
  moduleId: module.id.toString(),
  templateUrl: 'project-catalog.component.html'
})
export class ProjectCatalogComponent implements OnInit {

  constructor(
    private router: Router,
    private projService: ProjectService,
    private modelService: ModelService,
    private alertService: AlertService,
    private sharedService: SharedService) { }

  headers: any[] = [
    { name: 'Name', column: 'name' },
    { name: 'Category', column: 'category' },
    { name: 'Description', column: 'description' },
    { name: 'NLP', column: 'nlp' },
    { name: 'Intent Fulfillment', column: 'intentfulfillment' },
    // { name: 'Intent Implementation', column: 'enterpriseconnector' },
    { name: 'Actions', column: 'actions' }
  ];

  allProjects = [];
  filteredProjects = [];

  ngOnInit() {
    // this.getProjOptions();
    this.sharedService.FormData = {} as IFormData;

    this.getProjects();
  }

  // getProjOptions() {
  //   this.projOptions = [];

  //   this.projService.getProjectOptions().subscribe(response => {

  //     if (response && response.success && response.data) {
  //       this.projOptions = response.data;
  //       // console.log(JSON.stringify(this.projOptions));
  //     }
  //   },
  //     error => { this.alertService.error('Unable to get Project Options') });
  // }

  getProjects() {
    let transformedProjs = [];

    this.sharedService.getUserInfo().subscribe(user => {
      // this.projService.getAll().subscribe(response => {
      this.projService.getByUserId(user._id).subscribe(response => {
        if (response && response.success && response.data) {
          transformedProjs = response.data;

          // Transform project model to the desired view model
          this.allProjects = transformedProjs.map(x => {
            return {
              _id: x._id,
              name: x.name,
              category: x.category ? x.category.name : 'NA',
              description: x.description,
              nlp: this.getOptionValue(x.options, 'nlp.selected'),
              intentfulfillment: this.getOptionValue(x.options, 'if.selected'),
              // enterpriseconnector: this.getOptionValue(x.options, 'ec.selected'),
              nlpmodel_id: this.getOptionValue(x.options, 'nlp.ka.values'),
              domain_models: this.getOptionValue(x.options, 'if.domain.values'),
              clickable: user.admin === true // Allow navigation only if the user is admin
            };
          });

          this.filteredProjects = this.allProjects;
          // console.log(JSON.stringify(this.allProjects));
          // console.log(JSON.stringify(this.projects), JSON.stringify(transformedProjs));
        }
      },
        error => {
          this.alertService.error('Unable to get Projects');
        });
    });
    // console.log('Proj Options:' + JSON.stringify(this.projOptions));
  }

  // getOptionIdByName(optionName: string) {
  //   const match = this.projOptions.filter(x => { return x.name === optionName }).map(x => { return x._id });

  //   if (match && match.length === 1) {
  //     return match[0];
  //   }

  //   return null;
  // }

  getOptionValue(optionsData: any[], optionName: string) {
    // const projOptionId = this.getOptionIdByName(optionName);

    // if (projOptionId === null) {
    //   // console.log('not found')
    //   return null;
    // }

    if (optionsData != null && optionsData.length > 0) {
      const optValues = optionsData.filter((v, i, n) => v.id.name === optionName).map(x => x.values);
      // console.log('values:' + optValues.toString(), projOptionId);
      return (optValues != null && optValues.length > 0) ? optValues[0] : null;
    }

    return null;
  }

  goToCreateProject() {
    this.router.navigateByUrl('/createproject');
  }

  goToNext(id, name, nlp, iful, ec, nlpModelId, domainModels) {
    // console.log(id, name, nlp, iful, ec, nlpModelId, domainModels);

    if (nlp) {
      // this.router.navigateByUrl('/nlp');
      this.goToProjectModel(id, name, nlpModelId, domainModels);
    } else if (iful) {
      this.router.navigateByUrl('/intentfulfillment');
      // }
      // else if (ec) {
      //   this.router.navigateByUrl('/enterpriseconnector');
    } else {
      this.router.navigateByUrl('/');
    }
  }

  edit(id, name, type, desc) {
    this.sharedService.FormData = { ProjectForm: { projid: id, projname: name, projtype: type, projDesc: desc } } as IFormData;
    this.goToCreateProject();
  }

  goToProjectModel(id, name, nlpModelId, domainModels) {
    this.sharedService.FormData = {
      ProjectForm: {
        projid: id,
        projname: name,
        nlp_ka_model: nlpModelId,
        if_dom_models: domainModels
      }
    } as IFormData;

    this.router.navigateByUrl('/addutterance');
  }

  deleteProject(id, name, dialog) {
    this.alertService.clear();

    this.projService.delete(id).subscribe(data => {
      this.alertService.success('Project deleted successfully');
      this.getProjects();
    },
      error => {
        this.alertService.error('Error deleting project');
      });

    dialog.close();
  }

  searchProjects(text) {
    if (text == null || text.length === 0 || text.length < 5) {
      this.filteredProjects = this.allProjects;
      return;
    }

    this.filteredProjects = this.allProjects.filter(x => x.name
      .toString()
      .toLowerCase()
      .indexOf(text.toString().toLowerCase()) !== -1);
  }
}
