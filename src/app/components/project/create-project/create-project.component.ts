import { Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';

import { AlertService, ProjectService, ModelService, SharedService, NlpService, CommonService } from '../../../shared/service';
import { Project, Category, ProjectOption, UIOption, IFormData, IProject } from '../../../shared/models';
import { Utilities } from '../../../shared/utils';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';

@Component({
  selector: 'mercer-create-project',
  moduleId: module.id.toString(),
  templateUrl: 'create-project.component.html'
})
export class CreateProjectComponent implements OnInit, AfterViewInit {
  model: any = {};
  predefinedUiTypes: any[] = [
    { value: 'mslync', name: 'mslync', displayname: 'Microsoft Lync' },
    { value: 'webchat', name: 'webchat', displayname: 'Webchat Interface' },
  ];

  predefinedModels: any[];
  predefinedKnowledgeAreas: any[];
  predefinedCultures: any[];

  predefinedCategories: Category[];

  projOptions: ProjectOption[];
  projectData: any[];
  isEditing: boolean;

  projectGroup: FormGroup;
  catAddGroup: FormGroup;

  constructor(
    private router: Router,
    private projService: ProjectService,
    private modelService: ModelService,
    private nlpService: NlpService,
    private alertService: AlertService,
    private sharedService: SharedService,
    private commonService: CommonService,
    private changeDetectorRef: ChangeDetectorRef) {

    this.projectGroup = new FormGroup({
      projectName: new FormControl(null, Validators.required),
      projectType: new FormControl(null, Validators.required),
      // subcategory: new FormControl(null, Validators.required),
      // version: new FormControl(null, Validators.required),
      // provider: new FormControl(null, Validators.required),
      // sytemDefined: new FormControl(null, null),
    });

    this.catAddGroup = new FormGroup({
      catName: new FormControl(null, Validators.required)
    });
  }

  ngOnInit() {
    this.isEditing = false;

    const formData = this.sharedService.FormData;

    if (formData && formData.ProjectForm) {
      this.model.projectName = formData.ProjectForm.projid;
      this.model.projectType = formData.ProjectForm.projtype;
      this.model.projectId = formData.ProjectForm.projid;
      this.model.projectDesc = formData.ProjectForm.projDesc;
      this.isEditing = true;
    }

    this.model.proj = {};
    this.model.proj.mode = (this.isEditing) ? 'editproj' : 'newproj';

    this.model.nlp = {};
    this.model.nlp.context = {};
    this.model.nlp.sentiments = {};
    this.model.nlp.feedback = {};
    this.model.nlp.ui = {};
    // this.model.nlp.ui.type = 'mslync';
    this.model.nlp.ka = {};
    this.model.nlp.new = {};

    this.model.if = {};
    this.model.if.domain = {};
    this.model.if.domain.values = [];
    this.model.ec = {};

    this.populateDomainModels();
    this.populateKnowledgeAreas();
    this.populateCategories();
    this.populateCultures();

    this.getProjOptions();
    this.getAllUserProjects();
  }

  ngAfterViewInit() {
    if (this.isEditing) {
      // console.log('editing', this.model.projectName);
      this.setFormValues(this.model.projectId);
    }
  }

  getAllUserProjects() {
    this.projectData = [];
    let transformedProjs = [];

    this.sharedService.getUserInfo().subscribe(user => {
      // this.projService.getAll().subscribe(response => {
      this.projService.getByUserId(user._id).subscribe(response => {

        if (response && response.success && response.data) {
          transformedProjs = response.data;

          // Transform project model to the desired view model
          this.projectData = transformedProjs.map(proj => {
            return {
              _id: proj._id,
              name: proj.name,
              category: proj.category._id
            };
          });

          // console.log('all projs', this.projectData);
        }
      },
        error => {
          this.alertService.error('Unable to get Projects');
        });
    });
    // console.log('Proj Options:' + JSON.stringify(this.projOptions));
  }

  setFormValues(id: string) {
    this.projService.getById(id).subscribe(response => {

      if (response && response.success && response.data) {
        const projData = response.data;

        this.model.projectDesc = projData.description;
        this.model.projectType = projData.category.name;

        // console.log('Proj Data', JSON.stringify(projData));

        this.model.nlp.selected = this.getOptionValue(projData.options, 'nlp.selected');
        this.model.nlp.ui.selected = this.getOptionValue(projData.options, 'nlp.ui.selected');
        this.model.nlp.ui.type = this.getOptionValue(projData.options, 'nlp.ui.type');
        // console.log('this.model.nlp.ui.type', this.model.nlp.ui.type);

        this.model.nlp.context.selected = this.getOptionValue(projData.options, 'nlp.context.selected');
        this.model.nlp.sentiments.selected = this.getOptionValue(projData.options, 'nlp.sentiments.selected');
        this.model.nlp.feedback.selected = this.getOptionValue(projData.options, 'nlp.feedback.selected');

        this.model.nlp.ka.selected = this.getOptionValue(projData.options, 'nlp.ka.selected');

        this.model.nlp.ka.values = this.getOptionValue(projData.options, 'nlp.ka.values');
        // console.log('this.model.nlp.ka.values', this.model.nlp.ka.values);
        // const ka_values = this.getOptionValue(projData.options, 'nlp.ka.values');
        // this.setCheckedValues(this.predefinedKnowledgeAreas, ka_values);

        this.model.if.selected = this.getOptionValue(projData.options, 'if.selected');

        const domain_values = this.getOptionValue(projData.options, 'if.domain.values');
        this.model.if.domain.values = domain_values;
        // console.log('domain_values',JSON.stringify(domain_values));
        // this.setCheckedValues(this.predefinedModels, domain_values);

        this.model.ec.selected = this.getOptionValue(projData.options, 'ec.selected');
        this.changeDetectorRef.detectChanges();
      }
    },
      error => { this.alertService.error('Unable to get Project details'); });
  }

  setCheckedValues(data: any[], selected: any[]) {
    if (data === null || data.length === 0) {
      return;
    }

    if (selected === null || selected.length === 0) {
      return;
    }

    const utils = new Utilities();
    // console.log('setCheckedValues', utils.IsArray(selected[0]), utils.IsArray(selected), JSON.stringify(selected), selected.length);

    data.forEach((v, i, a) => {
      // array of arrays
      // if (utils.IsArray(selected[0])) {
      if (utils.IsArray(selected)) {
        // for (let index = 0; index < selected[0].length; index++) {
        for (let index = 0; index < selected.length; index++) {
          // console.log('values:', v._id, selected[0][index]);

          // if (selected[0][index].toString() === v._id.toString()) {
          if (selected[index].toString() === v._id.toString()) {
            // console.log('matched', v._id)
            v.checked = true;
          }
        }
      } else {
        // single value
        if (selected.toString() === v._id.toString()) {
          // console.log('matched single', v._id)
          v.checked = true;
        }
      }
    });
  }

  populateDomainModels() {
    this.predefinedModels = [];

    this.modelService.getAll().subscribe(models => {
      // console.log('found:' + JSON.stringify(response));
      if (models && models.success && models.data) {
        this.predefinedModels = models.data.map((v, i, a) => ({ _id: v._id, name: v.name, checked: false }));
      }

      // console.log(JSON.stringify(this.predefinedModels));
    },
      error => { this.alertService.error('Unable to get Categories'); });
  }

  populateKnowledgeAreas() {
    // this.predefinedKnowledgeAreas = [
    //   { _id: 1, name: 'Finance FAQs', checked: false },
    //   { _id: 2, name: 'Enterprise Reporting', checked: false },
    //   { _id: 3, name: 'Workday (Leave)', checked: false }
    // ];

    this.sharedService.getUserInfo().subscribe(user => {
      this.nlpService.getByUserId(user._id).subscribe(langModels => {
        // console.log('found:' + JSON.stringify(langModels));
        if (langModels) {
          this.predefinedKnowledgeAreas = langModels.map((v, i, a) =>
            (
              { _id: v._id, name: v.name, checked: false, sysdefined: v.sysdefined }
            )
          );
        }
      },
        error => {
          this.alertService.error('Unable to get Categories');
        });
    });
  }

  populateCategories() {
    this.alertService.clear();
    this.predefinedCategories = [];

    this.projService.getCategories().subscribe(response => {
      // console.log('found:' + JSON.stringify(response));
      if (response && response.success && response.data) {
        this.predefinedCategories = response.data;
      }
    },
      error => { this.alertService.error('Unable to get Categories'); });
  }

  populateCultures(): void {
    this.commonService.getAllCultures().subscribe(data => {
      this.predefinedCultures = data;
    },
      error => { this.alertService.error('Unable to get Cultures'); }
    );
  }

  getProjOptions() {
    this.projOptions = [];

    this.projService.getProjectOptions().subscribe(response => {

      if (response && response.success && response.data) {
        this.projOptions = response.data;
        // console.log(JSON.stringify(this.projOptions));
      }
    },
      error => { this.alertService.error('Unable to get Project Options'); });

    // console.log('Proj Options:' + JSON.stringify(this.projOptions));
  }

  getOptionByName(optionName: string) {
    // console.log(this.projOptions && this.projOptions.length > 0);
    if (this.projOptions && this.projOptions.length > 0) {
      for (let i = 0; i < this.projOptions.length; i++) {
        if (this.projOptions[i].name === optionName) {
          return this.projOptions[i];
        }
      }
    }

    return null;
  }

  getOptionValue(optionsData: any[], optionName: string) {
    const projOption = this.getOptionByName(optionName);

    // Validate the proj option
    if (projOption && optionsData && optionsData.length > 0) {
      const optValues = optionsData.filter((v, i, n) => v.id.name === optionName).map(x => x.values);

      // it returns an array of arrays
      if (optValues && optValues.length > 0) {
        const values = [];

        optValues[0].forEach(x => values.push(x.toString()));

        // console.log('getOptionValue:', optionName, values, values.length, projOption.multichoice);
        return (projOption.multichoice ? values : values[0]);
      }

      // If values are supplied, then check if option allows multiple values
      // if (optValues && optValues.length > 0) {
      //   return (projOption.multichoice ? optValues : optValues[0]);
      // }

    }

    return null;
  }

  validateUIOption(optName: string, values: any[]) {
    const opt = this.getOptionByName(optName);
    let value: any;
    let isValidValue = true;

    // If matching option is found
    if (opt) {
      // Check if the option allows multiple values
      if (opt.multichoice === false) {
        value = [values[0]];
      } else {
        value = values;
      }

      switch (opt.datatype.toLowerCase()) {

        case 'boolean': value.forEach((val, index, arr) => {
          isValidValue = isValidValue && (val === true || val === 'true' || val === false || val === 'false');
          // console.log(value, isValidValue);
          return isValidValue;
        });
          break;

        case 'string': value = value.map((val, index, arr) => val); break;
        // case 'integer' value.forEach((value, index, arr) => {
        //   isValidValue = isValidValue && Number(value);
        //   return isValidValue;
        // });
        // break;
      }

      // console.log('value:', value);
      // return {
      //   _id: opt._id,
      //   name: opt.name,
      //   datatype: opt.datatype,
      //   multichoice: opt.multichoice,
      //   modified: new Date(),
      //   values: (isValidValue ? value : null)
      // } as ProjectOption;

      return {
        id: opt._id,
        values: (isValidValue ? value : null)
      } as UIOption;
    }

    return null;
  }

  saveForm() {
    const formMode = this.model.proj.mode;
    const editing = (formMode === 'editproj');
    const transferData = {} as IFormData;
    transferData.ProjectForm = {} as IProject;

    const uiOptions: UIOption[] = [];
    let projOption: UIOption;

    // NLP checked
    if (this.model.nlp.selected) {
      transferData.ProjectForm.nlp_selected = true;

      projOption = this.validateUIOption('nlp.selected', [this.model.nlp.selected]);

      if (projOption != null && projOption.values != null) {
        uiOptions.push(projOption);
      }

      // UI checked
      if (this.model.nlp.ui.selected) {
        transferData.ProjectForm.nlp_ui_selected = true;

        projOption = this.validateUIOption('nlp.ui.selected', [this.model.nlp.ui.selected]);

        if (projOption != null && projOption.values != null) {
          uiOptions.push(projOption);
        }

        // UI Type selected
        if (this.model.nlp.ui.type) {
          transferData.ProjectForm.nlp_ui_type = this.model.nlp.ui.type;

          projOption = this.validateUIOption('nlp.ui.type', [this.model.nlp.ui.type]);

          if (projOption != null && projOption.values != null) {
            uiOptions.push(projOption);
          }
        }
      }

      // NLP context selected
      if (this.model.nlp.context.selected) {
        transferData.ProjectForm.nlp_conv_context_selected = true;

        projOption = this.validateUIOption('nlp.context.selected', [this.model.nlp.context.selected]);

        if (projOption != null && projOption.values != null) {
          uiOptions.push(projOption);
        }
      }

      // NLP sentiments analysis selected
      if (this.model.nlp.sentiments.selected) {
        transferData.ProjectForm.nlp_sentiments_selected = true;

        projOption = this.validateUIOption('nlp.sentiments.selected', [this.model.nlp.sentiments.selected]);

        if (projOption != null && projOption.values != null) {
          uiOptions.push(projOption);
        }
      }

      // NLP feedback selected
      if (this.model.nlp.feedback.selected) {
        transferData.ProjectForm.nlp_feedback_selected = true;

        projOption = this.validateUIOption('nlp.feedback.selected', [this.model.nlp.feedback.selected]);

        if (projOption != null && projOption.values != null) {
          uiOptions.push(projOption);
        }
      }

      // NLP Knowledge Area selected
      if (this.model.nlp.ka.selected) {
        transferData.ProjectForm.nlp_ka_selected = true;

        projOption = this.validateUIOption('nlp.ka.selected', [this.model.nlp.ka.selected]);

        if (projOption != null && projOption.values != null) {
          uiOptions.push(projOption);
        }

        // Get selected values from the Knowledge Area list
        // const selectedKA = this.predefinedKnowledgeAreas.filter((v, i, a) => v.checked).map((v, i, a) => v._id);
        // projOption = this.validateUIOption('nlp.ka.values', selectedKA);
        projOption = this.validateUIOption('nlp.ka.values', [this.model.nlp.ka.values]);

        if (projOption != null && projOption.values != null) {
          transferData.ProjectForm.nlp_ka_model = this.model.nlp.ka.values;
          uiOptions.push(projOption);
        }
      }
    }

    // Intent Fulfillment selected
    if (this.model.if.selected) {
      transferData.ProjectForm.if_selected = true;

      projOption = this.validateUIOption('if.selected', [this.model.if.selected]);

      if (projOption != null && projOption.values != null) {
        uiOptions.push(projOption);
      }

      // Get selected values from the NLP models list
      // const selectedModels = this.predefinedModels.filter((v, i, a) => v.checked).map((v, i, a) => v._id);
      const selectedModels = this.model.if.domain.values;
      projOption = this.validateUIOption('if.domain.values', selectedModels);

      if (projOption != null && projOption.values != null) {
        transferData.ProjectForm.if_dom_models = selectedModels;
        uiOptions.push(projOption);
      }
    }

    // Enterprise Connector selected
    if (this.model.ec.selected) {
      transferData.ProjectForm.ec_selected = true;

      projOption = this.validateUIOption('ec.selected', [this.model.ec.selected]);

      if (projOption != null && projOption.values != null) {
        uiOptions.push(projOption);
      }
    }

    const formData =
      {
        _id: editing ? this.model.projectId : null,
        name: editing ? null : this.model.projectName.toString().trim(),
        category: this.model.projectType,
        description: this.model.projectDesc,
        options: uiOptions
      } as Project;

    // if the user info is available, map user id
    this.sharedService.getUserInfo().subscribe(x => { formData.updatedBy = x._id; });
    this.alertService.clear();
    // console.log('formData', JSON.stringify(formData));

    if (formData) {
      transferData.ProjectForm.projname = formData.name;
      transferData.ProjectForm.projtype = formData.category;
      transferData.ProjectForm.projDesc = formData.description;

      if (editing) {
        this.projService.update(formData).subscribe(
          data => {
            this.alertService.success('Project Info updated successfully');
            this.router.navigate(['/projectcatalog']);
          },
          error => {
            this.alertService.error('Project Info Update failed!!!');
          });
      } else {
        this.projService.create(formData).subscribe(
          data => {
            this.alertService.success('Project created successfully');
            this.router.navigate(['/projectcatalog']);
          },
          error => {
            this.alertService.error(error.error.message);
          });
      }
    }

    // console.log('saveForm', JSON.stringify(formData));
    return formData;
  }

  goToProjectCatalog() {
    this.router.navigateByUrl('/projectcatalog');
  }

  // toggleOptions(checked, evt) {
  toggleOptions(evt) {
    const controlName = evt.currentTarget.name;
    const checked = evt.currentTarget.checked;

    // console.log(controlName, checked);
    // console.log(this.model.nlp.uiType, this.model.nlp.modelType);

    // unselect the radio choices on uncheck
    if (checked === false) {
      switch (controlName.toString().toLowerCase()) {
        case 'chkim':
          // this.model.nlp.modelType = null;
          this.model.nlp.ka.values = '';
          // this.predefinedKnowledgeAreas.forEach(x => x.checked = false);
          break;

        case 'chkconv':
          this.model.nlp.ui.type = null;
          // this.model.nlp.context.selected = false;
          // this.model.nlp.sentiments.selected = false;
          // this.model.nlp.feedback.selected = false;

          break;

        case 'nlp':
          this.model.nlp.ui.selected = false;
          this.model.nlp.ui.type = null;

          this.model.nlp.context.selected = false;
          this.model.nlp.sentiments.selected = false;
          this.model.nlp.feedback.selected = false;

          this.model.nlp.ka.selected = false;
          this.model.nlp.ka.values = '';
          this.model.nlp.modelType = null;

          // this.predefinedKnowledgeAreas.forEach(x => x.checked = false);
          break;

        case 'chkif':
        case 'chkdomain':
          this.model.if.domain.selected = null;
          this.predefinedModels.forEach(x => x.checked = false);
          break;
      }
    }

    // if (checked == false) {
    //   this.model.modelType = null;
    //   this.model.nlp.chkConv = null;
    // }
    // console.log(checked, evt.toString());
  }

  toggleSelections(evt) {
    const controlName = evt.currentTarget.name;
    const value = evt.currentTarget.value;

    // console.log(controlName, value);
    switch (controlName.toLowerCase()) {
      case 'projectname':
        this.alertService.clear();
        this.setFormValues(value);
        break;
      case 'projmode':
        this.isEditing = (value === 'editproj');
        this.model.proj.mode = value;

        if (this.isEditing === false) {
          // console.log('not editing');
          this.resetViewModel();
        }

        break;
    }
  }

  addCategory() {
    this.model.proj.newcategory = true;
  }

  saveCategory() {
    const catName = this.catAddGroup.get('catName').value;

    this.alertService.clear();

    if (new Utilities().IsNull(catName) || new Utilities().IsEmpty(catName)) {
      this.alertService.error('Category Name cannot be left blank');
      return;
    }

    // const catItems = this.predefinedCategories.length;
    const cat = { name: this.model.catName } as Category;

    // if the user info is available, map user id
    this.sharedService.getUserInfo().subscribe(x => { cat.updatedBy = x._id; });

    this.projService.createCategory(cat).subscribe(
      result => {
        if (result.success) {
          this.alertService.success(result.data);
          this.cancelAddCategory();
          this.populateCategories();
        } else {
          this.alertService.error(result.message);
        }
      },
      error => {
        this.alertService.error(error.error.message);
      });
  }

  cancelAddCategory() {
    this.alertService.clear();
    this.model.proj.newcategory = false;
    this.model.catName = null;
  }

  resetViewModel() {
    this.model.projectName = null;
    this.model.projectType = null;
    this.model.projDesc = null;
    this.model.catName = null;

    this.model.nlp.selected = null;
    this.model.nlp.ui.selected = null;
    this.model.nlp.ui.type = null;

    this.model.nlp.context.selected = null;
    this.model.nlp.sentiments.selected = null;
    this.model.nlp.feedback.selected = null;

    this.model.nlp.ka.selected = null;
    this.model.nlp.ka.values = '';
    // this.predefinedKnowledgeAreas.forEach((v, i, a) => { v.checked = false; });

    this.model.if.selected = null;
    this.predefinedModels.forEach((v, i, a) => { v.checked = false; });

    this.model.ec.selected = null;
  }

  addModel(dialog) {
    if (!dialog) { return; }

    this.model.nlp.new = {};
    this.model.nlp.new.shared = false;
    this.model.nlp.new.sysdefined = false;

    dialog.open();
  }

  public addNlpLanguageModel(infoExample: any): void {
    this.alertService.clear();

    // if the user info is available, map user id
    this.sharedService.getUserInfo().subscribe(x => {
      this.model.nlp.new.updatedBy = x._id;
      this.model.nlp.new.userName = x.username;
    });

    console.log('this.model.nlp.new', JSON.stringify(this.model.nlp.new));

    this.nlpService.createNlpLanguageModel(this.model.nlp.new).subscribe(data => {
      if (data && data.success) {
        this.alertService.success(data.message);
        this.populateKnowledgeAreas();
      }
    },
      error => {
        this.alertService.error(error.error.message);
      });

    infoExample.close();
  }
}
