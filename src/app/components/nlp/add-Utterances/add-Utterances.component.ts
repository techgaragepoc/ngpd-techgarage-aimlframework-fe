import {
  Component, OnInit, ViewEncapsulation,
  ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit, Input
} from '@angular/core';

import { DomSanitizer } from '@angular/platform-browser';

import { DataSource } from '@angular/cdk/collections';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';
import { PaginationInfo, IFormData, IProject } from '../../../shared/models';
import { Utilities } from '../../../shared/utils';

import {
  AlertService,
  AuthenticationService,
  SharedService,
  TextClassificationService,
  IntentModelDataService,
  NlpService
} from '../../../shared/service/index';

import { IntentCategory, IntentCategoryUtterance } from '../../../shared/models/Intents';

@Component({
  selector: 'mercer-add-utterance',
  templateUrl: './add-Utterances.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})

export class AddUtteranceComponent implements OnInit {
  selectedText = '';
  selectedRow: -1;
  startindex: number;
  endindex: number;
  utteranceTextMain: string;
  modalPopUpClose: any;

  preIntentName: string;
  errorMessage: string;
  IntentName = '';
  intentData: IntentCategoryUtterance;
  modelName: string;
  data: any;
  modelExists: false;
  isEditing = false;

  articles: any;
  JsonTree: any;
  keys: any;
  IntentScoreModel: any[];
  isTrained = false;
  intentClicked = false;

  public model: any = {};
  public modelIntents: any[];
  public uttScores: any[];

  public stripedheaderData: any[] = [
    { column: 'IntentClassification', name: 'Intent Classification' },
    { column: 'POS', name: 'Parts of Speech' },
    { column: 'Utterances', name: 'Utterances' },
    { column: 'CategoryName', name: 'Intent (Score)' },
    { column: 'Fulfilled', name: 'Fulfilled (Yes/No)' }
  ];

  public stripedrowData: any[] = [];

  // @Input() contextMenu: ContextMenuComponent;
  public paginationInfo: PaginationInfo = {
    offset: 0,
    limit: 10,
    limits: [2, 5, 10, 20, 30],
    totalCount: 10
  };

  public items = [
    { name: 'John', otherProperty: 'Foo' },
    { name: 'Joe', otherProperty: 'Bar' }
  ];

  public IntentCategorys: any;
  public IntentUtteranceCategorys: any;
  // route: ActivatedRoute;
  public childMessage = 'utterances';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private textClassificationService: TextClassificationService,
    private ModelDataService: IntentModelDataService,
    private alertService: AlertService,
    private sharedService: SharedService,
    private authService: AuthenticationService,
    private nlpService: NlpService,
    private changeDetectorRefs: ChangeDetectorRef,
    private sanitizer: DomSanitizer
  ) {
    this.model.utterances = [{ entities: [{}] }];
    this.model.edited = {};
  }

  ngOnInit(): void {
    const formData = this.sharedService.FormData;
    const projForm = (this.sharedService.FormData) ? this.sharedService.FormData.ProjectForm : null;

    this.preIntentName = (formData && formData.TextClassificationForm) ? formData.TextClassificationForm.intentname : 'none';
    const modelId = (projForm) ? projForm.nlp_ka_model : null;
    const projectName = (projForm) ? projForm.projname : null;

    // if (this.preIntentName === null || this.preIntentName.trim().length === 0) {
    //   this.preIntentName = 'none';
    // }
    this.model.projectName = projectName;
    this.model.CategoryName = this.preIntentName;
    this.modelName = (formData && formData.TextClassificationForm) ? formData.TextClassificationForm.modelname : null;

    // console.log(formData);
    const utils = new Utilities();
    const loggedUser = this.authService.getUserInfo();

    if (this.modelName != null && this.modelName.length > 0) {
      this.nlpService.getByName(this.modelName).subscribe(model => {
        if (model) {
          this.isTrained = model.istrained;
          this.model.modelId = model._id;
          this.model.editable = utils.isEditingAllowed(model, loggedUser);
          this.populateIntents(this.modelName);
          this.getUtterances(this.modelName, this.preIntentName);
        }
      },
        error => {
          this.isTrained = false;
        });
    } else if (modelId != null) {
      this.nlpService.getById(modelId).subscribe(model => {
        if (model) {
          this.isTrained = model.istrained;
          this.modelName = model.name;
          this.model.modelId = model._id;
          this.model.editable = utils.isEditingAllowed(model, loggedUser);
          this.populateIntents(this.modelName);
          this.getUtterances(this.modelName, this.preIntentName);
        }
      },
        error => {
          this.isTrained = false;
        });
    }

    // utils = null;
    // this.nlpService.getByName(this.modelName).subscribe(model => {
    //   if (model) {
    //     this.isTrained = model.istrained;
    //   }
    // },
    //   error => {
    //     this.isTrained = false;
    //   });

    // this.uttScores = [{
    //   'text': 'testing one step utt creation',
    //   'scores': [
    //     { "ent1": 0.123 },
    //     { "ent2": 0.877 },
    //   ]
    // }];

    // console.log(JSON.parse("[{\"test1\":0.0006170056876726449,\"test111\":0.06982064247131348}]"));

    // console.log(this.getUtteranceScores('testing one step utt creation'));
    // Not working need to cjeck
    // this.getIntents();

    // this.populateIntents(this.modelName);
    // this.getUtterances(this.modelName, this.preIntentName);
  }

  get UserName() {
    const userInfo = this.authService.getUserInfo();
    return userInfo != null ? userInfo.username : 'NA';
  }

  // getIntent(modelName, utteranceText) {
  // window.open('./intent?modelName=' + modelName + '&utterance=' + utteranceText, "_intent");
  // }

  // public getIntents(): void {
  //   // console.log('Test Category');
  //   this.textClassificationService.getByModel(this.modelName).subscribe(
  //     result => {
  //       // console.log(result)
  //       this.IntentCategorys = result;
  //       this.data = result;
  //       // console.log('this.IntentCategorys', this.IntentCategorys);
  //     },
  //     error => this.errorMessage = <any>error
  //   );
  // }

  public updatePage(model) {
    this.paginationInfo.limit = model.limit;
    this.paginationInfo.offset = model.offset;
    if (model.offset === 0) {
      this.stripedrowData = this.IntentUtteranceCategorys.slice(0, model.limit);
    } else {
      this.stripedrowData = this.IntentUtteranceCategorys.slice(model.offset, model.offset + model.limit);
    }
  }
  public getUtterances(modelName, catName): void {
    // this.textClassificationService.getIntentUtterances(modelName, catName).subscribe(
    this.textClassificationService.getUtterancesByModel(modelName).subscribe(
      result => {
        // console.log('result', JSON.stringify(result.data));

        if (result.success) {
          this.IntentCategorys = result.data;
          const filteredData = result.data.filter(x => typeof x.Entity === 'undefined').map(x => {
            return {
              _id: x._id,
              UtteranceText: x.UtteranceText,
              scores: this.getUtteranceScores(x.TrainingScores),
              CategoryName: x.Category.CategoryName,
              CategoryId: x.Category._id,
              POS: this.preparePOS(x.POS),
              ScoreMismatch: false
            };
          });

          // console.log('result', JSON.stringify(filteredData));
          filteredData.forEach(item => {
            item.ScoreMismatch = false;

            if (item.scores != null) {
              const lowScore = item.scores.filter(x => item.CategoryName === x.key && x.index > 0);
              item.ScoreMismatch = (lowScore != null && lowScore.length > 0);
            }
          });

          this.IntentUtteranceCategorys = filteredData;
          // this.stripedrowData = filteredData;
          this.stripedrowData = filteredData.slice(0, this.paginationInfo.limit);

          this.paginationInfo = {
            offset: 0,
            limit: this.paginationInfo.limit,
            limits: this.paginationInfo.limits,
            totalCount: filteredData.length
          };

          this.changeDetectorRefs.detectChanges();
        }
      },
      error => this.errorMessage = <any>error
    );
  }

  isNullOrEmpty(val: String) {
    return (val == null || val === '' || val === 'undefined');
  }

  private getEntities(utteranceText) {
    if (this.IntentCategorys == null || this.IntentCategorys.length === 0) { return; }

    // console.log('ic', JSON.stringify(this.IntentCategorys));

    const result = this.IntentCategorys
      .filter(x => x.UtteranceText.toString() === utteranceText.toString() && x.Entity != null)
      .map(x => x.Entity);
    // console.log('utt', utteranceText, JSON.stringify(result));
    return result;
  }

  private validateUtterance(model): boolean {
    let isModelValid = true;
    this.errorMessage = '';
    if (this.model != null) {
      if (this.isNullOrEmpty(model.UtteranceText)) {
        this.alertService.error('Utterance cannot be blank');
        this.errorMessage = 'Utterance cannot be blank';
        isModelValid = false;
      }
    }
    return isModelValid;
  }

  public addIntentUtterance(parentDialog, optionsDialog, categoryName): void {
    const intentName = this.model.UtteranceText;
    this.model.ModelName = this.modelName;
    this.model.CategoryName = categoryName;

    this.alertService.clear();

    if (this.validateUtterance(this.model)) {
      this.textClassificationService.addIntentUtterance(this.model).subscribe(data => {

        if (data.success) {
          this.alertService.success(data.data);
          this.getUtterances(this.model.ModelName, this.model.CategoryName);
          this.isTrained = false;
        } else {
          this.alertService.error(data.data);
        }
      },
        error => {
          this.alertService.error(error.error.message);
        });

      optionsDialog.close();
      parentDialog.close();
    }
  }

  public createEntity(infoExample1: any): void {
    /* this.route
     .queryParams
     .subscribe(params => {
       // Defaults to 0 if no query param provided.
       this.preIntentName = params['page'];
       this.modelName = params['modelName'];
     }); */
    // this.preIntentName = localStorage.getItem('intentName');
    // this.modelName = localStorage.getItem('modelName');

    const intentName = this.utteranceTextMain;

    this.model.Utterance = intentName;
    this.model.selectedText = this.selectedText;
    this.model.startIndex = this.startindex;
    this.model.endIndex = this.endindex + 1;
    this.model.A = intentName;

    // console.log(intentName);

    // this.model.CategoryName = this.preIntentName;
    // this.modelName = 'Rakeshtestmodel'

    this.alertService.clear();

    this.textClassificationService.createEntity(this.model, this.modelName).subscribe(result => {
      if (result.success) {
        this.alertService.success(result.data);
        this.isTrained = false;
        this.model.EntityName = '';
        this.selectedText = '';

        this.getUtterances(this.modelName, this.model.CategoryName);
      } else {
        this.alertService.error(result.data);
      }
    },
      error => {
        this.alertService.error(error.error.message);
      });

    infoExample1.close();
  }

  public trainModel(): void {
    // this.route.queryParams.subscribe(params => {
    //   // Defaults to 0 if no query param provided.
    //   this.preIntentName = params['page'];
    // });

    // console.log(this.model);

    // let intentName = this.model.UtteranceText;
    // this.model.CategoryName = this.preIntentName;

    // let trainModelString = '[';

    // this.IntentUtteranceCategorys.forEach(function (value) {
    //   console.log('(' + value.UtteranceText + ',' + value.CategoryName + ')');
    //   trainModelString += '(\'' + value.UtteranceText + '\',\'' + value.CategoryName + '\'),';
    // });

    // trainModelString = trainModelString.substring(0, trainModelString.length - 1);
    // trainModelString += ']';

    // //this.modelName = 'rakeshtestmodel';
    // console.log(this.modelName);
    // console.log('trainModelString' + trainModelString);

    const loggedUser = this.authService.getUserInfo();
    const userName = loggedUser ? loggedUser.username : 'NA';

    // console.log('username', userName);
    this.textClassificationService.trainEntireModel(this.modelName, userName).subscribe(data => {
      if (data && data.success) {
        this.isTrained = true;
        this.alertService.clear();
        this.alertService.success('Model trained successfully');
      }
    },
      error => {
        this.alertService.clear();
        this.alertService.error(error.error.message);
      });
  }

  public testModel(): void {
    // console.log('Test', this.modelName);
    this.router.navigate(['/intent'], { queryParams: { modelName: this.modelName } });
  }

  showSimilarUtterances(utteranceText, modalDialog) {
    this.model.UtteranceText = utteranceText;
    modalDialog.open();
  }

  showSelectedText(oField, infoExample1, rowId, categoryName) {
    // alert(oField);
    this.modalPopUpClose = infoExample1;
    let text = '';
    this.utteranceTextMain = oField;
    if (window.getSelection) {
      // alert('Inside If');
      this.selectedText = window.getSelection().toString().trim();
      if (this.selectedText !== '') {
        text = window.getSelection().toString().trim();
        const intentName = this.model.UtteranceText;
        const startIndex = oField.indexOf(text);
        this.startindex = startIndex;

        const endIndex = startIndex + text.length - 1;
        this.endindex = endIndex;
        // alert(this.endindex);
        // alert(this.startindex);
      }
    } else if (document['selection'] && document['selection'].type !== 'Control') {
      // alert('Outside If');
      text = document['selection'].createRange().text;
    }
    this.model.CategoryName = categoryName;
    this.selectedText = text;
    this.selectedRow = rowId;
  }

  deleteUtterance(_id, modelName, dialog) {
    if (!dialog) { return; }
    this.alertService.clear();

    this.textClassificationService.deleteIntentUtterance(_id, modelName).subscribe(data => {
      if (data) {
        switch (data.success) {
          case true: {
            this.alertService.success(data.data);
            this.isTrained = false;
            this.getUtterances(this.modelName, this.preIntentName);
            break;
          }

          case false: this.alertService.error(data.data); break;
        }
      }
    },
      error => {
        this.alertService.error(error.error.message);
      });

    dialog.close();
  }

  populateIntents(modelName) {
    this.textClassificationService.getByModel(this.modelName).subscribe(
      result => {
        this.modelIntents = result.map(x => ({ _id: x._id, CategoryName: x.CategoryName, selected: false }));

        this.modelIntents.forEach((v, i, a) => {
          if (i == 0) {
            this.model.intentName = v.CategoryName;
          }
        });

        // console.log('this.modelIntents', JSON.stringify(this.modelIntents));
      },
      error => this.errorMessage = <any>error
    );
  }

  addIntent() {
    this.model.newIntent = true;
  }

  cancelAddIntent() {
    this.model.newIntent = false;
    this.model.intent = null;
    // this.model.intentName = null;
  }

  saveIntent() {
    // const catItems = this.predefinedCategories.length;
    const intent = { CategoryName: this.model.intent, ModelName: this.modelName, _Id: '' };

    this.alertService.clear();

    this.textClassificationService.createIntent(intent).subscribe(
      result => {
        if (result.success) {
          this.alertService.success(result.data);
          this.cancelAddIntent();
          this.populateIntents(this.modelName);
        } else {
          this.alertService.error(result.message);
        }
      },
      error => {
        this.alertService.error(error.error.message);
      });
  }

  onTrainedClick(result: any) {
    // console.log('onTrainedClick($event)', JSON.stringify(data));

    if (result.success === 'true' || result.success === true) {
      this.textClassificationService.updateUtteranceScores(this.modelName, result.data.utterances).subscribe(response => {
        // this.alertService.info(response.data);
        this.getUtterances(this.modelName, this.preIntentName);
      });
    }
  }

  getUtteranceScores(scores: string) {
    if (scores == null || scores.length === 0) {
      return null;
    }

    // let index = 0;
    const scoresObject = JSON.parse(scores);
    const finalScores = [];
    let scoresTotalPercent = 0.0;

    // Go through all the items
    scoresObject.forEach(record => {
      const keys = Object.keys(record);

      keys.forEach(key => {
        // Returns an array of values
        const scoreItem = record[key];
        const scoreValue = Number.parseFloat(scoreItem);
        scoresTotalPercent += scoreValue;

        finalScores.push({ key: key, value: scoreValue });
      });
    });

    // console.log('finalScores', JSON.stringify(finalScores));

    // if (scoresTotalPercent < 1.0) {
    //   finalScores.push({ key: 'none', value: 1.0 - scoresTotalPercent });
    // }

    // Sort by highest score first
    const indexedScores = finalScores.sort(function (a, b) {
      if (a.value > b.value) {
        return -1;
      }
      if (a.value < b.value) {
        return 1;
      }
      return 0;
    }).map((v, i, a) => ({ key: v.key, value: v.value, index: i }));

    // console.log('indexedScores', JSON.stringify(indexedScores));
    return indexedScores;
  }

  getIntents(dialog, utterance): void {
    // console.log("Input " + this.inputIntent);
    if (dialog !== null) {
      dialog.open();
    }

    const self = this;
    this.articles = '';
    this.IntentScoreModel = [];
    this.intentClicked = true;

    // self.textClassificationService.getIntentsByUser(this.model.UtteranceText, this.modelName, this.UserName).subscribe
    self.textClassificationService.getIntentsByUser(utterance, this.modelName, this.UserName).subscribe
      (
      response => {
        this.articles = JSON.parse(response.message);
        this.keys = Object.keys(this.articles);
        // Object.keys(this.articles).forEach(key => {
        this.keys.forEach(key => {
          if (key.toLowerCase() === 'intents') {
            // Array of intents expected
            const item = this.articles[key];

            Object.keys(item).forEach(key1 => {
              // console.log("tesdt", item[key1]);
              const item1 = item[key1];

              Object.keys(item1).forEach(key2 => {
                // const datamodel: any = {};
                // datamodel.key = key2;
                // datamodel.value = item1[key2];
                this.IntentScoreModel.push({ key: key2, value: item1[key2] });
                // console.log("tesdsdt", item1[key2]);
              });
            });
          }
        });

        // console.log('IntentScoreModel',
        this.IntentScoreModel.sort(function (a, b) {
          if (a.value > b.value) {
            return -1;
          }
          if (a.value < b.value) {
            return 1;
          }
          return 0;
        });

        this.IntentScoreModel = this.IntentScoreModel.slice(0, 3);
        // this.IntentScoreModel.push({ key: 'new', value: '' })

        // console.log("IntentScoreModel sliced", this.IntentScoreModel);
        this.intentClicked = false;
      },
      error => {
        this.errorMessage = <any>error;
        this.intentClicked = false;
      }
      );

    this.getTree();
  }

  public getTree(): void {
    // console.log("Input " + this.inputIntent);
    const self = this;
    this.articles = '';

    self.textClassificationService.getTreeByUser(this.model.UtteranceText, this.modelName, this.UserName).subscribe
      (
      response => {
        this.JsonTree = this.transform(response.toString());
        // console.log("JSonTree", this.JsonTree);
      },
      error => this.errorMessage = <any>error
      );

  }

  public getKeys(data) {
    this.keys = Object.keys(data);
    return true;
  }

  transform(html) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

  goToNext(_id: String, catName: String) {
    this.sharedService.FormData = {
      FulfillmentForm: {
        projname: this.model.projectName,
        modelid: this.model.modelId,
        modelname: this.modelName,
        utteranceid: _id,
        intentcatname: catName
      }
    } as IFormData;

    this.router.navigate(['/intentfulfillment'], { queryParams: { id: _id, modelId: this.model.modelId } });
  }

  editUtterance(dialog, row) {
    if (!row) { return; }
    if (!dialog) { return; }

    this.isEditing = true;

    this.model.edited._id = row._id;
    this.model.edited.text = row.UtteranceText;
    this.model.edited.catName = row.CategoryName;
    this.model.edited.catId = row.CategoryId;

    dialog.open();
  }

  updateUtterance(dialog: any): void {
    if (this.isEditing) {
      this.textClassificationService.updateUtterance(this.modelName, this.model.edited).subscribe(data => {
        // console.log('updateUtterance', JSON.stringify(data));
        this.isTrained = !(data && data.success);
        this.showMessage(data);
        this.getUtterances(this.modelName, this.preIntentName);
        this.model.edited = {};
      },
        error => { this.alertService.error(error.error.message); });
    }

    dialog.close();
  }

  showMessage(data) {
    if (data) {
      this.alertService.clear();

      switch (data.success) {
        case true: this.alertService.success(data.data); break;
        case false: this.alertService.error(data.data); break;
      }
    }
  }

  preparePOS(pos: string) {
    const posObject = this.parseJSON(pos);

    let posHTML = '';

    if (posObject == null) {
      return posHTML;
    }

    if (posObject.nouns != null && posObject.nouns.length > 0) {
      posHTML += '<b>Nouns : </b>' + this.formatPOSInput(posObject.nouns.map(x => x.value));
    }

    if (posObject.propernouns != null && posObject.propernouns.length > 0) {
      posHTML += '<br><b>Proper Nouns : </b>' + this.formatPOSInput(posObject.propernouns.map(x => x.value));
    }

    if (posObject.verbs != null && posObject.verbs.length > 0) {
      posHTML += '<br><b>Verbs : </b>' + this.formatPOSInput(posObject.verbs.map(x => x.value));
    }

    if (posObject.adjectives != null && posObject.adjectives.length > 0) {
      posHTML += '<br><b>Adjectives : </b>' + this.formatPOSInput(posObject.adjectives.map(x => x.value));
    }

    return posHTML;
  }

  formatPOSInput(list: string[]) {
    if (list == null || list.length === 0) {
      return '';
    }

    let input = '';

    list.forEach((v, i, a) => {
      if (i > 0) {
        input += ' , ';
      }

      input += '"' + v + '"';
    });

    return input;
  }

  parseJSON(text) {
    try {
      return JSON.parse(text);
    } catch (e) {
      return {};
    }
  }
}
