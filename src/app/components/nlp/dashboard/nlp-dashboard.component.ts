import { Router, ActivatedRoute } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { getLocaleDateTimeFormat } from '@angular/common';
import { getDate } from 'date-fns';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';

import { AlertService, SharedService, NlpService, CommonService } from '../../../shared/service';
import { IFormData, ITextClassification, Culture } from '../../../shared/models';
import { Utilities } from '../../../shared/utils';

@Component({
  selector: 'mercer-nlp-dashboard',
  moduleId: module.id.toString(),
  templateUrl: './nlp-dashboard.component.html'
})
export class NLPDashboardComponent implements OnInit {
  public model: any = {};

  public stripedheaderData: any[] = [
    { name: 'Model Name' },
    { name: 'Culture' },
    { name: 'Description' },
    { name: 'Created Date' },
    { name: 'Shared' },
    { name: 'System Defined' },
    { name: 'Last Published (24 hrs format)' },
    { name: 'Action' },
    // { name: 'End Point Hits' }
  ];
  public stripedrowData: any = [];

  predefinedCultures: any = [];
  // NlpLanguageModelData: any;
  isEditing = false;

  errorMessage: string;

  constructor(
    private nlpService: NlpService,
    private sharedService: SharedService,
    private commonService: CommonService,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getCultures();

    // Get NLP language Models
    this.getNlpLanguageModel();
  }

  public SaveNlpLanguageModel(infoExample: any): void {
    // if the user info is available, map user id
    this.sharedService.getUserInfo().subscribe(x => {
      this.model.updatedBy = x._id;
      this.model.userName = x.username;
    });

    this.alertService.clear();

    if (this.isEditing === false) {
      this.nlpService.createNlpLanguageModel(this.model).subscribe(data => {

        // Create Spacy Model only when NLP Language model is created successfully
        if (data.success) {
          this.getNlpLanguageModel();
          // this.createModel(this.model);
        }

        this.showMessage(data);
      },
        error => { this.alertService.error(error.error.message); });
    } else {
      this.nlpService.updateLanguageModel(this.model).subscribe(data => {
        this.isEditing = false;
        this.showMessage(data);
        this.getNlpLanguageModel();
      },
        error => { this.alertService.error(error.error.message); });
    }

    infoExample.close();
  }

  private createModel(model): void {
    this.nlpService.createModel(this.model).subscribe(data => {
    },
      error => { });
  }

  public getNlpLanguageModel(): void {
    // this.nlpService.getAll().subscribe(data => {

    this.sharedService.getUserInfo().subscribe(user => {
      this.nlpService.getByUserId(user._id).subscribe(data => {
        // this.NlpLanguageModelData = data;
        // console.log('data', data, JSON.stringify(x));
        const utils = new Utilities();

        this.stripedrowData = data.map(row => {
          return {
            _id: row._id,
            name: row.name,
            cultureCode: row.cultureCode,
            cultureName: row.cultureCode ? row.cultureCode.name : 'NA',
            description: row.description,
            modified: row.modified,
            shared: row.shared,
            sysdefined: row.sysdefined,
            publishdate: row.publishdate,
            editable: utils.isEditingAllowed(row, user)
          };
        });

        this.changeDetectorRef.detectChanges();
      },
        error => {
          this.alertService.clear();
          this.alertService.error(error.error.message);
        });
    });
  }

  public getCultures(): void {
    this.commonService.getAllCultures().subscribe(data => { this.predefinedCultures = data; },
      error => { this.alertService.error('Unable to get Cultures'); }
    );
  }

  goToDetails(modelName) {
    const formData = {} as IFormData;
    formData.TextClassificationForm = { modelname: modelName } as ITextClassification;

    this.sharedService.FormData = formData;

    // console.log('goToDetails', JSON.stringify(formData));
    // this.router.navigate(['/intentclassification']);
    this.router.navigate(['/addutterance']);
  }

  addModel(dialog) {
    if (!dialog) { return; }

    this.isEditing = false;

    // this.model._id = '';
    // this.model.name = '';
    // this.model.cultureCode = '';
    // this.model.description = '';
    this.model = {};
    this.model.shared = false;
    this.model.sysdefined = false;

    dialog.open();
  }

  editModel(dialog, row) {
    if (!row) { return; }
    if (!dialog) { return; }

    this.isEditing = true;

    this.model._id = row._id;
    this.model.name = row.name;
    this.model.cultureCode = row.cultureCode._id;
    this.model.description = row.description;
    this.model.shared = row.shared;
    this.model.sysdefined = row.sysdefined;

    dialog.open();
  }

  showMessage(data) {
    if (data) {
      this.alertService.clear();

      switch (data.success) {
        case true: this.alertService.success(data.data); break;
        case false: this.alertService.error(data.data); break;
      }
    }
  }

  deleteModel(_id, dialog) {
    this.alertService.clear();

    this.nlpService.deleteNlpLanguageModel(_id).subscribe(result => {
      if (result.success) {
        this.showMessage(result);

        this.getNlpLanguageModel();
      }
    },
      error => {
        this.alertService.error(error.error.message);
      });

    dialog.close();
  }

  downloadModel(_id) { }
}
