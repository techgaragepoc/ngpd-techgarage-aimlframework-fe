import {
  Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
  ChangeDetectorRef, AfterViewInit, Input
} from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { Router, ActivatedRoute, Params } from '@angular/router';

// import { MosConfigurationService } from 'merceros-ui-components/services';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';

import { PaginationInfo } from '../../../shared/models';
import { Utilities } from '../../../shared/utils';

import {
  AuthenticationService,
  SharedService,
  TextClassificationService,
  NlpService,
  // IntentModelDataService,
  AlertService
} from '../../../shared/service/index';

import { IntentCategory, IntentCategoryUtterance } from '../../../shared/models/Intents';
import { IFormData, ITextClassification } from '../../../shared/models/formdata';

@Component({
  selector: 'mercer-text-classification',
  templateUrl: './text-classification.component.html',
  styleUrls: ['./text-classification.component.scss']
})
export class TextClassificationComponent implements OnInit {
  paginationInfo: PaginationInfo = {
    offset: 0,
    limit: 10,
    limits: [2, 5, 10, 20, 30],
    totalCount: 10
  };
  public model: any = {};

  public stripedheaderData: any[] = [
    { name: 'Intent Class' },
    { name: '#Utterances' }
  ];

  // public stripedrowData: Observable<any>;
  public stripedrowData: any[] = [];

  public childMessage = 'intentclassification';
  public isTrained = false;

  modelName: string;
  errorMessage: string;
  IntentName: string;
  intentData: IntentCategory;
  modelExists: false;

  IntentCategorys: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private textClassificationService: TextClassificationService,
    // private ModelDataService: IntentModelDataService,
    private authService: AuthenticationService,
    private alertService: AlertService,
    private sharedService: SharedService,
    private nlpService: NlpService,
    private changeDetectorRefs: ChangeDetectorRef) { }

  ngOnInit(): void {
    const formData = (this.sharedService.FormData) ? this.sharedService.FormData.TextClassificationForm : null;
    this.modelName = (formData) ? formData.modelname : null;

    if (this.modelName == null || this.modelName.length === 0) {
      this.modelName = new Utilities().GetQueryStringParam(this.route, 'modelName');
    }

    this.nlpService.getByName(this.modelName).subscribe(model => {
      if (model) {
        this.isTrained = model.istrained;
      }
    },
      error => {
        this.isTrained = false;
      });

    this.getIntents();
  }

  public getIntents(): void {
    // console.log('Test Category');

    this.textClassificationService.getByModel(this.modelName).subscribe(
      result => {
        this.IntentCategorys = result;
        this.stripedrowData = result.slice(0, this.paginationInfo.limit);

        this.paginationInfo = {
          offset: 0,
          limit: this.paginationInfo.limit,
          limits: this.paginationInfo.limits,
          totalCount: result.length
        };

        this.changeDetectorRefs.detectChanges();
      },
      error => this.errorMessage = <any>error
    );

  }
  public updatePage(model) {
    // console.log('updatePage', model);
    this.paginationInfo.limit = model.limit;
    this.paginationInfo.offset = model.offset;

    if (model.offset === 0) {
      this.stripedrowData = this.IntentCategorys.slice(0, model.limit);
    } else {
      this.stripedrowData = this.IntentCategorys.slice(model.offset, model.offset + model.limit);
    }
  }

  isNullOrEmpty(val: String) {
    return (val === null || val === '' || (typeof val === 'undefined'));
  }

  private validateIntent(model): boolean {
    let isModelValid = true;
    this.errorMessage = '';
    if (this.model != null) {
      if (this.isNullOrEmpty(model.CategoryName)) {
        this.errorMessage = 'Intent Name cannot be blank';
        isModelValid = false;
      }

    }
    return isModelValid;
  }

  public SaveIntent(infoExample: any): void {
    const intentName = this.model.CategoryName;
    this.model.ModelName = this.modelName;

    this.alertService.clear();

    if (this.validateIntent(this.model)) {
      this.textClassificationService.createIntent(this.model).subscribe(data => {
        let formData = this.sharedService.FormData;

        if (formData) {
          if (formData.TextClassificationForm) {
            formData.TextClassificationForm.intentname = intentName;
          } else {
            formData =
              {
                TextClassificationForm:
                {
                  intentname: intentName,
                  modelname: this.modelName
                } as ITextClassification
              } as IFormData;
          }
        } else {
          formData =
            {
              TextClassificationForm:
              {
                intentname: intentName,
                modelname: this.modelName
              } as ITextClassification
            } as IFormData;
        }

        this.sharedService.FormData = formData;

        // console.log('formdata', JSON.stringify(formData));
        // this.ModelDataService.changeCategory(intentName);
        switch (data.success) {
          case true: {
            this.alertService.success(data.data);
            this.isTrained = false;
            // this.changeDetectorRefs.detectChanges();
            this.getIntents();
            // this.router.navigate(['/addutterance']);
            break;
          }
          case false: {
            this.alertService.error(data.data);
            break;
          }
        }
      },
        error => {
          // console.log(JSON.stringify(error));
          this.alertService.error(error.error.message);
        });
    }

    infoExample.close();
  }
  public navigatetoAddUtterances(intentName): void {
    let data = {} as IFormData;

    if (this.sharedService.FormData) {
      data = this.sharedService.FormData;

      if (this.sharedService.FormData.TextClassificationForm) {
        data.TextClassificationForm.intentname = intentName;
        data.TextClassificationForm.modelname = this.modelName;
      } else {
        data = { TextClassificationForm: { intentname: intentName, modelname: this.modelName } } as IFormData;
      }
    }

    // this.ModelDataService.changeCategory(intentName);
    // localStorage.setItem('intentName', intentName);
    this.sharedService.FormData = data;
    this.router.navigate(['/addutterance']);
  }

  // public trainModel(): void {
  //   const loggedUser = this.authService.getUserInfo();
  //   const userName = loggedUser ? loggedUser.username : 'NA';

  //   this.alertService.clear();

  //   // console.log('username', userName);
  //   this.textClassificationService.trainEntireModel(this.modelName, userName).subscribe(data => {
  //     if (data && data.success) {
  //       this.alertService.success('Model trained successfully');
  //     }
  //   },
  //     error => {
  //       this.alertService.error(error.error.message);
  //     });
  // }

  public deleteCategory(_id, dialog) {
    if (!dialog) { return; }
    this.alertService.clear();

    this.textClassificationService.deleteIntent(_id).subscribe(data => {
      if (data) {

        switch (data.success) {
          case true: {
            this.alertService.success(data.data);
            this.isTrained = false;
            this.getIntents();
            break;
          }

          case false: {
            this.alertService.error(data.data);
            break;
          }
        }
      }
    },
      error => {
        this.alertService.error(error.error.message);
      });

    dialog.close();
  }

  // testModel() {
  //   this.router.navigate(['/intent'], { queryParams: { modelName: this.modelName } });
  // }
}
