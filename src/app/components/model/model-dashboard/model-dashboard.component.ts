// import { Subscription, Observable } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';
// import { environment } from '../../../../environments/environment';

@Component({
  selector: 'mercer-model-dashboard',
  moduleId: module.id.toString(),
  templateUrl: 'model-dashboard.component.html'
})
export class ModelDashboardComponent {
  public stripedheaderData: any[] = [
    {
      name: 'Organization',
      column: 'org'
    },
  ];
  public stripedrowData: any[] = [
    {
      org: 'Google'
    },
    {
      org: ''
    }
  ];

  public stripedheaderData1: any[] = [
    {
      name: 'Person',
      column: 'person'
    },
  ];
  public stripedrowData1: any[] = [
    {
      person: 'Users'
    },
    {
      person: ''
    }
  ];

  public stripedheaderData2: any[] = [
    {
      name: 'Location',
      column: 'location'
    },
  ];
  public stripedrowData2: any[] = [
    {
      location: 'Mountain View'
    },
    {
      location: ''
    }
  ];

  public stripedheaderData3: any[] = [
    {
      name: 'Event',
      column: 'event'
    },
  ];
  public stripedrowData3: any[] = [
    {
      event: 'Consumer Electronic Show'
    },
    {
      event: ''
    }
  ];


  public stripedheaderData4: any[] = [
    {
      name: 'Leavepolicy',
      column: 'leavepolicy'
    },
  ];
  public stripedrowData4: any[] = [
    {
      leavepolicy: 'Confidence : 4.5'
    },
    {
      leavepolicy: ''
    }
  ];

  public stripedheaderData5: any[] = [
    {
      name: 'Personality',
      column: 'personality'
    },
  ];
  public stripedrowData5: any[] = [
    {
      personality: 'Confidence : 2.5'
    },
    {
      personality: ''
    }
  ];

  public stripedheaderData6: any[] = [
    {
      name: 'Sick',
      column: 'sick'
    },
  ];
  public stripedrowData6: any[] = [
    {
      sick: 'Confidence : 0.5'
    },
    {
      sick: ''
    }
  ];

  public stripedheaderData7: any[] = [
    {
      name: 'Vacation',
      column: 'vacation'
    },
  ];
  public stripedrowData7: any[] = [
    {
      vacation: 'Confidence : 9.5'
    },
    {
      vacation: ''
    }
  ];

  title = 'Tour of Heroes';
  rakesh: Observable<any>;
  errorMessage: string;
  articles: string;
  inputIntent: string;

  constructor() {
  }

  /*
  getIntents(): void {
    console.log("Input " + this.inputIntent);
    let self = this;

    self._intentService.getJSON(this.inputIntent).subscribe
      (
      response => {
        this.articles = JSON.parse(response.message)
        // console.log("In COmponenet ",JSON.stringify(response));
      },
      error => this.errorMessage = <any>error
      );
    }*/
}
