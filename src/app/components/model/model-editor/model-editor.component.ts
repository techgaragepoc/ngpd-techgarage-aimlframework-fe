import { Component, ViewChild, ElementRef, AfterViewInit, OnInit } from '@angular/core';
import { Model, User, DomainModel, NodeAttributeModel } from '../../../shared/models';
import { Router } from '@angular/router';
import {DomainModelService} from '../../../shared/service';
import { Observable } from 'rxjs/Observable';

const gojs = require('../../../shared/libs/gojs/go');

@Component({
  selector: 'mercer-model-editor',
  styleUrls: ['./model-editor.component.css'],
  moduleId: module.id.toString(),
  templateUrl: 'model-editor.component.html'
})
export class ModelEditorComponent implements AfterViewInit {
  @ViewChild('contextMenu') contextMenu: HTMLDivElement;
  @ViewChild('myPaletteDiv') divPalette;
  @ViewChild('myPropertiesDiv') divProperties;
  @ViewChild('myDiagramDiv') divDiagram;

  myDiagram: any;
  myPalette: any;
  myModel: any;

  nodeName: string;  
  domainName: string;
  nodeNumber: number;
  currentNodeKey:string;
  domainModel: DomainModel;  
  attributes:Array<NodeAttributeModel>;
  attributeDescription: string;
  attributeName: string;
  attributeType: string;
  attributeGridHeaders: any;
  attribute: NodeAttributeModel;
  showPropertiesWindow:boolean = false;
  domainModels:DomainModel[];
  


  public attributeTypes = ['Numeric','String','Date'];

  nodeConnector: string;
  nodeColor:string;
  selectedNode: any;

  //TODO: As on 8th October, we need to populate data into Model objects added from 
  // the screen
  constructor(private router: Router,
  private domainModelService: DomainModelService) {     
  
  this.attribute = new NodeAttributeModel();
  }

  ngOnInit(): void {

    this.attributeGridHeaders = [{
      "name": "Name",
      "column": "name"
    },
    {
      "name": "Attribute Type",
      "column": "attributeType"
    }    
    ];        

    this.attributes = [];    
    this.domainName = "Health";
    this.nodeNumber = 0;

  }
  nodeConnectorChanged(): void {
    if (this.selectedNode)
      this.myDiagram.model.setDataProperty(this.selectedNode, "connector", this.nodeConnector);
  }
  nodeColorChanged():void{
    if (this.selectedNode)
      this.myDiagram.model.setDataProperty(this.selectedNode, "key", this.currentNodeKey);
  }
  nodeNameChanged(): void {
    if (this.selectedNode)
    {
      this.myDiagram.model.setDataProperty(this.selectedNode, "name", this.nodeName);
      console.log("Current Node Id", this.currentNodeKey);
    }
  }
  getTree(event): void {
    const data = JSON.parse(this.myDiagram.model.toJson());
    console.log(data.nodeDataArray);
    console.log(data.linkDataArray);
  }
  ngAfterViewInit() {
    var $ = gojs.GraphObject.make;
    var diagramDiv = this.divDiagram.nativeElement;
    var paletteDiv = this.divPalette.nativeElement;
    var propertiesDiv = this.divProperties.nativeElement;
    const self = this;
    this.myDiagram =
      $(gojs.Diagram, diagramDiv, {
        initialContentAlignment: gojs.Spot.Center,
        allowDrop: true,
        "undoManager.isEnabled": true
      });

    this.myDiagram.addDiagramListener("ObjectSingleClicked",
      function (e) {
        var part = e.subject.part;
        if (!(part instanceof gojs.Link)) {
          self.selectedNode = self.myDiagram.model.nodeDataArray.filter(v => v.key == part.data.key)[0];
          self.nodeConnector = self.selectedNode.connector;
          self.nodeName = self.selectedNode.name;    
          self.currentNodeKey = self.selectedNode.key;      
          //console.log("cURRENT nODE kEY ",pa);
          console.log("cURRENT nODE kEY ",part.data.key);
          console.log("cURRENT nODE Base kEY ",part.data.key);
        }
      });
    this.myDiagram.addDiagramListener("BackgroundSingleClicked",
      function (e) {
        self.selectedNode = self.nodeConnector = self.nodeName = self.nodeColor = '';
      });
    this.myDiagram.addDiagramListener("SelectionDeleted",
      function (e) {
        self.selectedNode = self.nodeConnector = self.nodeName = self.nodeColor = '';
      });
    this.myDiagram.addDiagramListener("BackgroundDoubleClicked",
      function (e) {
        self.selectedNode = self.nodeConnector = self.nodeName =  self.nodeColor = '';
      });
    this.myDiagram.addDiagramListener("ExternalObjectsDropped", function (e) {
      let part = e.diagram.selection.first();
      if (!(part instanceof gojs.Link)) {
        self.nodeNumber = self.nodeNumber + 1;
        console.log("Node Number  ",self.nodeNumber);
        self.showPropertiesWindow = true;
        self.selectedNode = self.myDiagram.model.nodeDataArray.filter(v => v.key == part.data.key)[0];
        self.nodeConnector = self.selectedNode.connector;
        self.nodeName = self.selectedNode.name;
        self.currentNodeKey = self.selectedNode.key;
        self.selectedNode.basekey = self.selectedNode.key
      }
    });

    this.myDiagram.nodeTemplate =
      $(gojs.Node, "Auto",  // the Shape will go around the TextBlock
        new gojs.Binding("desiredSize", "size", gojs.Size.parse),
        new gojs.Binding("position", "pos", gojs.Point.parse).makeTwoWay(gojs.Point.stringify),
        $(gojs.Shape, "RoundedRectangle",//Circle
          { portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer", strokeWidth: 0, fill: "#00c0ef" },
          new gojs.Binding("fill", "color")),
        $(gojs.TextBlock,
          { margin: 8 },  // some room around the text
          new gojs.Binding("text", "text"))
      );
      var myconnector = "sanjay";
      
    var myPalette =
      $(gojs.Palette, paletteDiv, {
        maxSelectionCount: 1,
        nodeTemplateMap: self.myDiagram.nodeTemplateMap,
        linkTemplate: $(gojs.Link, {
          locationSpot: gojs.Spot.Left,
          selectionAdornmentTemplate: $(gojs.Adornment, "Link", {
            locationSpot: gojs.Spot.Left
          },
            $(gojs.Shape, {
              isPanelMain: true,
              fill: null,
              strokeWidth: 0
            })
          )
        }, {
            routing: gojs.Link.AvoidsNodes,
            curve: gojs.Link.JumpOver,
            corner: 5,
            toShortLength: 4
          },
        ),
        model: new gojs.GraphLinksModel([ // specify the contents of the Palette
          { basekey:1, nodetype:"entity", text: this.attributeName, figure: "RoundedRectangle", color: "#add8e6" },
        ],
          []
        )
      });      
  }

  public saveDomainModel(event: any): void {
    //Save Domain Model here.    
  }

  public getModelList(event: any): void {
    //This will get the names of models along with Last modified/created etc.
  }

  public getModel(event: any): void {
   //Get Single Model
  }

  addAttributes(event): void {

    this.saveAttributes().subscribe(
      collection => {
        //this.attributes = collection.data;
        console.log("Hello ",collection.data);
    //    console.log("Collection Data ", this.attributes.length);
      },
      error => {
        console.log("Error ",error);
      }
    );
    console.log("Hi ");
    //Reset the Attributes
    this.attributeName = "";
    this.attributeDescription = "";
    this.attributeType = "";
  }
  saveAttributes():Observable<any>
  {
    var collection: Array<NodeAttributeModel> = [];
    return  Observable.create(observer =>
    {            
      this.attribute.name = this.attributeName;
      this.attribute.description = this.attributeDescription;       
      this.attribute.attributeType = this.attributeType;    
      collection = this.attributes.concat(this.attribute); 
      
      this.attributes = collection;
      observer.data = this.attributes;
      console.log("Master ", this.attributes.length)      
    });
        
  }
  
  onSelectDescription(selectedIndex: any,filteredList:any) {    
    this.attributeType = filteredList[selectedIndex];     
  }
}
