import { Component, Input, OnInit } from '@angular/core';

import { ModelService } from '../../../shared/service/model.service';
import { environment } from '../../../../environments/environment';

@Component({
    // moduleId: module.id,
    selector: 'mercer-model-master',
    template: './model-master.component.html'
})
export class ModelMasterComponent implements OnInit {
    // constructor(private trackerService: TrackerService) { }

    private totalRows: number;
    private errorMessage: string;

    eventId: string;
    eventName: string;
    eventDesc: string;
    eventPayload: any;

    eventMasterList: any[];

    ngOnInit() {
        this.getEventMasterData();
    }

    getEventMasterData() {
        // this.trackerService.getEventTypes().subscribe(
        //     res => {
        //         this.eventMasterList = res.data;
        //         this.totalRows = res.total;

        //         for (let i = 0; i < this.eventMasterList.length; i++) {
        //             this.eventMasterList[i].EventStr = JSON.stringify(JSON.parse(this.eventMasterList[i].EventStr), null, 4);
        //         }
        //     },
        //     error => this.errorMessage = <any>error
        // );
    }

    clearFields() {
        this.eventMasterList = null;
        this.eventId = '';
        this.eventName = '';
        this.eventDesc = '';
        this.eventPayload = '';
    }

    saveEventsMasterInfo() {
        // this.trackerService.addEventMaster(this.eventName, this.eventDesc, this.eventPayload).subscribe(
        //     data => {
        //         this.clearFields();
        //         // console.log('Added successfully');
        //         this.getEventMasterData();
        //     },
        //     error => {
        //         // console.log(JSON.parse("{'data': 'Test 2 one more time'}"));
        //         this.errorMessage = error;
        //         console.error(error);
        //     });
    }

    editInfo(rowItem: any) {
        this.eventId = rowItem._id;
        this.eventName = rowItem.EventName;
        this.eventDesc = rowItem.EventDesc;
        this.eventPayload = rowItem.EventStr;
    }

    private IsNewRecord(propName: string, propValue: string): boolean {
        const result = (this.eventId === null || this.eventId === '');
        // console.log('IsNewRecord: ' + result);
        return result;

        // for (var index = 0; index < this.events.length; index++) {
        //     var item = this.events[index];

        //     if (item.hasOwnProperty(propName)) {
        //         if (item[propName].toLowerCase() === propValue.toLowerCase()) {
        //             return false;
        //         }
        //     }
        // };

        // return true;
    }
}
