import { Router, ActivatedRoute } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { getLocaleDateTimeFormat } from '@angular/common';
import { getDate } from 'date-fns';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';

import {
  AlertService, AuthenticationService, SharedService, NlpService, CommonService,
  IntentFulfillmentModelService, TextClassificationService
} from '../../../shared/service';
import { IFormData, ITextClassification, Culture } from '../../../shared/models';

@Component({
  selector: 'intent-fullfilment-dashboard',
  moduleId: module.id.toString(),
  templateUrl: './intent-fulfillment-dashboard.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class IntentFulfillDashboardComponent implements OnInit, OnDestroy {
  public model: any = {};
  public fulfilledIntentMappings: any = [];

  public stripedheaderData: any[] = [
    { name: 'Intent' },
    { name: 'NLP Model' },
    { name: 'Requester' },
    { name: 'Last Updated' },
    { name: 'Status' },
    { name: 'Implemented' },
    { name: 'Action' }
  ];
  public stripedrowData: any = [];

  predefinedCultures: any = [];
  isEditing = false;
  public intentDetails: any = {};
  public nlpModels: any = [];
  public intentCategories: any = [];

  errorMessage: string;

  constructor(
    private nlpService: NlpService,
    private sharedService: SharedService,
    private alertService: AlertService,
    private router: Router,
    private intentFulfillmentService: IntentFulfillmentModelService,
    private changeDetectorRef: ChangeDetectorRef,
    private textClassificationService: TextClassificationService
  ) { }

  ngOnInit(): void {
    // Get Fulfilled Intents
    this.getNlpModels();
  }

  private createModel(model): void {
    this.nlpService.createModel(this.model).subscribe(data => {
    },
      error => { });
  }

  ngOnDestroy() {

  }

  public getFulfilledIntents(): void {

    this.intentFulfillmentService.getAllIntentFulfillmentModels().subscribe(data => {

      this.stripedrowData = data.data.map(x => {
        const mapping = this.fulfilledIntentMappings.find(map => map.intentfulfillmentmodelgraphid === x._id);

        const implementationStatus = (mapping != null && typeof mapping !== 'undefined' &&
          mapping.implementationstatus !== '' && typeof mapping.implementationstatus !== 'undefined')
          ? mapping.implementationstatus : '0';

        const nlpInfo = this.getNlpInfo(x._id);

        // console.log('nlpInfo', JSON.stringify(nlpInfo), JSON.stringify(mapping));
        const nlpIntentId = nlpInfo != null ? nlpInfo.nlpintentid : '';
        const nlpModelId = nlpInfo != null ? nlpInfo.nlpmodelid : '';

        return {
          intentfulfillmentmodelgraphid: x._id,
          createdby: x.createdby,
          createdon: x.createdon,

          lastmodifiedby: (x.lastmodifiedby != null && typeof x.lastmodifiedby !== 'undefined')
            ? x.lastmodifiedby : x.createdby,

          lastmodifiedon: x.lastmodifiedon,
          name: x.name,
          implementationstatus: implementationStatus,

          status: (parseInt(implementationStatus, 0) < 100) ? 'In Progress' : 'Implemented',

          nlpintentid: nlpIntentId,
          nlpmodelid: nlpModelId,
          modelName: this.getnlpModelName(nlpModelId),
          intentCategory: this.getintentCategoryName(nlpIntentId)
        };
      });

      this.changeDetectorRef.detectChanges();
    },
      error => {
        this.alertService.clear();
        this.alertService.error(error.error.message);
      });
  }

  getNlpInfo(key: string) {
    const nlpInfo = this.fulfilledIntentMappings
      .filter(x => x.intentfulfillmentmodelgraphid === key)
      .map(x => ({ nlpintentid: x.nlpintentid, nlpmodelid: x.nlpmodelid }));

    if (nlpInfo != null && nlpInfo.length === 1) {
      return nlpInfo[0];
    }

    return null;
  }

  getnlpIntentId(key: string): string {
    let value = '';

    for (let i = 0; i < this.fulfilledIntentMappings.length; i++) {
      if (this.fulfilledIntentMappings[i].intentfulfillmentmodelgraphid === key) {
        value = this.fulfilledIntentMappings[i].nlpintentid;

        break;
      }
    }
    return value;

  }

  getnlpModelId(key: string): string {
    let value = '';

    for (let i = 0; i < this.fulfilledIntentMappings.length; i++) {
      if (this.fulfilledIntentMappings[i].intentfulfillmentmodelgraphid === key) {
        value = this.fulfilledIntentMappings[i].nlpmodelid;
        break;
      }
    }
    return value;
  }

  getnlpModelName(id: string): string {
    const modelInfo = this.nlpModels.filter(x => x._id === id);

    if (modelInfo != null && modelInfo.length === 1) {
      return modelInfo[0].name;
    }

    return null;
  }

  getintentCategoryName(id: string): string {
    const intentInfo = this.intentCategories.filter(x => x._id === id);

    if (intentInfo != null && intentInfo.length === 1) {
      return intentInfo[0].CategoryName;
    }

    return null;
  }

  public getAllIntentFulfillmentMappings() {
    // console.log("Mapping called");

    this.intentFulfillmentService.getAllIntentFulfillmentMappings().subscribe(data => {
      this.fulfilledIntentMappings = data.data;
      this.getFulfilledIntents();
      // console.log("All Mapping Data  ", this.fulfilledIntentMappings);
    },
      error => {
        this.alertService.clear();
        this.alertService.error(error.error.message);
      });
  }

  showMessage(data) {
    if (data) {
      this.alertService.clear();

      switch (data.success) {
        case true: this.alertService.success(data.data); break;
        case false: this.alertService.error(data.data); break;
      }
    }
  }

  deleteModel(_id, dialog) {
    this.alertService.clear();

    this.nlpService.deleteNlpLanguageModel(_id).subscribe(result => {
      if (result.success) {
        this.showMessage(result);

        this.getFulfilledIntents();
      }
    },
      error => {
        this.alertService.error(error.error.message);
      });

    dialog.close();
  }

  downloadModel(_id) { }

  goToNext(nlpintentid: string, nlpmodelid: string, categoryName: string, modelName: string) {
    this.sharedService.FormData = {
      FulfillmentForm: {
        projname: this.model.projectName,
        modelid: nlpmodelid,
        modelname: modelName,
        intentcatname: categoryName,
        intentid: nlpintentid
      }
    } as IFormData;

    this.router.navigate(['/intentfulfillment'],
      {
        queryParams: {
          intentCategoryId: nlpintentid,
          modelId: nlpmodelid,
          categoryName: categoryName,
          nlpModel: modelName
        }
      });
  }

  addModel() {
    this.router.navigate(['/intentfulfillment']);
  }

  getNlpModels() {

    this.nlpService.getAll().subscribe
      (
      response => {
        this.nlpModels = response;
        this.getAllIntentCategories();
        // console.log("Models ", this.nlpModels);
      },
      error => this.errorMessage = <any>error
      );

  }

  getAllIntentCategories() {

    this.textClassificationService.getAll().subscribe
      (
      response => {
        this.intentCategories = response;
        // console.log("Categories ", response);
        this.getAllIntentFulfillmentMappings();

      },
      error => this.errorMessage = <any>error
      );
  }
}
