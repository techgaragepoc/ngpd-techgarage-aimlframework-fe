import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import {
  Component, Inject, OnInit, OnDestroy, AfterViewInit, ViewChild,
  ChangeDetectorRef, ChangeDetectionStrategy
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';

import { Model, User, IntentFulfillmentModel, IFormData } from '../../../shared/models';
import { Utilities } from '../../../shared/utils';
const gojs = require('../../../shared/libs/gojs/go');

import {
  AlertService, TextClassificationService, AuthenticationService, ModelService,
  IntentFulfillmentModelService, DomainModelService, SharedService
} from '../../../shared/service';

@Component({
  selector: 'mercer-intent-fulfillment-main',
  moduleId: module.id.toString(),
  styleUrls: ['./intent-fulfillment-main.component.css'],
  templateUrl: './intent-fulfillment-main.component.html',
  changeDetection: ChangeDetectionStrategy.Default
})
export class IntentFulfillmentMainComponent implements AfterViewInit, OnInit {

  @ViewChild('domainModel') divDomainModel;
  @ViewChild('intentFulfillment') divIntentFulfillment;
  @ViewChild('aggregationPipeline') divAggregationPipeline;
  models: Observable<any>;
  isDisabled: false;
  myDomainModelDiv: any;
  myIntentFulfillmentDiv: any;
  myAggregationPipelineDiv: any;
  modelName: string;
  inputUtterance: string;
  articles: any;
  IntentScoreModel: any[];
  keys: any;
  errorMessage: string;
  utteranceId: string;
  posTree: string;
  trainedEntities: any[];
  intentCategoryName: string;
  projectName: string;

  currentUser: User;
  username: String = '';
  intentfulfillmentModel: IntentFulfillmentModel;

  nlpmodelid: String;
  nlpintentid: String;
  intent: string;
  showFunctionList: boolean;
  iconList: any[];
  implementButtonDisabled: boolean = true;

  constructor(
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private route: ActivatedRoute,
    private textClassificationService: TextClassificationService,
    private domainmodelService: DomainModelService,
    private intentFulfillmentModelService: IntentFulfillmentModelService,
    private alertService: AlertService,
    private authService: AuthenticationService,
    private sharedService: SharedService
  ) {

    this.currentUser = this.authService.getUserInfo();
    this.username = (this.currentUser != null && this.currentUser.username !== 'undefined') ? this.currentUser.username : 'none';
    this.showFunctionList = false;
    this.iconList = [
      { code: "Func004", icon: "Mimimum_G.png" },
      { code: "Func002", icon: "Average_G.png" },
      { code: "Func001", icon: "Sum_G.png" },
      { code: "Func003", icon: "Max_G.png" },
      { code: "Func005", icon: "Supervised_G.png" },
      { code: "Func006", icon: "Unsupervised_G.png" },
      { code: "Func009", icon: "count.png" }];
  }

  ngOnInit() {
    this.inputUtterance = 'How many claims received against health policies?';

    const fulfillmentForm = (this.sharedService.FormData) ? this.sharedService.FormData.FulfillmentForm : null;
    this.projectName = (fulfillmentForm) ? fulfillmentForm.projname : null;
    this.modelName = (fulfillmentForm) ? fulfillmentForm.modelname : 'en';
    this.intentfulfillmentModel = new IntentFulfillmentModel();

    const utils = new Utilities();

    if (this.utteranceId == null || this.utteranceId.length === 0) {
      this.utteranceId = utils.GetQueryStringParam(this.route, 'id');
    }

    if (this.nlpmodelid == null || this.nlpmodelid.length === 0) {
      this.nlpmodelid = utils.GetQueryStringParam(this.route, 'modelId');
    }

    // Came from NLP Uttrances Page
    if (this.utteranceId != null) {
      this.loadIntentFromId();
    } else {
      // Came from Intent Listing
      this.nlpintentid = utils.GetQueryStringParam(this.route, 'intentCategoryId');
      this.intentCategoryName = utils.GetQueryStringParam(this.route, 'categoryName');
      const nlpModel = utils.GetQueryStringParam(this.route, 'nlpModel');
      this.loadFirstIntentFromCategory(nlpModel);
      // this.loadModel();
    }



  }

  loadIntentFromId() {
    this.textClassificationService.getIntentUtteranceById(this.utteranceId).subscribe
      (
      response => {
        if (response.data != null) {
          // console.log('NLP Response ', response.data);
          if (response.data.POS != null) {
            const pos: any = JSON.parse(response.data.POS);
            this.articles = this.getIntent(pos);
          }

          this.posTree = response.data.POSTree;
          this.trainedEntities = response.data.TrainedEntities != null ? JSON.parse(response.data.TrainedEntities) : null;

          if (response.data.Category != null) {
            this.intentCategoryName = response.data.Category.CategoryName;
            this.intentfulfillmentModel.name = this.intentCategoryName;
            this.nlpintentid = response.data.Category._id;
            this.loadModel();
          }
        }
      },
      error => this.errorMessage = <any>error
      );
  }

  loadFirstIntentFromCategory(modelName: string) {

    this.textClassificationService.getIntentUtterances(modelName, this.intentCategoryName).subscribe(
      response => {
        if (response.data != null && response.data.length > 0) {
          if (response.data[0].POS != null) {
            const pos: any = JSON.parse(response.data[0].POS);
            this.articles = this.getIntent(pos);
          }

          this.posTree = response.data[0].POSTree;
          this.trainedEntities = response.data[0].TrainedEntities != null ? JSON.parse(response.data[0].TrainedEntities) : null;

          if (response.data[0].Category != null) {
            this.nlpintentid = response.data[0].Category._id;
            this.loadModel();
          }
        }
      },
      error => this.errorMessage = <any>error
    );
  }


  ngAfterViewInit() {
    this.getAllModels();
    const $ = gojs.GraphObject.make;

    const domainModelDiv = this.divDomainModel.nativeElement;
    const intentFulfillmentDiv = this.divIntentFulfillment.nativeElement;

    if (!this.showFunctionList) {
      var aggregationPipelineDiv = this.divAggregationPipeline.nativeElement;
    }

    this.myDomainModelDiv = $(gojs.Diagram, domainModelDiv,  // create a Diagram for the DIV HTML element
      {
        initialContentAlignment: gojs.Spot.Center,
        allowDragOut: true,
        allowDrop: true,
        'commandHandler.archetypeGroupData': { isGroup: true },
        'ModelChanged': function (e) {
        },
        'ExternalObjectsDropped': function (e) {
          if (myIntentFulfillmentDivRef.commandHandler.canDeleteSelection() &&
            !(myIntentFulfillmentDivRef.lastInput.control || myIntentFulfillmentDivRef.lastInput.meta)) {
            myIntentFulfillmentDivRef.commandHandler.deleteSelection();
          }
        }
      });

    if (!this.showFunctionList) {
      this.myAggregationPipelineDiv = $(gojs.Diagram, aggregationPipelineDiv,  // create a Diagram for the DIV HTML element
        {

          initialContentAlignment: gojs.Spot.Top,
          allowDragOut: true,
          allowDrop: true,
          layout: $(gojs.GridLayout,
            {
              cellSize: new gojs.Size(200, 20),
              wrappingColumn: 1
            }),
          'commandHandler.archetypeGroupData': { isGroup: true },
          'ModelChanged': function (e) {
          },
          'ExternalObjectsDropped': function (e) {
            if (myIntentFulfillmentDivRef.commandHandler.canDeleteSelection() &&
              !(myIntentFulfillmentDivRef.lastInput.control || myIntentFulfillmentDivRef.lastInput.meta)) {
              myIntentFulfillmentDivRef.commandHandler.deleteSelection();
            }
          }
        });
    }

    // define a simple Node template
    this.myDomainModelDiv.nodeTemplate =
      $(gojs.Node, 'Auto',  // the Shape will go around the TextBlock
        new gojs.Binding('desiredSize', 'size', gojs.Size.parse),
        new gojs.Binding('position', 'pos', gojs.Point.parse).makeTwoWay(gojs.Point.stringify),
        $(gojs.Shape, 'RoundedRectangle', // Circle
          { portId: '', fromLinkable: true, toLinkable: true, cursor: 'pointer', strokeWidth: 0, fill: '#00c0ef' },
          // Shape.fill is bound to Node.data.color
          new gojs.Binding('fill', 'color')),
        $(gojs.TextBlock,
          { margin: 8 },  // some room around the text
          // TextBlock.text is bound to Node.data.key
          new gojs.Binding('text', 'text'))
      );

    this.myDomainModelDiv.model = new gojs.GraphLinksModel([], []);
    const myDomainModelDivRef = this.myDomainModelDiv;
    const myIntentFulfillmentDivRef = this.myIntentFulfillmentDiv;

    this.myIntentFulfillmentDiv = $(gojs.Diagram, intentFulfillmentDiv,
      {
        initialContentAlignment: gojs.Spot.Center,
        allowDragOut: true,
        allowDrop: true,
        'commandHandler.archetypeGroupData': { isGroup: true },
        // handle undo or redo maybe changing Diagram properties controlled by check boxes
        'ModelChanged': function (e) {
          // if (e.isTransactionFinished) allChecks(true);
        },
        'ExternalObjectsDropped': function (e) {
          if (myDomainModelDivRef.commandHandler.canDeleteSelection() &&
            !(myDomainModelDivRef.lastInput.control || myDomainModelDivRef.lastInput.meta)) {
            myDomainModelDivRef.commandHandler.deleteSelection();
          }
        }
      });
    // share templates with the first diagram
    this.myIntentFulfillmentDiv.nodeTemplate = this.myDomainModelDiv.nodeTemplate;

    this.myAggregationPipelineDiv.nodeTemplate =
      $(gojs.Node, 'Horizontal',
        // the Shape will go around the TextBlock
        // { position: new gojs.Point(0, 100) },

        { desiredSize: new gojs.Size(110, 20) },
        $(gojs.Picture,
          { maxSize: new gojs.Size(20, 20) },
          new gojs.Binding('source', 'img')),

        // Shape.fill is bound to Node.data.color
        $(gojs.TextBlock,
          { margin: new gojs.Margin(0, 0, 0, 2) },
          { font: '10pt sans-serif' },
          { columnSpan: 1 },
          // some room around the text
          // TextBlock.text is bound to Node.data.key
          new gojs.Binding('text', 'text'))
      );
    this.myIntentFulfillmentDiv.model = new gojs.GraphLinksModel([], []);

    this.intentFulfillmentModelService.getAllFunctionMetaData()
      .subscribe(functions => {
        if (functions.data) {
          const functionArray = functions.data.map((item, index, a) =>
            (
              {
                model: 0,
                basekey: item.code,
                nodetype: 'method',
                key: index + 100,
                text: item.name,
                color: 'lightblue',
                img: '../assets/images/' + this.iconList.filter(s => s.code === item.code).map(x => x.icon)
              }
            ));

          this.myAggregationPipelineDiv.model = new gojs.GraphLinksModel(functionArray, []);
        }
      });

    this.myDomainModelDiv.allowDragOut = true;
    this.myDomainModelDiv.allowDrop = true;
    this.myDomainModelDiv.isReadOnly = true;
    this.myDomainModelDiv.allowCopy = true;
    this.myDomainModelDiv.allowDelete = true;
    this.myDomainModelDiv.allowInsert = true;
    this.myDomainModelDiv.allowMove = true;

    this.myIntentFulfillmentDiv.allowDragOut = true;
    this.myIntentFulfillmentDiv.allowDrop = true;
    this.myIntentFulfillmentDiv.allowCopy = true;
    this.myIntentFulfillmentDiv.allowDelete = true;
    this.myIntentFulfillmentDiv.allowInsert = true;
    this.myIntentFulfillmentDiv.allowMove = true;
    this.myIntentFulfillmentDiv.isReadOnly = false;

    this.myAggregationPipelineDiv.allowDragOut = true;
    this.myAggregationPipelineDiv.allowDrop = true;
    this.myAggregationPipelineDiv.isReadOnly = true;
    this.myAggregationPipelineDiv.allowCopy = true;
    this.myAggregationPipelineDiv.allowDelete = true;
    this.myAggregationPipelineDiv.allowInsert = true;
    this.myAggregationPipelineDiv.allowMove = true;

    this.myDomainModelDiv.model.undoManager = this.myIntentFulfillmentDiv.model.undoManager;
    this.myDomainModelDiv.model.undoManager.isEnabled = true;
  }

  public getModelPOS() {
    if (this.trainedEntities != null && typeof this.trainedEntities !== 'undefined'
      && this.trainedEntities.length > 0) {
      return { 'Entities': this.trainedEntities, 'PosTree': this.posTree };
    } else {
      return { 'Entities': [{ 'entity_type': 'None', 'value': 'none' }], 'PosTree': this.posTree };
    }
  }

  public saveIntentffModel(event: any, implementModel: Boolean): void {
    const data = JSON.parse(this.myIntentFulfillmentDiv.model.toJson());
    const graphjson = {
      'node': data.nodeDataArray,
      'link': data.linkDataArray
    };

    this.alertService.clear();

    if (typeof this.intentfulfillmentModel.name !== 'undefined' && this.intentfulfillmentModel.name !== '') {
      this.intentfulfillmentModel.pos = this.getModelPOS();
      this.intentfulfillmentModel.graphjson = graphjson;
      this.intentfulfillmentModel.nodeproperties = [];

      if (!this.intentfulfillmentModel._id) {
        this.intentfulfillmentModel.createdby = this.username;

        this.intentFulfillmentModelService.addIntentFulfillmentModel(this.nlpmodelid, this.nlpintentid, this.intentfulfillmentModel)
          .subscribe(intentffmodel => {
            this.intentfulfillmentModel._id = intentffmodel['data']['_id'];
            // console.log('model added successfully =>', intentffmodel);
            this.alertService.success('IntentFulfillment Model added successfully');
            if (implementModel) {
              this.redirectToImplementation();
            } else {
              this.goBack();
            }
          });
      } else {
        this.intentfulfillmentModel.lastmodifiedby = this.username;
        this.intentFulfillmentModelService.updateIntentFulfillmentModel(this.nlpmodelid, this.nlpintentid, this.intentfulfillmentModel)
          .subscribe(intentffmodel => {
            // console.log('model updated successfully =>', intentffmodel);
            this.alertService.success('IntentFulfillment Model updated successfully');
            if (implementModel) {
              this.redirectToImplementation();
            } else {
              this.goBack();
            }
          });
      }
    } else {
      this.alertService.error('Please enter intent fulfillment model name');
      return;
    }
  }

  redirectToImplementation() {
    this.sharedService.FormData = {
      FulfillmentForm: {
        projname: this.projectName,
        modelid: this.nlpmodelid,
        modelname: this.modelName,
        intentcatname: this.intentCategoryName,
        intentid: this.nlpintentid
      }
    } as IFormData;

    this.router.navigate(['/intentimplementation'], {
      queryParams: {
        intentffmodelgraphid: this.intentfulfillmentModel._id,
        nlpmodel: this.nlpmodelid,
        nlpintent: this.nlpintentid
      }
    });
  }

  // const data = JSON.parse(this.myIntentFulfillmentDiv.model.toJson());
  // console.log(data.nodeDataArray);
  // console.log(data.linkDataArray);

  public loadModel(): void {
    this.intentFulfillmentModelService.getIntentffMappingByNLPModelIntent(this.nlpmodelid, this.nlpintentid).subscribe(
      mapping => {
        // console.log('mapping =>', mapping);
        if (mapping != null && mapping.data != null && mapping.data.length > 0) {
          const modelID = mapping.data[0].intentfulfillmentmodelgraphid;
          this.implementButtonDisabled = false;
          this.intentFulfillmentModelService.getIntentFulfillmentModelById(modelID)
            .subscribe(intentffmodel => {
              if (intentffmodel.data) {
                this.intentfulfillmentModel = intentffmodel.data;
                // Chnaged so that NLP Intent Category and Intentfulfillment Category Name are same.
                // Changes to be done to save this name alonwith NLP intent only, no need to save with fulfillment
                this.intentfulfillmentModel.name = this.intentCategoryName;
                this.myIntentFulfillmentDiv.model = new gojs.GraphLinksModel(
                  intentffmodel.data.graphjson.node,
                  intentffmodel.data.graphjson.link);
              }
            });
        }

      });
  }

  public renderModel(event): void {  // event will give you full breif of action
    const _id = event.target.value;
    const data = this.models.filter(item => item._id === _id)[0];

    this.myDomainModelDiv.model = new gojs.GraphLinksModel(data.graphjson.node, data.graphjson.link);
  }

  get UserName() {
    const userInfo = this.authService.getUserInfo();
    return userInfo != null ? userInfo.username : 'NA';
  }

  getPOS(): void {
    const self = this;
    this.articles = '';
    this.IntentScoreModel = [];
    self.textClassificationService.getIntentsByUser(this.inputUtterance, this.modelName, this.UserName).subscribe
      (
      response => {
        this.articles = JSON.parse(response.message);
        // console.log('NLP Response - ', this.articles);
        this.keys = Object.keys(this.articles);
        this.keys.forEach(key => {
          if (key.toLowerCase() === 'intents') {
            const item = this.articles[key];
            Object.keys(item).forEach(key1 => {
              const item1 = item[key1];
              Object.keys(item1).forEach(key2 => {
                this.IntentScoreModel.push({ key: key2, value: item1[key2] });
              });
            });
          }
        });
        console.log('IntentScoreModel - ', this.IntentScoreModel.sort(function (a, b) {
          if (a.value > b.value) {
            return -1;
          }
          if (a.value < b.value) {
            return 1;
          }
          return 0;
        }));
        this.IntentScoreModel = this.IntentScoreModel.slice(0, 3);
      },
      error => this.errorMessage = <any>error
      );
  }

  getAllModels(): void {
    this.domainmodelService.getAllDomainModels().subscribe(models => {
      this.models = models.data;
      this.changeDetectorRef.detectChanges();
    });
  }

  showModel() {

  }

  getIntent(pos: any): string {
    let intent = '';

    if (pos == null) {
      return intent;
    }

    if (pos.nouns != null && pos.nouns.length > 0) {
      intent += '<nouns>' + pos.nouns.map(x => x.value);
    }

    if (pos.adjectives != null && pos.adjectives.length > 0) {
      intent += '<adjectives>' + pos.adjectives.map(x => x.value);
    }

    if (pos.verbs != null && pos.verbs.length > 0) {
      intent += '<verbs>' + pos.verbs.map(x => x.value);
    }

    return intent;
  }

  goBack() {
    this.router.navigate(['/intentlist']);
  }
}
