// import { Subscription, Observable } from 'rxjs';
import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy } from '@angular/core';
import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';
// import { environment } from '../../../../environments/environment';

@Component({
  selector: 'mercer-intent-fulfillment-step3',
  moduleId: module.id.toString(),
  templateUrl: 'intent-fulfillment-step3.component.html'
})
export class IntentFulfillmentStep3Component {
}
