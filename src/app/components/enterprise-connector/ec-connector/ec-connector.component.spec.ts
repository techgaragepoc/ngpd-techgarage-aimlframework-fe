import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcConnectorComponent } from './ec-connector.component';

describe('EcConnectorComponent', () => {
  let component: EcConnectorComponent;
  let fixture: ComponentFixture<EcConnectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcConnectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcConnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
