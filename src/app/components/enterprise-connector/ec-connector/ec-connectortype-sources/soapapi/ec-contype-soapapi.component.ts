import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { SoapAPIConnection, Credentials } from '../../../../../shared/models';
import * as clone from 'clone';

@Component({
    selector: 'mercer-ec-contype-soapapi',
    templateUrl: './ec-contype-soapapi.component.html',
    // styleUrls: ['./ec-connector.component.scss']
})
export class EcContypeSoapAPIComponent implements OnInit, OnChanges {

    @Input() connectiondetail: any;
    @Output() onChange = new EventEmitter();

    public soapapi: SoapAPIConnection;

    constructor() {
        this.soapapi = new SoapAPIConnection();
        this.soapapi.credentials = new Credentials();
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        this.initialize();
    }

    initialize() {
        if (this.connectiondetail) {
            if (typeof this.connectiondetail !== 'object') {
                this.assignValues(JSON.parse(this.connectiondetail));
            } else {
                this.assignValues(this.connectiondetail);
            }
            // console.log('soap api before...', this.connectiondetail, Object.keys(this.soapapi),this.soapapi);

        }
    }

    assignValues(condtl) {
        if (condtl && condtl !== 'undefined') {
            this.soapapi.baseurl = condtl.baseurl;
            this.soapapi.proxy = condtl.proxy;

            if (condtl.credentials && condtl.credentials !== 'undefined') {

                this.soapapi.credentials.userid = (condtl.credentials.userid &&
                    condtl.credentials.userid !== 'undefined') ? condtl.credentials.userid : '';

                this.soapapi.credentials.password = (condtl.credentials.password &&
                    condtl.credentials.password !== 'undefined') ? condtl.credentials.password : '';
            }
        }
    }

    valuechanged() {
        this.onChange.emit(this.soapapi);
    }
}
