import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { OracleConnection } from '../../../../../shared/models';
import * as clone from 'clone';

@Component({
    selector: 'mercer-ec-contype-oracle',
    templateUrl: './ec-contype-oracle.component.html',
    // styleUrls: ['./ec-connector.component.scss']
})
export class EcContypeOracleComponent implements OnInit, OnChanges {

    @Input() connectiondetail: any;
    @Output() onChange = new EventEmitter();

    public oracle: OracleConnection;

    constructor() {
        this.oracle = new OracleConnection();
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        this.initialize();
    }

    initialize() {
        if (this.connectiondetail) {
            if (typeof this.connectiondetail !== 'object') {
                this.assignValues(JSON.parse(this.connectiondetail));
            } else {
                this.assignValues(this.connectiondetail);
            }
            // console.log('soap api before...', this.connectiondetail, Object.keys(this.soapapi),this.soapapi);

        }
    }

    assignValues(condtl) {
        if (condtl && condtl !== 'undefined') {
            this.oracle.connectstring = condtl.connectstring;
            this.oracle.user = condtl.user;
            this.oracle.password = condtl.password;
        }
    }

    valuechanged() {
        this.onChange.emit(this.oracle);
    }
}
