import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { SqlServerConnection } from '../../../../../shared/models';
import * as clone from 'clone';

@Component({
    selector: 'mercer-ec-contype-sqlserver',
    templateUrl: './ec-contype-sqlserver.component.html',
    // styleUrls: ['./ec-connector.component.scss']
})
export class EcContypeSqlServerComponent implements OnInit, OnChanges {
    @Input() connectiondetail: any;
    @Output() onChange = new EventEmitter();

    public sqlserver: SqlServerConnection;

    constructor() {
        this.sqlserver = new SqlServerConnection();
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        this.initialize();
    }

    initialize() {
        if (this.connectiondetail) {
            if (typeof this.connectiondetail !== 'object') {
                this.assignValues(JSON.parse(this.connectiondetail));
            }            else {
                this.assignValues(this.connectiondetail);
            }
        }
    }

    assignValues(condtl) {
        if (condtl && condtl !== 'undefined') {
            this.sqlserver.server = condtl.server;
            this.sqlserver.port = condtl.port;
            this.sqlserver.database = condtl.database;
            this.sqlserver.user = condtl.user;
            this.sqlserver.password = condtl.password;
            this.sqlserver.instancename = condtl.instancename;
        }
    }

    valuechanged() {
        this.onChange.emit(this.sqlserver);
    }
}
