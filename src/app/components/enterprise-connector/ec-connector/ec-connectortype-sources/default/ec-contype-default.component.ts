import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import * as clone from 'clone';

@Component({
    selector: 'mercer-ec-contype-default',
    templateUrl: './ec-contype-default.component.html',
    // styleUrls: ['./ec-connector.component.scss']
})
export class EcContypeDefaultComponent implements OnInit, OnChanges {

    @Input() connectiondetail: any;
    @Output() onChange = new EventEmitter();

    public jsonString: String;

    constructor() {
        this.jsonString = '{}';
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        this.initialize();
    }

    initialize() {
        if (this.connectiondetail) {

            if (typeof this.connectiondetail !== 'object') {
                this.assignValues(JSON.parse(this.connectiondetail));
            } else {
                this.assignValues(this.connectiondetail);
            }
        }
    }

    assignValues(condtl) {
        if (condtl && condtl !== 'undefined') {
            this.jsonString = JSON.stringify(this.connectiondetail);
        }
    }

    valuechanged(event: any) {
        this.onChange.emit(JSON.parse(this.jsonString.toString()));
    }
}
