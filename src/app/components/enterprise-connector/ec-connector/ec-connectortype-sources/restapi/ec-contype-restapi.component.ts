import { Component, OnInit, OnChanges, Input, Output, EventEmitter, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'mercer-ec-contype-restapi',
    templateUrl: './ec-contype-restapi.component.html',
    // styleUrls: ['./ec-connector.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.Default
})
export class EcContypeRestAPIComponent implements OnInit, OnChanges {
    @Input() connectiondetail: any;
    @Output() onChange = new EventEmitter();

    public restapi: any;

    constructor() {
        this.restapi = {
            'baseurl': '',
            'headers': {},
            'headersString': '',
            'proxy': '',
            'sessioninfo': {
                'options': {
                    'url': '',
                    'method': 'POST',
                    'headers': {},
                    'headersString': ''
                },
                'tokenkey': {
                    'inkey': '<key name received in response from server>',
                    'outkey': '<key name of auth token send in request to server>'
                }

            }
        };
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        this.initialize();
    }

    initialize() {
        if (this.connectiondetail) {
            if (typeof this.connectiondetail !== 'object') {
                this.assignValues(JSON.parse(this.connectiondetail));
            }            else {
                this.assignValues(this.connectiondetail);
            }
        }
    }

    assignValues(condtl) {
        if (condtl && condtl !== 'undefined') {

            this.restapi.baseurl = condtl.baseurl;
            this.restapi.proxy = condtl.proxy;
            this.restapi.headers = this.connectiondetail.headers;
            this.restapi.headersString = JSON.stringify(this.connectiondetail.headers);

            if (condtl.sessioninfo && condtl.sessioninfo !== 'undefined') {
                if (condtl.sessioninfo.options && condtl.sessioninfo.options !== 'undefined') {
                    this.restapi.sessioninfo.options.url = this.connectiondetail.sessioninfo.options.url;
                    this.restapi.sessioninfo.options.method = 'POST'; // this.connectiondetail.sessioninfo.options.method;
                    this.restapi.sessioninfo.options.headers = this.connectiondetail.sessioninfo.options.headers;
                    this.restapi.sessioninfo.options.headersString = JSON.stringify(this.connectiondetail.sessioninfo.options.headers);
                }

                if (condtl.sessioninfo.tokenkey && condtl.sessioninfo.tokenkey !== 'undefined') {
                    this.restapi.sessioninfo.tokenkey.inkey = this.connectiondetail.sessioninfo.tokenkey.inkey;
                    this.restapi.sessioninfo.tokenkey.outkey = this.connectiondetail.sessioninfo.tokenkey.outkey;
                }
            }
        }
    }

    valuechanged() {
        this.restapi.headers = JSON.parse(this.restapi.headersString);
        this.restapi.sessioninfo.options.headers = JSON.parse(this.restapi.sessioninfo.options.headersString);
        this.onChange.emit(this.restapi);
    }
}
