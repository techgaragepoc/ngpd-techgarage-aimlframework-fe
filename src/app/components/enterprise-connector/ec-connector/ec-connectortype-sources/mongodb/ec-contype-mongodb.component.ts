import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { MongodbConnection } from '../../../../../shared/models';
import * as clone from 'clone';

@Component({
    selector: 'mercer-ec-contype-mongodb',
    templateUrl: './ec-contype-mongodb.component.html',
    // styleUrls: ['./ec-connector.component.scss']
})
export class EcContypeMongDBComponent implements OnInit, OnChanges {

    @Input() connectiondetail: any;
    @Output() onChange = new EventEmitter();

    public mongodb: MongodbConnection;

    constructor() {
        this.mongodb = new MongodbConnection();
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        this.initialize();
    }

    initialize() {
        if (this.connectiondetail) {

            if (typeof this.connectiondetail !== 'object') {
                this.assignValues(JSON.parse(this.connectiondetail));
            } else {
                this.assignValues(this.connectiondetail);
            }
        }
    }

    assignValues(condtl) {
        if (condtl && condtl !== 'undefined') {
            this.mongodb.url = condtl.url;
            this.mongodb.dbname = condtl.dbname;
            this.mongodb.user = condtl.user;
            this.mongodb.password = condtl.password;
        }
    }

    valuechanged() {
        this.onChange.emit(this.mongodb);
    }
}
