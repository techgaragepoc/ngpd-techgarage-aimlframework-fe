import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AuthenticationService, AlertService, ConnectorService, ConnectorTypeService, DataService } from '../../../shared/service';
import { User, Connector, ConnectorType, TestConnection, PaginationInfo } from '../../../shared/models';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as clone from 'clone';

@Component({
  selector: 'mercer-ec-connector',
  templateUrl: './ec-connector.component.html',
  styleUrls: ['./ec-connector.component.scss']
})
export class EcConnectorComponent implements OnInit {

  currentUser: User;
  username: String = '';

  public userlist: any[] = [];
  public connectors: Connector[];
  public connectortypes: ConnectorType[];
  public connector: Connector;
  public connectorForm: FormGroup;
  public actionIcon: String = 'delete';
  public defaultconnectortype: Boolean = false;
  public testconnectionmsgclass: String = '';

  public paginationInfo: PaginationInfo;
  public stripedrowData: any[] = [];
  public popuptitle: String = 'Add New Connector';

  public connectorheaderData: any[] = [
    {
      name: 'Connector Name',
      column: 'connectorname'
    },
    {
      name: 'Type',
      column: 'type'
    },
    {
      name: 'Connection Detail',
      column: 'connectiondetail'
    },
    {
      name: 'Is System Defined?',
      column: 'sysdefined'
    },
    {
      name: 'Actions',
      disableSort: true,
      column: 'actionIcon',
      textAlign: 'center'
    },
  ];

  constructor(
    private authService: AuthenticationService,
    private alertService: AlertService,
    private connectortypeService: ConnectorTypeService,
    private connectorService: ConnectorService,
    private dataservice: DataService,
    private changeDetectorRefs: ChangeDetectorRef) {

    this.paginationInfo = {
      offset: 0,
      limit: 5,
      limits: [5, 10, 20, 30],
      totalCount: 0
    };

    this.currentUser = this.authService.getUserInfo();
    this.username = (this.currentUser != null && this.currentUser.username !== 'undefined') ? this.currentUser.username : 'none';

    this.defaultconnectortype = false;
    this.connectors = [];
    this.connectortypes = [];
    this.connector = new Connector();
    this.connector.testconnectionresult = new TestConnection();
    this.connectorForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      connectortype: new FormControl(null, Validators.required),
      sytemDefined: new FormControl(null, null)
    });
  }

  ngOnInit() {
    this.getAllConnectors();
  }


  getAllConnectors() {
    // first get the connector types
    this.connectortypeService.getAllConnectorType().
      subscribe(cntyps => {
        console.log('connectortypes...', cntyps.data);
        if (cntyps && cntyps.data) {
          this.connectortypes = cntyps.data;

          // after getting connector types, lets get the connectors
          this.populateConnectors();
        }
      });
  }

  populateConnectors() {
    this.stripedrowData = [];
    this.connectorService.getAllConnectors().
      subscribe(cons => {
        console.log('connectors...', cons.data);
        this.connectors = cons.data;
        for (let i = 0; i < this.connectors.length; i++) {
          const contype = this.connectortypes.find(c => c.connectortypeid === this.connectors[i].connectortypeid);
          if (contype && contype !== 'undefined') {
            this.connectors[i].connectortype = clone(contype);
          }
          this.connectors[i].connectiondetailAsObject = JSON.parse(this.connectors[i].connectiondetail.toString());
        }

        this.stripedrowData = this.connectors.slice(0, this.paginationInfo.limit);
        this.paginationInfo = {
          offset: 0,
          limit: this.paginationInfo.limit,
          limits: this.paginationInfo.limits,
          totalCount: this.connectors.length
        };

      });
  }

  connectortypechange() {
    this.connector.connectortype = this.connectortypes.find(c => c.connectortypeid === this.connector.connectortypeid);
    console.log(this.connector.connectortype.subcategory);
    this.changeDetectorRefs.detectChanges();
    // this.resetsource = false;
  }

  public updatePage(model) {
    this.paginationInfo.limit = model.limit;
    this.paginationInfo.offset = model.offset;

    if (model.offset === 0) {
      this.stripedrowData = this.connectors.slice(0, model.limit);
    } else {
      this.stripedrowData = this.connectors.slice(
        model.offset,
        model.offset + model.limit
      );
    }
  }

  updateConnectionDetail(event: any) {
    console.log('connection detail changed...', event);
    this.connector.connectiondetailAsObject = event;
    this.connector.connectiondetail = JSON.stringify(this.connector.connectiondetailAsObject);
  }

  validateconnector(): Boolean {
    this.alertService.clear();
    this.connector.testconnectionresult = new TestConnection();
    this.connector.testconnectionresult.isConnected = false;
    this.connector.testconnectionresult.details = '';

    console.log('validating connector...', this.connector);
    return this.isformValid();
  }

  saveconnector(popupwindow: any) {
    this.alertService.clear();
    console.log('saving connector...', this.connector);

    if (this.validateconnector()) {
      if (this.isNullOrEmpty(this.connector.connectorid)) {
        this.saveNewConnector(popupwindow);
      } else {
        this.editExistingConnector(popupwindow);
      }
    }
  }

  saveNewConnector(popupwindow: any) {
    this.connector.createdby = this.username;
    this.connectorService.createConnector(this.connector).subscribe(
      con => {
        console.log(con);
        this.resetconnector();
        this.getAllConnectors();
        this.alertService.success('Connector successfully created.');
        popupwindow.close();
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  editExistingConnector(popupwindow: any) {
    this.connector.lastmodifiedby = this.username;
    // Need to check if there is already a connector present with a same name if connector name is edited
    this.connectorService.updateConnector(this.connector).subscribe(
      con => {
        console.log(con);
        this.resetconnector();
        this.getAllConnectors();
        this.alertService.success('Connector successfully Updated.');
        popupwindow.close();
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  resetconnector() {
    this.alertService.clear();
    this.connectorForm.reset();
    this.popuptitle = 'Add New Connector';

    this.connector = new Connector();
    this.connector.connectortypeid = '';
    console.log('new connector...', this.connector);
    this.changeDetectorRefs.detectChanges();
  }

  checkConnectorTypeSupport() {
    if (this.connector && this.connector.connectortype
      && this.connector.connectortype.subcategory.toLowerCase() === 'restapi') {
      this.defaultconnectortype = true;
    } else {
      this.defaultconnectortype = false;
    }
  }

  getConnectorDataTransform(connectordata) {
    let connector = new Connector();
    connector.testconnectionresult = new TestConnection();

    if (connectordata && connectordata !== 'undefined') {
      connector = connectordata;

      connector.connectiondetailAsObject = JSON.parse(connector.connectiondetail.toString());
      connector.connectortype = this.connectortypes.find(ct => ct.connectortypeid === connector.connectortypeid);
    }

    return connector;
  }

  edit(connectorid: String, istestconnection: Boolean) {
    this.alertService.clear();
    this.popuptitle = 'Edit Connector';
    if (!this.isNullOrEmpty(connectorid)) {
      this.connectorService.getConnectorById(connectorid).subscribe(
        con => {
          this.connector = this.getConnectorDataTransform(con.data);
          this.connectortypechange();
          // this.checkConnectorTypeSupport();
          console.log('selected connector...', this.connector);
          this.changeDetectorRefs.detectChanges();
          if (istestconnection) {
            this.testconnection();
          }
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
  }

  delete(connectorid: String, name: String) {
    this.alertService.clear();
    if (confirm('Are you sure to delete ' + name)) {
      this.connectorService.deleteConnector(connectorid).subscribe(
        contype => {
          this.getAllConnectors();
          this.alertService.success('Connector successfully deleted.');
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
  }

  testconnection() {
    this.alertService.clear();
    console.log('testing connector...', this.connector);

    if (this.validateconnector()) {
      this.connector.testconnectionresult.details = 'Trying to establishing a connection for test...';
      this.testconnectionmsgclass = 'testconnectionmsg';
      this.changeDetectorRefs.detectChanges();

      this.dataservice.testconnection(this.connector).subscribe(response => {

        if (this.isSameConnector(this.connector.connectiondetailAsObject, response.result.connectioninfo.connectiondetail)) {
          console.log('test connection result...', response.result);
          this.connector.testconnectionresult.details = (response.result.success) ? 'Connection established successfully'
            : 'Error occured while establishing connection - '
            + JSON.stringify(response.result.error);
          this.connector.testconnectionresult.isConnected = response.result.success;
          this.testconnectionmsgclass = (response.result.success) ? 'testconnectionmsg-success' : 'testconnectionmsg-error';
          this.changeDetectorRefs.detectChanges();
        }

      },
        err => {
          this.connector.testconnectionresult.details = 'Error occured while establishing connection - ' + JSON.stringify(err);
          this.connector.testconnectionresult.isConnected = false;
          this.testconnectionmsgclass = 'testconnectionmsg-error';
          this.changeDetectorRefs.detectChanges();
        }
      );
    } else {
      this.connector.testconnectionresult.details = 'Please verify the input values - validation failed';
      this.connector.testconnectionresult.isConnected = false;
      this.testconnectionmsgclass = 'testconnectionmsg-error';
    }
  }


  isSameConnector(conndtl1, conndtl2) {
    console.log('connection details...', conndtl1, conndtl2);

    if (typeof conndtl1 === 'object' && typeof conndtl2 === 'object') {
      if (JSON.stringify(conndtl1) === JSON.stringify(conndtl2)) {
        return true;
      }
    }

    return false;

  }

  isformValid() {
    let isvalid = true;
    const errmessages = [];

    if (this.isNullOrEmpty(this.connectorForm.get('name').value)) {
      errmessages.push('Connector name cannot be left blank');
      isvalid = false;
    }

    if (this.isNullOrEmpty(this.connectorForm.get('connectortype').value)) {
      errmessages.push('Connectortype needs to be selected');
      isvalid = false;
    }

    if (this.isNullOrEmpty(this.connector.connectiondetail)) {
      errmessages.push('Connection Detail cannot be left blank');
      isvalid = false;
    }

    if (!isvalid) {
      errmessages.forEach(element => {
        this.alertService.error('Error' + element);
      });
    }
    return isvalid;
  }

  isNullOrEmpty(val: String) {
    if (val == null || val === '' || val === 'undefined') {
      return true;
    } else {
      return false;
    }
  }
}
