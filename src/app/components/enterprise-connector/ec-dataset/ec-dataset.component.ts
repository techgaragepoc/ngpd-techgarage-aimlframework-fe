import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  AuthenticationService, AlertService,
  ConnectorService, ConnectorTypeService, DataSetService, DataService
} from '../../../shared/service';
import { User, Connector, ConnectorType, DataSet, PaginationInfo } from '../../../shared/models';
import * as clone from 'clone';

@Component({
  selector: 'mercer-ec-dataset',
  templateUrl: './ec-dataset.component.html',
  styleUrls: ['./ec-dataset.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})
export class EcDatasetComponent implements OnInit, OnDestroy {

  currentUser: User;
  username: String = '';

  public connectors: Connector[];
  public datasets: DataSet[];
  public dataset: DataSet;
  public datasetForm: FormGroup;
  public resetsource: Boolean = false;
  public connectortypes: ConnectorType[];
  public filterDataSets: DataSet[];

  public parameters: any[] = [];
  public previewstatus: String = '';
  public previewresult: String = '';
  public previewresultstyle: String = 'mos-u-resize--vertical preview-result preview-result-success';

  public paginationInfo: PaginationInfo;
  public stripedrowData: any[] = [];
  public popuptitle: String = 'Add New Dataset';


  public datasetheaderData: any[] = [
    {
      name: 'DataSet Name',
      column: 'datasetname'
    },
    {
      name: 'Connector',
      column: 'connectorname'
    },
    {
      name: 'Connector Type',
      column: 'table'
    },
    {
      name: 'Is Public?',
      column: 'ispublic'
    },
    {
      name: 'Is System Defined?',
      column: 'sysdefined'
    },
    {
      name: 'Actions',
      disableSort: true,
      column: 'actionIcon',
      textAlign: 'center'
    },
  ];

  constructor(private authService: AuthenticationService,
    private alertService: AlertService,
    private datasetService: DataSetService,
    private connectorService: ConnectorService,
    private connectortypeService: ConnectorTypeService,
    private dataService: DataService,
    private changedetectorRef: ChangeDetectorRef) {

    this.paginationInfo = {
      offset: 0,
      limit: 5,
      limits: [5, 10, 20, 30],
      totalCount: 0
    };

    this.currentUser = this.authService.getUserInfo();
    this.connectortypes = [];
    this.connectors = [];
    this.datasets = [];
    this.filterDataSets = [];
    this.paginationInfo = new PaginationInfo();
    this.paginationInfo.limit = 5;
    this.paginationInfo.limits = [5, 10, 20];
    this.paginationInfo.offset = 0;
    this.paginationInfo.totalCount = 5;

    console.log('current user...', this.currentUser);
    this.username = (this.currentUser != null && this.currentUser.username !== 'undefined') ? this.currentUser.username : 'none';

    this.initializedataset();

    this.datasetForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      connector: new FormControl(null, Validators.required),
      source: new FormControl(null),
      isPublic: new FormControl(null, null),
      sytemDefined: new FormControl(null, null)
    });
  }

  ngOnInit() {
    const self = this;
    this.getAllRequiredData(function () {
      self.filterResult();
    });
  }

  ngOnDestroy() {

  }

  initializedataset() {
    this.dataset = new DataSet();
    this.dataset.connector = new Connector();
    this.dataset.connector.connectortype = new ConnectorType();
  }

  getAllRequiredData(callback) {
    this.populateConnectortypes(callback);
  }

  populateDataSets(callback) {
    this.stripedrowData = [];
    this.datasetService.getAllDataSet().subscribe(ds => {
      console.log('existing datasets...', ds);
      if (ds && ds.data && ds.data.length > 0) {
        this.datasets = ds.data;
        for (let i = 0; i < this.datasets.length; i++) {
          const con = this.connectors.find(c => c.connectorid === this.datasets[i].connectorid);
          if (con && con !== 'undefined') {
            this.datasets[i].connector = clone(con);
          }
          this.datasets[i].sourceAsObject = JSON.parse(this.datasets[i].source.toString());
        }
      }
      callback();
    });
  }

  populateConnectors(callback) {
    this.connectorService.getAllConnectors().subscribe(cn => {
      console.log('connectors..', cn);
      if (cn && cn.data && cn.data.length > 0) {
        this.connectors = cn.data;
      }

      for (let i = 0; i < this.connectors.length; i++) {
        const contype = this.connectortypes.find(c => c.connectortypeid === this.connectors[i].connectortypeid);
        if (contype && contype !== 'undefined') {
          this.connectors[i].connectortype = clone(contype);
        }
      }

      this.populateDataSets(callback);
    });
  }

  populateConnectortypes(callback) {
    this.connectortypeService.getAllConnectorType().subscribe(cntyps => {
      console.log('connector types => ', cntyps);
      if (cntyps && cntyps.data && cntyps.data.length > 0) {
        this.connectortypes = cntyps.data;
      }
      this.populateConnectors(callback);
    });
  }

  public updatePage(model) {
    this.paginationInfo.limit = model.limit;
    this.paginationInfo.offset = model.offset;

    if (model.offset === 0) {
      this.stripedrowData = this.datasets.slice(0, model.limit);
    } else {
      this.stripedrowData = this.datasets.slice(
        model.offset,
        model.offset + model.limit
      );
    }
  }

  filterResult() {

    this.stripedrowData = this.datasets.slice(0, this.paginationInfo.limit);
    this.paginationInfo = {
      offset: 0,
      limit: this.paginationInfo.limit,
      limits: this.paginationInfo.limits,
      totalCount: this.datasets.length
    };
    this.changedetectorRef.detectChanges();
  }

  updatesource(event: any) {
    console.log('source value changed...', event);
    this.dataset.sourceAsObject = event;
  }

  presaveprocessing() {
    if (this.dataset && this.dataset.connector && this.dataset.connector.connectortype
      && this.dataset.connector.connectortype.category !== 'undefined'
      && this.dataset.connector.connectortype.category.toLowerCase() === 'rdbms'
      && this.dataset.source && this.dataset.sourceAsObject.sourcetype) {
      if (this.dataset.sourceAsObject.sourcetype === 'sql') {
        this.dataset.sourceAsObject.table = null;
        this.dataset.sourceAsObject.columns = null;
        this.dataset.sourceAsObject.procedure = { name: '', parameters: [] };
      } else {
        if (this.dataset.sourceAsObject.sourcetype === 'table') {
          this.dataset.sourceAsObject.sql = null;
          this.dataset.sourceAsObject.procedure = { name: '', parameters: [] };
        } else {
          if (this.dataset.sourceAsObject.sourcetype === 'proc') {
            this.dataset.sourceAsObject.table = null;
            this.dataset.sourceAsObject.columns = null;
            this.dataset.sourceAsObject.sql = null;
          }
        }
      }
    }
  }

  savedataset(popupwindow: any) {
    this.alertService.clear();
    this.presaveprocessing();
    this.dataset.source = JSON.stringify(this.dataset.sourceAsObject);
    console.log('saving dataset...', this.dataset);
    const self = this;

    if (this.isInputValid()) {
      // updating existing dataset
      if (this.dataset.datasetid && this.dataset.datasetid !== '') {
        this.dataset.lastmodifiedby = this.username;
        this.datasetService.updateDataSet(this.dataset).subscribe(
          ds => {
            console.log('dataset updated successfully!!');
            self.alertService.success('DataSet has been updated successfully!!');
            self.getAllRequiredData(function () {
              self.filterResult();
              popupwindow.close();
              // this.changedetectorRef.detectChanges();
            });
          },
          err => {
            console.log('Error occured while updating dataset ', err);
            self.alertService.error('Error occured while updating dataset');
          }
        );
      } else {  // creating new dataset
        this.dataset.createdby = this.username;
        this.datasetService.createDataSet(this.dataset).subscribe(
          ds => {
            console.log('dataset saved successfully!!');
            self.alertService.success('DataSet has been added successfully!!');
            self.getAllRequiredData(function () {
              self.filterResult();
              popupwindow.close();
              // this.changedetectorRef.detectChanges();
            });
          },
          err => {
            console.log('Error occured while saving dataset ', err);
            self.alertService.error('Error occured while adding dataset');
          }
        );
      }
    }
  }

  resetdataset() {
    this.alertService.clear();
    this.datasetForm.reset();
    this.initializedataset();
    this.resetsource = true;
    this.popuptitle = 'Add New Dataset';
  }


  connectorchange() {
    if (this.dataset.connectorid!='') { 
    this.dataset.connector = this.connectors.find(c => c.connectorid === this.dataset.connectorid);
    this.resetsource = false;
    }
    else {
      this.dataset.connector = new Connector();
      this.dataset.connector.connectortype = new ConnectorType();
      this.dataset.connector.connectortype.category='';
    }
  }

  editdataset(datasetid: String, fordatapreview: Boolean) {
    this.alertService.clear();
    this.popuptitle = 'Edit Dataset';
    this.dataset = clone(this.datasets.find(ds => ds.datasetid === datasetid));
    console.log('selected dataset for editing...', this.dataset);
    if (fordatapreview) {
      this.extractParameters();
    }
  }

  deletedataset(datasetid: String) {
    this.alertService.clear();
    const self = this;
    this.datasetService.deleteDataSet(datasetid).subscribe(
      ds => {
        self.dataset = ds.data;
        self.alertService.success('DataSet has been deleted successfully');
        self.getAllRequiredData(function () {
          self.filterResult();
        });
      },
      err => {
        console.log('error while deleting datset ...', datasetid, err);
        self.alertService.error('Error occured while deleting dataset');
      }
    );
  }

  extractParameters() {
    this.previewstatus = 'Please click on Execute button...';
    this.previewresult = '';
    this.parameters = [];
    let start = false;
    let end = false;
    let varname = '';

    if (this.dataset && this.dataset.source &&
      this.dataset.source !== 'undefined') {
      for (let i = 0; i < this.dataset.source.length; i++) {
        const stringChar = this.dataset.source.charAt(i);
        // console.log(i, stringChar)
        if (stringChar === '#') {
          end = (start && !end) ? true : end;
          start = (!start) ? true : start;
        }

        if (start && !end && stringChar !== '#') {
          varname += stringChar;
        }

        if (start && end) {
          this.parameters.push({ 'name': varname, 'value': '' });
          start = false;
          end = false;
          varname = '';
        }
      }
    }

    // console.log('source string...', this.dataset.sourcestring);
    // console.log('extracted parameters...', this.parameters);
    this.changedetectorRef.detectChanges();
  }

  paramValueChange(param, event) {
    this.previewstatus = 'Please click on Execute button...';
    this.previewresult = '';
    console.log(param, event.target.value);
    param.value = event.target.value;
  }

  datapreview() {
    const params = {};
    for (let i = 0; i < this.parameters.length; i++) {
      params[this.parameters[i].name] = this.parameters[i].value;
    }

    this.previewstatus = 'Fetcing data...';
    this.previewresult = '';
    this.previewresultstyle = 'mos-u-resize--vertical preview-result preview-result-success';
    console.log('data preview params...', this.dataset.name, params);
    this.dataService.getdata(this.dataset.datasetid, this.dataset.name, params, 5).subscribe(resp => {

      console.log('data for preview...', resp);
      this.previewstatus = 'Result...';
      this.previewresult = JSON.stringify(resp.data);
    },
      err => {
        this.previewstatus = 'Error...';
        this.previewresultstyle = 'mos-u-resize--vertical preview-result preview-result-error';
        this.previewresult = JSON.stringify(err);
      }
    );
  }

  copyResult(inputElement) {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

  isInputValid(): Boolean {
    return true;
  }
}
