import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcDatasetComponent } from './ec-dataset.component';

describe('EcDatasetComponent', () => {
  let component: EcDatasetComponent;
  let fixture: ComponentFixture<EcDatasetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcDatasetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcDatasetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
