import {
    Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
    Input, Output, EventEmitter, OnChanges, SimpleChanges
} from '@angular/core';

@Component({
    selector: 'mercer-ec-dataset-source-rdbms',
    templateUrl: './ec-dataset-source-rdbms.component.html',
    styleUrls: ['../ec-dataset-source.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.Default
})
export class EcDatasetSrcRDBMSComponent implements OnInit, OnChanges {

    public procparamscommaseparated: String;
    public rdbmsSource: any;

    @Input() reset: Boolean;
    @Input() source: any;
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
        this.initialize();
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        // if (this.reset) {
        this.initialize();
        // }
    }

    initialize() {
        this.rdbmsSource = {
            'sourcetype': 'table',
            'schema': '',
            'table': '',
            'columns': '',
            'filter': '',
            'sql': '',
            'procedure': {
                'name': '',
                'parameters': []
            }
        };
        this.procparamscommaseparated = '';

        console.log('initialize..', this.source);
        if (this.source && this.source !== 'undefined') {
            if (typeof this.source !== 'object') {
                this.source = JSON.parse(this.source);
            }

            const templatekeys = Object.keys(this.rdbmsSource);
            const inputkeys = Object.keys(this.source);

            for (let i = 0; i < templatekeys.length; i++) {
                const tmpval = this.rdbmsSource[templatekeys[i]];

                for (let j = 0; j < inputkeys.length; j++) {
                    const inpval = this.source[inputkeys[j]];

                    if (templatekeys[i] === inputkeys[j]) {
                        this.rdbmsSource[templatekeys[i]] = inpval;
                        break;
                    }
                }
            }

            if (this.rdbmsSource.sql && this.rdbmsSource.sql !== '' && this.rdbmsSource.sql !== 'undefined') {
                this.rdbmsSource.sourcetype = 'sql';
            } else {
                if (this.rdbmsSource.table && this.rdbmsSource.table !== '' && this.rdbmsSource.table !== 'undefined') {
                    this.rdbmsSource.sourcetype = 'table';
                } else {
                    this.rdbmsSource.sourcetype = 'proc';

                    // convert list to comma separated values
                    if (this.rdbmsSource.procedure && this.rdbmsSource.procedure !== 'undefined'
                        && this.rdbmsSource.procedure.parameters !== 'undefined' && this.rdbmsSource.procedure.parameters.length > 0) {
                        this.procparamscommaseparated = '';
                        for (let i = 0; i < this.rdbmsSource.procedure.parameters.length; i++) {
                            this.procparamscommaseparated += (this.procparamscommaseparated !== '') ? ',' : '';
                            this.procparamscommaseparated += this.rdbmsSource.procedure.parameters[i];
                        }
                    }
                }

            }
        }

    }

    valuechange(event: any) {
        console.log(event.target.value, this.rdbmsSource);

        if (this.rdbmsSource.procedure && this.rdbmsSource.procedure !== 'undefined'
            && this.procparamscommaseparated !== 'undefined' && this.procparamscommaseparated !== '') {
            this.rdbmsSource.procedure.parameters = this.procparamscommaseparated.split(',');
        }

        this.onChange.emit(this.rdbmsSource);
    }
}
