import {
    Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
    Input, Output, EventEmitter, OnChanges, SimpleChanges
} from '@angular/core';

@Component({
    selector: 'mercer-ec-dataset-source-restapi',
    templateUrl: './ec-dataset-source-restapi.component.html',
    styleUrls: ['../ec-dataset-source.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.Default
})
export class EcDatasetSrcRestAPIComponent implements OnInit, OnChanges {

    public restapiSource: any;
    public headersAsString: String;

    @Input() reset: Boolean;
    @Input() source: any;
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
        this.initialize();
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        // if (this.reset) {
        this.initialize();
        // }
    }

    initialize() {
        this.restapiSource = {
            'url': '',
            'method': '',
            'headers': {},
        };
        this.headersAsString = '';

        console.log('initialize..', this.source);
        if (this.source && this.source !== 'undefined') {
            if (typeof this.source !== 'object') {
                this.source = JSON.parse(this.source);
            }

            const templatekeys = Object.keys(this.restapiSource);
            const inputkeys = Object.keys(this.source);

            for (let i = 0; i < templatekeys.length; i++) {
                const tmpval = this.restapiSource[templatekeys[i]];

                for (let j = 0; j < inputkeys.length; j++) {
                    const inpval = this.source[inputkeys[j]];

                    if (templatekeys[i] === inputkeys[j]) {
                        this.restapiSource[templatekeys[i]] = inpval;
                        if (templatekeys[i] === 'headers' && typeof inpval === 'object') {
                            this.headersAsString = JSON.stringify(inpval);
                        }
                        break;
                    }
                }
            }
        }
    }

    valuechange(event: any) {
        console.log(event.target.value, this.restapiSource);
        this.restapiSource.headers = JSON.parse(this.headersAsString.toString());
        this.onChange.emit(this.restapiSource);
    }
}
