import {
    Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
    Input, Output, EventEmitter, OnChanges, SimpleChanges
} from '@angular/core';

@Component({
    selector: 'mercer-ec-dataset-source-mongo',
    templateUrl: './ec-dataset-source-mongo.component.html',
    styleUrls: ['../ec-dataset-source.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.Default
})
export class EcDatasetSrcMongoComponent implements OnInit, OnChanges {

    public mongodbSource: any;

    @Input() reset: Boolean;
    @Input() source: any;
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
        this.initialize();
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        // if (this.reset) {
        this.initialize();
        // }
    }

    initialize() {
        this.mongodbSource = {
            'collection': '',
            'filtercriteria': '',
        };

        console.log('initialize..', this.source);
        if (this.source && this.source !== 'undefined') {
            if (typeof this.source !== 'object') {
                this.source = JSON.parse(this.source);
            }

            const templatekeys = Object.keys(this.mongodbSource);
            const inputkeys = Object.keys(this.source);

            for (let i = 0; i < templatekeys.length; i++) {
                const tmpval = this.mongodbSource[templatekeys[i]];

                for (let j = 0; j < inputkeys.length; j++) {
                    const inpval = this.source[inputkeys[j]];

                    if (templatekeys[i] === inputkeys[j]) {
                        this.mongodbSource[templatekeys[i]] = inpval;
                        break;
                    }
                }
            }
        }
    }

    valuechange(event: any) {
        console.log(event.target.value, this.mongodbSource);
        this.onChange.emit(this.mongodbSource);
    }
}
