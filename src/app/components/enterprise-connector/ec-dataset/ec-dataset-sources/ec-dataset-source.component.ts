import {
  Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
  Input, Output, EventEmitter, OnChanges, SimpleChanges
} from '@angular/core';

@Component({
  selector: 'mercer-ec-dataset-source',
  templateUrl: './ec-dataset-source.component.html',
  styleUrls: ['./ec-dataset-source.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.Default
})
export class EcDatasetSrcComponent implements OnInit, OnChanges {

  public procparamscommaseparated: String;
  public rdbmsSource: any;

  @Input() reset: Boolean;
  @Input() source: any;
  @Input() category: String;
  @Input() subcategory: String;
  @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
    this.initialize();
  }

  ngOnInit() {
    this.initialize();
  }

  ngOnChanges() {
    // if (this.reset) {
    this.initialize();
    // }
  }

  initialize() {

  }

  valuechange(event: any) {
    this.onChange.emit(event);
  }
}

