import {
    Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
    Input, Output, EventEmitter, OnChanges, SimpleChanges
} from '@angular/core';

@Component({
    selector: 'mercer-ec-dataset-source-soapapi',
    templateUrl: './ec-dataset-source-soapapi.component.html',
    styleUrls: ['../ec-dataset-source.component.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.Default
})
export class EcDatasetSrcSoapAPIComponent implements OnInit, OnChanges {

    public soapapiSource: any;
    public argumentsAsString: String;

    @Input() reset: Boolean;
    @Input() source: any;
    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
        this.initialize();
    }

    ngOnInit() {
        this.initialize();
    }

    ngOnChanges() {
        // if (this.reset) {
        this.initialize();
        // }
    }

    initialize() {
        this.soapapiSource = {
            'url': '',
            'methodname': '',
            'arguments': {},
        };
        this.argumentsAsString = '';

        console.log('initialize..', this.source);
        if (this.source && this.source !== 'undefined') {
            if (typeof this.source !== 'object') {
                this.source = JSON.parse(this.source);
            }

            const templatekeys = Object.keys(this.soapapiSource);
            const inputkeys = Object.keys(this.source);

            for (let i = 0; i < templatekeys.length; i++) {
                const tmpval = this.soapapiSource[templatekeys[i]];

                for (let j = 0; j < inputkeys.length; j++) {
                    const inpval = this.source[inputkeys[j]];
                    if (templatekeys[i] === inputkeys[j]) {
                        this.soapapiSource[templatekeys[i]] = inpval;

                        if (templatekeys[i] === 'arguments' && typeof inpval === 'object') {
                            this.argumentsAsString = JSON.stringify(inpval);
                        }
                        break;
                    }
                }
            }
        }

    }

    valuechange(event: any) {
        console.log(event.target.value, this.soapapiSource);
        this.soapapiSource.arguments = JSON.parse(this.argumentsAsString.toString());
        this.onChange.emit(this.soapapiSource);
    }
}
