import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcConnectortypeComponent } from './ec-connectortype.component';

describe('EcConnectortypeComponent', () => {
  let component: EcConnectortypeComponent;
  let fixture: ComponentFixture<EcConnectortypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcConnectortypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcConnectortypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
