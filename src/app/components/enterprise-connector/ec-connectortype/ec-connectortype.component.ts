import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {
  AuthenticationService,
  AlertService,
  ConnectorTypeService
} from '../../../shared/service';
import { User, PaginationInfo, ConnectorType } from '../../../shared/models';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'mercer-ec-connectortype',
  templateUrl: './ec-connectortype.component.html',
  styleUrls: ['./ec-connectortype.component.scss']
})
export class EcConnectortypeComponent implements OnInit {
  currentUser: User;
  username: String = '';

  public connectortypeheaderData: any[] = [
    {
      name: 'Connector Type',
      column: 'connectortype',
      disableSort: true
    },
    {
      name: 'Category',
      column: 'category'
    },
    {
      name: 'Sub Category',
      column: 'subcategory'
    },
    {
      name: 'Is System Defined',
      column: 'systemdefined'
    },
    {
      name: 'Actions',
      disableSort: true,
      column: 'actionIcon',
      textAlign: 'center'
    }
  ];

  public paginationInfo: PaginationInfo;
  public stripedrowData: any[] = [];
  public popuptitle: String = 'Add New Connector Type';

  public connectortypes: ConnectorType[];
  public connectortype: ConnectorType;

  public catgorygroup: any[] = [];
  public Categories: string[];
  public SubCategories: string[];

  public connectortypeForm: FormGroup;

  constructor(
    private authService: AuthenticationService,
    private alertService: AlertService,
    private connectortypeService: ConnectorTypeService,
    private changeDetectorRefs: ChangeDetectorRef
  ) {
    this.paginationInfo = {
      offset: 0,
      limit: 5,
      limits: [5, 10, 20, 30],
      totalCount: 0
    };
    this.currentUser = this.authService.getUserInfo();
    this.username =
      this.currentUser !== null && this.currentUser.username !== 'undefined'
        ? this.currentUser.username : 'none';

    this.connectortypes = [];
    this.connectortype = new ConnectorType();
    this.connectortypeForm = new FormGroup({
      name: new FormControl(null, Validators.required),
      category: new FormControl(null, Validators.required),
      subcategory: new FormControl(null, Validators.required),
      version: new FormControl(null, Validators.required),
      provider: new FormControl(null, Validators.required),
      sytemDefined: new FormControl(null, null),
    });

    this.catgorygroup = [
      {
        category: 'rdbms',
        subcategory: ['sqlserver', 'oracle']
      },
      {
        category: 'nosql',
        subcategory: ['mongodb']
      },
      {
        category: 'webapi',
        subcategory: ['restapi', 'soapapi']
      }
    ];
  }

  ngOnInit() {
    this.getAllConnectorTypes();
    this.populateCategories();
    this.alertService.clear();
  }

  getAllConnectorTypes() {
    this.connectortypeService.getAllConnectorType().subscribe(
      contype => {
        this.connectortypes = contype.data;
        this.stripedrowData = contype.data.slice(0, this.paginationInfo.limit);
        this.paginationInfo = {
          offset: 0,
          limit: this.paginationInfo.limit,
          limits: this.paginationInfo.limits,
          totalCount: contype.data.length
        };
        this.changeDetectorRefs.detectChanges();
        console.log('connectortypes...', this.connectortypes);
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  populateCategories() {
    if (this.catgorygroup && this.catgorygroup.length > 0) {
      this.Categories = []; // ['rdbms', 'nosql', 'webapi'];
      for (let i = 0; i < this.catgorygroup.length; i++) {
        this.Categories.push(this.catgorygroup[i].category);
      }
    } else {
      this.Categories = [];
    }
  }

  populateSubCategories(selcategory: String) {
    this.SubCategories = [];
    console.log('select catg =>', selcategory);

    if (this.catgorygroup && this.catgorygroup.length > 0) {
      const catgrp = this.catgorygroup.find(
        c => c.category.toLowerCase() === selcategory.toLowerCase()
      );

      if (catgrp) {
        this.SubCategories = catgrp.subcategory;
      }
    }
  }


  reset() {
    this.alertService.clear();
    this.popuptitle = 'Add New Connector Type';
    this.connectortype = new ConnectorType();
    this.connectortype.category = '';
    this.connectortypeForm.reset();
  }

  saveClick(popupwindow: any) {
    this.alertService.clear();
    console.log('to save =>', this.connectortype);
    if (this.isformValid()) {
      if (this.isNullOrEmpty(this.connectortype.connectortypeid)) {
        this.saveNewConnectorType(popupwindow);
      } else {
        this.editExistingConnectorType(popupwindow);
      }
    }
  }

  saveNewConnectorType(popupwindow: any) {
    this.connectortype.createdby = this.username;
    this.connectortypeService.createConnectorType(this.connectortype).subscribe(
      contype => {
        console.log(contype);
        this.reset();
        this.getAllConnectorTypes();
        this.alertService.success('ConnectorType successfully created.');
        popupwindow.close();
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  editExistingConnectorType(popupwindow: any) {
    this.connectortype.lastmodifiedby = this.username;
    // Need to check if there is already a connectortype present with a same name if connectortype name is edited
    this.connectortypeService.updateConnectorType(this.connectortype).subscribe(
      contype => {
        console.log(contype);
        this.reset();
        this.getAllConnectorTypes();
        this.alertService.success('ConnectorType successfully Updated.');
        popupwindow.close();
      },
      error => {
        this.alertService.error(error);
      }
    );
  }

  edit(connectortypeid: String) {
    this.alertService.clear();
    this.popuptitle = 'Edit Connector Type';
    if (!this.isNullOrEmpty(connectortypeid)) {
      this.connectortypeService.getConnectorTypeById(connectortypeid).subscribe(
        contype => {
          this.connectortype = contype.data;
          this.populateSubCategories(this.connectortype.category);
          this.changeDetectorRefs.detectChanges();
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
  }

  delete(connectortypeid: String, name: String) {
    this.alertService.clear();
    if (confirm('Are you sure to delete ' + name)) {
      this.connectortypeService.deleteConnectorType(connectortypeid).subscribe(
        contype => {
          this.getAllConnectorTypes();
          this.alertService.success('ConnectorType successfully deleted.');
        },
        error => {
          this.alertService.error(error);
        }
      );
    }
  }

  public updatePage(model) {
    this.paginationInfo.limit = model.limit;
    this.paginationInfo.offset = model.offset;

    if (model.offset === 0) {
      this.stripedrowData = this.connectortypes.slice(0, model.limit);
    } else {
      this.stripedrowData = this.connectortypes.slice(
        model.offset,
        model.offset + model.limit
      );
    }
  }

  isformValid() {
    let isvalid = true;
    const errmessages = [];

    if (this.isNullOrEmpty(this.connectortypeForm.get('name').value)) {
      errmessages.push('ConnectorType name cannot be left blank');
      isvalid = false;
    }

    if (this.isNullOrEmpty(this.connectortypeForm.get('category').value)) {
      errmessages.push('Category cannot be left blank');
      isvalid = false;
    }

    if (this.isNullOrEmpty(this.connectortypeForm.get('subcategory').value)) {
      errmessages.push('Subcategory cannot be left blank');
      isvalid = false;
    }

    if (this.isNullOrEmpty(this.connectortypeForm.get('version').value)) {
      errmessages.push('Version cannot be left blank');
      isvalid = false;
    }

    if (this.isNullOrEmpty(this.connectortypeForm.get('provider').value)) {
      errmessages.push('Provider cannot be left blank');
      isvalid = false;
    }

    if (!isvalid) {
      errmessages.forEach(element => {
        this.alertService.error('Error' + element);
      });
    }
    return isvalid;
  }

  isNullOrEmpty(val: String) {
    return (val == null || val === '' || val === 'undefined');
  }
}
