import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy } from '@angular/core';

@Component({
  selector: 'enterprise-connector',
  styleUrls: ['./enterprise-connector.component.scss'],
  moduleId: module.id.toString(),
  templateUrl: 'enterprise-connector.component.html'
})
export class EnterpriseConnectorComponent {
}
