import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntentImplementationNodeMappingComponent } from './intent-implementation-nodemap.component';

describe('IntentImplementationNodeMappingComponent', () => {
  let component: IntentImplementationNodeMappingComponent;
  let fixture: ComponentFixture<IntentImplementationNodeMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntentImplementationNodeMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentImplementationNodeMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
