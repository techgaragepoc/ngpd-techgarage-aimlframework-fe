import {
  Component, OnInit, ChangeDetectorRef, ViewEncapsulation, AfterViewInit,
  ViewChild, OnChanges, ChangeDetectionStrategy, Input
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AuthenticationService, AlertService,
  DataSetService, IntentImplementationService
} from '../../../shared/service';
import { User, DataSet, IntentFulfillmentModel } from '../../../shared/models';

import { Utilities } from '../../../shared/utils';
import * as Rx from 'rxjs';
import { switchMap } from 'rxjs/operators';
import * as clone from 'clone';


// import { FormControl, FormGroup, Validators } from '@angular/forms';
// import { Observable } from 'rxjs/Observable';

const gojs = require('../../../shared/libs/gojs/go');

@Component({
  selector: 'mercer-intent-implementation-nodemap',
  templateUrl: './intent-implementation-nodemap.component.html',
  styleUrls: ['./intent-implementation-nodemap.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IntentImplementationNodeMappingComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChild('intentFulfillmentGraph') divIntentFulfillmentGraph;

  currentUser: User;
  username: String = '';

  @Input() nlpmodel: String;
  @Input() nlpintent: String;
  @Input() intentffmodelgraph: String;

  public selectedIntentffModelGraph: IntentFulfillmentModel;

  public intentffmodelgraphs: IntentFulfillmentModel[];
  public datasets: DataSet[];
  public domainmodels: any[];
  public aggregatefunctions: any[];
  public modelNodeDatasetMappings: any[];

  public intentffmodel: any;

  public nodemappings: NodeMapping[];
  public nodemapping: NodeMapping;
  // public node: Node;
  public propertytype: String = '';
  public modelname: String = '';
  public posTree:String = '';

  public updatedMappings: NodeMapping[];
  public updatedModelid: String;

  private utilities: Utilities;
  public modelselctr: number;

  public detailLog: Boolean;

  public entities: any[];
  public updatedEntities: any[];

  public executionStatus = '';
  public executionresult = '';
  public processedmodel: any;

  public modelFulfilledPercentage: number = 0;
  public fulfillprogresstheme: String = 'primary';

  // myDomainModelDiv: any;
  goJSIntentFulfillmentGraph: any;


  public nodemappingHeader: any[] = [
    {
      name: 'Node',
      column: 'node.label',
      disableSort: true
    },
    {
      name: 'Mapped?',
      column: 'ismapped',
      textAlign: 'center',
      disableSort: true
    },
    {
      name: 'Source',
      column: 'mappedsourcename',
      disableSort: true
    },
    {
      name: 'Edit Mapping',
      disableSort: true,
      column: 'actionIcon',
      textAlign: 'center'
    }
  ];

  constructor(
    private authService: AuthenticationService,
    private alertService: AlertService,
    private dataSetService: DataSetService,
    private intentImplementationService: IntentImplementationService,
    private changeDetectorRefs: ChangeDetectorRef,
    private route: ActivatedRoute
  ) {

    this.utilities = new Utilities();
    this.currentUser = this.authService.getUserInfo();
    this.username = (this.currentUser != null && this.currentUser.username !== 'undefined') ? this.currentUser.username : 'none';

    this.modelselctr = 0;
    this.intentffmodelgraphs = [];
    this.datasets = [];
    this.aggregatefunctions = [];

    this.nodemappings = [];
    this.nodemapping = null;

    this.updatedMappings = [];
    this.updatedModelid = '';

    this.modelNodeDatasetMappings = [];
    this.selectedIntentffModelGraph = new IntentFulfillmentModel();

    this.detailLog = true;
    this.entities = [];
    this.updatedEntities = [];
    this.executionStatus = '';
    this.executionresult = '';
    this.processedmodel = {};
    this.posTree = '';

  }

  ngOnInit() {
    this.initialize();
  }

  ngOnChanges() {
    this.initialize();
  }

  ngAfterViewInit() {
    this.initializeGraphCanvas();
  }

  initialize() {
    const self = this;

    this.updatedMappings = [];
    this.updatedModelid = '';
    this.entities = [];
    this.updatedEntities = [];
    this.alertService.clear();

    //load all graph models
    this.intentImplementationService.getAllIntentFulfillmentGraphModels().subscribe(graphmodels => {
      this.intentffmodelgraphs = graphmodels.data;

      //load all functions meta data
      this.intentImplementationService.getAllFunctionsMetaData()
        .subscribe(funcs => {

          this.aggregatefunctions = funcs.data;

          //load all datasets
          this.dataSetService.getAllDataSet().subscribe(ds => {
            console.log('existing datasets...', ds);

            if (ds && ds.data && ds.data.length > 0) {
              this.datasets = ds.data;
            }

            //load selected graph model
            if (this.intentffmodelgraph != null && typeof this.intentffmodelgraph != 'undefined') {
              this.onIntentffModelSelection(this.intentffmodelgraph);
            }
          });
        });
    });
  }

  onModelChanged(model: any) {
    this.updatedModelid = model._id;
    this.entities = [];
    this.updatedEntities = [];
  }

  getPOSEntities(intentffmodel) {

    if (!this.utilities.IsNull(intentffmodel.pos)) {
      var entities = this.utilities.GetKeyValue(intentffmodel.pos, 'entities');
      return (!this.utilities.IsNull(entities)) ? entities : [];
    }
    return [];
  }

  onIntentffModelSelection(modelgraphid: String) {
    this.posTree = '';

    this.intentImplementationService.getIntentFulfillmentModelGraphById(modelgraphid)
      .subscribe(modelgraph => {

        if (modelgraph.data != null && modelgraph.data != 'undefined') {
          this.selectedIntentffModelGraph._id = modelgraph.data._id;
          this.selectedIntentffModelGraph.name = modelgraph.data.name;
          this.selectedIntentffModelGraph.pos = modelgraph.data.pos;
          this.selectedIntentffModelGraph.graphjson = modelgraph.data.graphjson;
          this.selectedIntentffModelGraph.nodeproperties = modelgraph.data.nodeproperties;

          this.entities = this.getPOSEntities(this.selectedIntentffModelGraph);
          this.posTree = JSON.stringify(modelgraph.data.pos);

          this.modelname = this.selectedIntentffModelGraph.name;
          this.loadIntentFulfillmentGraph();
        }
      });
  }

  loadIntentFulfillmentGraph() {
    if (this.selectedIntentffModelGraph != null && this.selectedIntentffModelGraph.graphjson != null
      && this.selectedIntentffModelGraph.graphjson != 'undefined') {

      var graphJson = this.selectedIntentffModelGraph.graphjson;
      console.log('this.selectedIntentffModelGraph.graphjson =>', graphJson);
      this.goJSIntentFulfillmentGraph.model = new gojs.GraphLinksModel(graphJson.node, graphJson.link);

      //load transformed model
      console.log('this.selectedIntentffModelGraph._id =>', this.selectedIntentffModelGraph._id);
      this.intentImplementationService.getIntentFulfillmentModelByGraphId(this.selectedIntentffModelGraph._id)
        .subscribe(model => {
          console.log('intentffmodel =>', model);
          this.intentffmodel = model.data;

          // load all domain models for node attributes
          // [TODO: load specific domain models only of selected intentff model]
          this.intentImplementationService.getAllDomainModels().subscribe(domainmodels => {
            this.domainmodels = domainmodels.data;

            this.intentImplementationService.getAllNodeDatasetMappings()
              .subscribe(mappings => {
                this.modelNodeDatasetMappings = mappings.data;

                // populate data table
                this.populateDataTable();

              }); // getAllNodeDatasetMappings
          }); // getAllDomainModels
        }); // getIntentFulfillmentModelByGraphId
    }
  }

  assignSequenceNo(nodes: any[], nodeid: String, sequence: number, sortednodes: any[]) {
    let node = nodes.find(nd => nd.nodeid == nodeid);
    if (node != null && typeof node != 'undefined') {
      node["sequence"] = sequence;

      let existingnode = sortednodes.find(snd => snd.nodeid == node.nodeid);
      if (this.utilities.IsNull(existingnode)) {
        sortednodes.push(node);
      }
      else {
        existingnode["sequence"] = sequence;
      }

      if (!this.utilities.IsNull(node.nextnodes) && this.utilities.IsArray(node.nextnodes)
        && node.nextnodes.length > 0) {
        for (let i = 0; i < node.nextnodes.length; i++) {
          this.assignSequenceNo(nodes, node.nextnodes[i], sequence + 1, sortednodes);
        }
      }
    }
  }

  sortByFlow(nodes: any[], startnodeid: String) {
    let sortednodes = [];

    if (!this.utilities.IsNull(nodes) && this.utilities.IsArray(nodes)) {

      //to assign default sequence 0 to all nodes
      for(let i=0;i<nodes.length;i++) { 
        nodes[i]["sequence"] = 0;
        sortednodes.push(nodes[i]);
      }

      this.assignSequenceNo(nodes, startnodeid, 1, sortednodes);
      sortednodes = this.utilities.SortArray(sortednodes, 'sequence');
    }
    else {
      sortednodes = nodes;
    }
    return sortednodes;
  }

  populateDataTable() {
    this.nodemappings = [];

    if (this.intentffmodel != null && this.intentffmodel != 'undefined'
      && this.intentffmodel.nodes != null && typeof this.intentffmodel.nodes != 'undefined'
      && this.intentffmodel.nodes.length > 0) {
      const startnode = (this.intentffmodel.details.startnode != null)
        ? this.intentffmodel.details.startnode : this.intentffmodel.nodes[0].nodeid;
      const nodes = this.sortByFlow(this.intentffmodel.nodes, startnode);
      console.log('sorted nodes =>', nodes);
      // console.log('this.intentffmodel =>', this.intentffmodel);
      for (let i = 0; i < nodes.length; i++) {
        let isConnectorMapped = 'No';
        let mappedSource = null;
        let mappedSourceName = '';

        const currentNodeProperties = this.getNodeProperties(nodes[i].nodeid);
        const baseNodeAttributes = this.getBaseNodeAttributes(nodes[i].basemodelgraphid, nodes[i].baseid);
        const connectorDatasets = this.getConnectorDatasets(nodes[i].basemodelgraphid, nodes[i].baseid);

        let nodeimage = '';
        let connectorimage = '';

        console.log('nodeid - nodeprop =>', nodes[i].nodeid, currentNodeProperties, baseNodeAttributes);

        if (nodes[i].nodetype != 'undefined' && nodes[i].nodetype.toLowerCase() == 'entity') {
          nodeimage = '../assets/images/node.svg';


          if (!this.utilities.IsNull(currentNodeProperties) &&
            !this.utilities.IsNull(currentNodeProperties.connectordataset) &&
            !this.utilities.IsNull(currentNodeProperties.connectordataset.datasetid)) {
            isConnectorMapped = 'Yes';
            mappedSource = this.datasets.find(ds => ds.datasetid == currentNodeProperties.connectordataset.datasetid);

          }
          else {
            //if nodeproperties not defined yet (new model case), check if basmodel & node is mapped
            if (connectorDatasets != null && connectorDatasets.length > 0) {
              var defaultConnectorDatset = connectorDatasets.find(ds => ds.default == true);
              mappedSource = (defaultConnectorDatset != null && typeof defaultConnectorDatset != 'undefined')
                ? defaultConnectorDatset.dataset : connectorDatasets[0].dataset;
              isConnectorMapped = 'Yes'

            }
          }

          mappedSourceName = (!this.utilities.IsNull(mappedSource)) ? mappedSource.name : '';
          connectorimage = (!this.utilities.IsNull(mappedSource)) ? '../assets/images/database.svg' : '';
        }
        else {
          nodeimage = '../assets/images/function.svg';

          /*
          if (!this.utilities.IsNull(currentNodeProperties) &&
            !this.utilities.IsNull(currentNodeProperties.inputparameters) &&
            currentNodeProperties.inputparameters.length > 0) {
              */
          isConnectorMapped = 'Yes';
          mappedSource = this.aggregatefunctions.find(ds => ds.code == nodes[i].baseid);
          if (!this.utilities.IsNull(mappedSource)) {
            mappedSourceName = mappedSource.name;
          }
          //  }

          connectorimage = '../assets/images/function.svg';
        }

        this.nodemappings.push({
          "modelgraphid": this.intentffmodel.modelgraphid,
          "nodetype": nodes[i].nodetype.toLowerCase(),
          "node": nodes[i],
          "posentities": this.getPOSEntities(this.intentffmodel),
          "nodeproperties": currentNodeProperties,
          "basenodeattributes": baseNodeAttributes,
          "connectordatasets": connectorDatasets,
          "mappedsource": mappedSource,
          "mappedsourcename": mappedSourceName,
          "ismapped": isConnectorMapped,
          "nodeimagesrc": nodeimage,
          "mappedclass": (isConnectorMapped == 'Yes') ? "status-mapped" : "status-unmapped",
          "connimagesrc": connectorimage
        });
      } // for each node

      this.calculateImplementPercentage();
    }
  }

  calculateImplementPercentage() {
    let totalEntityNodes = 0,
      totalFulfilledNodes = 0;

    this.modelFulfilledPercentage = 0;
    this.fulfillprogresstheme = 'primary';

    if (!this.utilities.IsNull(this.nodemappings) && this.nodemappings.length > 0) {
      for (let i = 0; i < this.nodemappings.length; i++) {
        if (this.nodemappings[i].nodetype.toLowerCase() == 'entity') {
          totalEntityNodes++;

          if (!this.utilities.IsNull(this.nodemappings[i].mappedsource)) {
            totalFulfilledNodes++;
          }
        }
      } // for each node

      this.modelFulfilledPercentage = (totalEntityNodes > 0) ? ((totalFulfilledNodes / totalEntityNodes) * 100) : 0;

      this.fulfillprogresstheme = (this.modelFulfilledPercentage < 30) ? 'alert'
        : ((this.modelFulfilledPercentage < 50) ? 'warning'
          : ((this.modelFulfilledPercentage < 100) ? 'primary' : 'success'));

    }
  }

  edit(proptype: String, selectednodemapping: any) {
    console.log('selected row =>', selectednodemapping);
    this.nodemapping = clone(selectednodemapping);
    // this.node = clone(selectednodemapping.node);
    this.propertytype = proptype;
    this.updatedMappings = clone(this.nodemappings);
  }

  openChangeModel(modelwin: any) {
    //this.updatedModelid = this.selectedIntentffModelGraph._id;
    this.modelselctr++;
    modelwin.open();
  }

  onModelSelectionfocus() {
    this.modelselctr++;
  }

  reflectModelChange(isOk: Boolean) {
    if (isOk) {
      this.onIntentffModelSelection(this.updatedModelid);
    }
    else {
      this.updatedModelid = '';
    }
  }

  reflectNodePropertyChanges(isOk: Boolean) {
    if (isOk) {
      this.nodemappings = clone(this.updatedMappings);
      console.log('on accept mapping change =>', this.nodemappings);
      this.calculateImplementPercentage();
      // this.saveNodeDatasetMapping();
    }
    else {
      this.updatedMappings = [];
    }
  }

  reflectTestEntities(isOk: Boolean) {
    if (isOk) {
      this.entities = clone(this.updatedEntities);
      // this.saveNodeDatasetMapping();
    }
    else {
      this.updatedEntities = [];
    }
  }

  openEntityTypes(popupwin) {
    this.updatedEntities = clone(this.entities);
    popupwin.open();
  }

  entitytypeValueChange(entity, event) {
    var newValue = event.target.value;
    for (let i = 0; i < this.updatedEntities.length; i++) {
      if (this.updatedEntities[i].entity_type == entity.entity_type) {
        this.updatedEntities[i].value = newValue;
      }
    }
  }

  executeModel() {
    var self = this;

    if (this.entities.length <= 0) {
      this.entities = this.getPOSEntities(this.selectedIntentffModelGraph);
    }

    let nlpPOS = {
      "Entities": self.entities
    };

    console.log('nlp entites...', nlpPOS);

    this.executionStatus = 'Processing...';
    this.executionresult = '';
    this.processedmodel = {};

    this.intentImplementationService.getIntentFulfillmentModelByGraphId(this.selectedIntentffModelGraph._id)
      .subscribe(intentffmodel => {

        this.intentImplementationService.executeIntentFulfillmentModel(intentffmodel.data, nlpPOS)
          .subscribe(result => {

            console.log('model executed...', result)
            //var sessionid = .data;
            const sessionid = result.data.logsessionid;
            var loglevel = (this.detailLog) ? '1' : '0';

            self.executionStatus = 'Completed';
            self.processedmodel = result.data; //show result as JSON

            //show log records
            self.intentImplementationService.getExecutionResult(sessionid, loglevel)
              .subscribe(log => {
                var newline = '&#10;';
                var execresults = [];
                self.executionresult = '';

                if (!self.utilities.IsNull(log.data) && !self.utilities.IsNull(log.data.result)
                  && self.utilities.IsArray(log.data.result)) {
                  execresults = log.data.result;
                }

                for (let i = 0; i < execresults.length; i++) {
                  var message = '';
                  if (!self.utilities.IsNull(execresults[i].message) &&
                    !self.utilities.IsNull(execresults[i].message.step) &&
                    !self.utilities.IsNull(execresults[i].message.message)) {
                    message = (execresults[i].message.step + '...' + execresults[i].message.message);
                  }
                  else {
                    message = JSON.stringify(execresults[i].message);
                  }
                  self.executionresult += ('>> ' + message + ' ' + newline + ' ');
                }

              });

          });
      });
  }

  setNodeProperties(mapping: any) {
    console.log('mapping =>', mapping);
    for (let i = 0; i < this.updatedMappings.length; i++) {
      if (this.updatedMappings[i].node.nodeid == mapping.node.nodeid) {
        this.updatedMappings[i] = mapping;
        this.nodemapping = mapping;
        break;
        //this.nodemappings[i].mappedsourcename = mapping.mappedsourcename;

      }
    }
  }

  saveModelMappings() {
    var promises = [];
    // var nodedatasetmappings = [];
    var intentffmodelnodeproperties = [];

    this.alertService.clear();
    this.calculateImplementPercentage();

    console.log('saving nodemappings =>', this.nodemappings);

    for (let i = 0; i < this.nodemappings.length; i++) {

      //Part 1: node mapping
      if (this.nodemappings[i].nodetype.toLowerCase() == 'entity') {

        if (!this.utilities.IsNull(this.nodemappings[i].mappedsource)) {
          var mapping = {};
          var conndataset = this.nodemappings[i].connectordatasets.find(ds => ds.dataset.datasetid == this.nodemappings[i].mappedsource.datasetid);
          // console.log('conndataset =>', conndataset);
          mapping["domainmodelgraphid"] = this.nodemappings[i].node.basemodelgraphid;
          mapping["nodeid"] = this.nodemappings[i].node.baseid;
          mapping["datasetid"] = (!this.utilities.IsNull(this.nodemappings[i].mappedsource)) ? this.nodemappings[i].mappedsource.datasetid : '';
          mapping["attributemapping"] = (!this.utilities.IsNull(conndataset)) ? conndataset.attributemapping : [];
          mapping["default"] = true;
          mapping["createdby"] = this.username;
          mapping["lastmodifiedby"] = this.username;

          promises.push(this.intentImplementationService.updateNodeDatasetMappings(mapping));
        }
      }

      //Part 2: node property
      var nodeproperty = {};
      nodeproperty["connectordataset"] = {};
      nodeproperty["nodeid"] = this.nodemappings[i].node.nodeid;

      if (this.nodemappings[i].nodetype.toLowerCase() == 'entity') {
        if (!this.utilities.IsNull(this.nodemappings[i].mappedsource)) {
          nodeproperty["connectordataset"] = {
            "datasetid": this.nodemappings[i].mappedsource.datasetid,
            "name": this.nodemappings[i].mappedsource.name,
          };
        }
      }
      nodeproperty["inputparameters"] = (!this.utilities.IsNull(this.nodemappings[i].nodeproperties)) ? this.nodemappings[i].nodeproperties.inputparameters : [];
      nodeproperty["output"] = (!this.utilities.IsNull(this.nodemappings[i].nodeproperties)) ? this.nodemappings[i].nodeproperties.output : [];

      intentffmodelnodeproperties.push(nodeproperty);

    } //for each nodemapping


    var intentffmodel = {
      modelgraphid: this.selectedIntentffModelGraph._id,
      nodeproperties: intentffmodelnodeproperties,
      lastmodifiedby: this.username
    };
    promises.push(this.intentImplementationService.updateIntentFulfillmentNodeProperties(intentffmodel));

    //status update promise
    promises.push(this.intentImplementationService.updateImplementationStatus(
      this.selectedIntentffModelGraph._id, this.modelFulfilledPercentage.toString(), this.username));

    //execute all subscribes
    Rx.Observable.forkJoin(promises).subscribe(
      success => {
        this.updatedMappings = [];
        this.loadIntentFulfillmentGraph();
        this.changeDetectorRefs.detectChanges();
        this.alertService.success("Intent Fulfillment Model mappings saved successfully!!");
      },
      err => {
        console.log('error in save  =>', err);
        this.updatedMappings = [];
        this.loadIntentFulfillmentGraph();
        this.changeDetectorRefs.detectChanges();
        this.alertService.error("Error occured while saving Model mappings!!");
      });
  }


  getNodeProperties(nodeid) {
    const nodeproperties = this.intentffmodel.nodeproperties;

    if (nodeproperties != null && nodeproperties != 'undefined' && nodeproperties.length > 0) {
      for (let i = 0; i < nodeproperties.length; i++) {
        if (nodeproperties[i].nodeid == nodeid) {
          return nodeproperties[i];
        }
      }
    }
    return null;
  }

  reset() {
    this.alertService.clear();
  }

  getConnectorDatasets(basemodelid, basenodeid): any[] {
    let connectorDatasets = [];
    console.log('getConnectorDatasets =>', basemodelid, basenodeid);
    console.log('this.modelNodeDatasetMappings =>', this.modelNodeDatasetMappings);
    if (!this.utilities.IsNull(this.modelNodeDatasetMappings) && this.modelNodeDatasetMappings.length > 0) {
      for (let i = 0; i < this.modelNodeDatasetMappings.length; i++) {
        var mapping = this.modelNodeDatasetMappings[i];
        if (mapping.domainmodelgraphid == basemodelid &&
          mapping.nodeid == basenodeid) {
          var mappedsource = this.datasets.find(ds => ds.datasetid == mapping.datasetid);
          connectorDatasets.push({
            dataset: mappedsource,
            default: mapping.default,
            attributemapping: mapping.attributemapping
          });
        } //if model & node id matched
      } // for each mapping
    } // if mappings are valid

    return connectorDatasets;
  }

  getBaseNodeAttributes(basemodelid, basenodeid): any[] {
    let attributes = [];

    if (this.domainmodels != null && this.domainmodels.length > 0) {
      for (let i = 0; i < this.domainmodels.length; i++) {
        var model = this.domainmodels[i];
        if (model.modelgraphid == basemodelid) {
          for (let j = 0; j < model.nodeproperties.length; j++) {
            if (model.nodeproperties[j].nodeid == basenodeid) {
              attributes = model.nodeproperties[j].attributes;
              break;
            }
          } //for each node property
        } // if model matched
      } // loop through each model till desired model & node not found
    } // if domain models populated

    return attributes;
  }

  initializeGraphCanvas() {
    var $ = gojs.GraphObject.make;

    var intentFulfillmentDiv = this.divIntentFulfillmentGraph.nativeElement;

    // var goJSIntentFulfillmentGraphRef = this.goJSIntentFulfillmentGraph;
    this.goJSIntentFulfillmentGraph = $(gojs.Diagram, intentFulfillmentDiv,
      {
        initialContentAlignment: gojs.Spot.Center,

        allowDragOut: false,
        allowDrop: false,
        "commandHandler.archetypeGroupData": { isGroup: true },
        // handle undo or redo maybe changing Diagram properties controlled by check boxes
        "ModelChanged": function (e) {
          // if (e.isTransactionFinished) allChecks(true);
        }
      });

    // define a simple Node template
    this.goJSIntentFulfillmentGraph.nodeTemplate =
      $(gojs.Node, "Auto",  // the Shape will go around the TextBlock
        new gojs.Binding("desiredSize", "size", gojs.Size.parse),
        new gojs.Binding("position", "pos", gojs.Point.parse).makeTwoWay(gojs.Point.stringify),
        $(gojs.Shape, "RoundedRectangle",//Circle
          { portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer", strokeWidth: 0, fill: "#00c0ef" },
          // Shape.fill is bound to Node.data.color
          new gojs.Binding("fill", "color")),
        $(gojs.TextBlock,
          { margin: 8 },  // some room around the text
          // TextBlock.text is bound to Node.data.key
          new gojs.Binding("text", "text"))
      );

    this.goJSIntentFulfillmentGraph.model = new gojs.GraphLinksModel([], []);
    this.goJSIntentFulfillmentGraph.allowDragOut = this.goJSIntentFulfillmentGraph.allowDrop = this.goJSIntentFulfillmentGraph.allowCopy = this.goJSIntentFulfillmentGraph.allowDelete = this.goJSIntentFulfillmentGraph.allowInsert = false;
    this.goJSIntentFulfillmentGraph.allowMove = true;
    this.goJSIntentFulfillmentGraph.isReadOnly = true;
    this.goJSIntentFulfillmentGraph.model.undoManager.isEnabled = true;
  }

}



class NodeMapping {
  modelgraphid?: String;
  nodetype?: String;
  node?: any = {};
  posentities?: any;
  nodeproperties?: any = {};
  basenodeattributes?: any[] = [];
  connectordatasets?: any[] = [];
  mappedsource?: any = {};
  mappedsourcename?: String;
  ismapped?: String;
  nodeimagesrc?: String;
  mappedclass?: String;
  connimagesrc?: String;
}