import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntentImplementationPropertyComponent } from './intent-implementation-property.component';

describe('IntentImplementationPropertyComponent', () => {
  let component: IntentImplementationPropertyComponent;
  let fixture: ComponentFixture<IntentImplementationPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntentImplementationPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentImplementationPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
