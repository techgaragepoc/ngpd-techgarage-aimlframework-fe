import {
  Component, OnInit, ChangeDetectorRef, ViewEncapsulation, AfterViewInit,
  Input, Output, EventEmitter, ViewChild, ChangeDetectionStrategy, OnChanges
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService, AlertService, DataSetService, DataService } from '../../../shared/service';
import { User, DataSet } from '../../../shared/models';
import { Utilities } from '../../../shared/utils';

@Component({
  selector: 'mercer-intent-implementation-property',
  templateUrl: './intent-implementation-property.component.html',
  styleUrls: ['./intent-implementation-property.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IntentImplementationPropertyComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() aggregatefunctions: any[];
  @Input() nodemappings: any[];

  @Input() propertytype: any;
  @Input() nodemapping: any;
  @Input() datasets: DataSet[];


  @Output() onChange: EventEmitter<any> = new EventEmitter<any>();


  currentUser: User;
  username: String = '';

  private utilities: Utilities;

  public connectordatasets: DataSet[];
  public selectedDatasetId: String;
  public attributemapping: any[];
  public inputparams: any[];
  public outputparams: any[];

  public nodelabel: String = '';
  public nodetype: String = '';
  public outputAllCols: Boolean = false;
  public basenodeattributes: any;
  public posEntities: any[];

  public previewstatus:String = '';
  public previewresult:String = '';
  public selecteddatasetname: String = '';

  constructor(
    private authService: AuthenticationService,
    private alertService: AlertService,
    private dataSetService: DataSetService,
    private dataService: DataService,
    private changeDetectorRefs: ChangeDetectorRef
  ) {
    this.utilities = new Utilities();
    this.currentUser = this.authService.getUserInfo();
    this.username = (this.currentUser != null && this.currentUser.username !== 'undefined') ? this.currentUser.username : 'none';
    this.connectordatasets = [];
    this.selectedDatasetId = '';
    this.attributemapping = [];
    this.nodelabel = '';
    this.nodetype = '';
    this.inputparams = [];
    this.outputparams = [];
    this.outputAllCols = false;
    this.posEntities = [];
    this.basenodeattributes = {};
  }

  ngOnInit() {
    const self = this;
    this.alertService.clear();
  }

  ngAfterViewInit() {
  }

  ngOnChanges() {
    console.log('this.nodemapping =>', this.nodemapping);

    if (!this.utilities.IsNull(this.nodemapping) && !this.utilities.IsNull(this.nodemapping.nodetype)) {
      this.nodelabel = this.nodemapping.node.label;
      this.nodetype = 'entity';
      this.posEntities = this.nodemapping.posentities;
      //for node as entity
      if (this.nodemapping.nodetype.toLowerCase() == 'entity') {
        this.selectedDatasetId = (!this.utilities.IsNull(this.nodemapping.mappedsource)) ? this.nodemapping.mappedsource.datasetid : '';
        this.connectordatasets = this.utilities.SortArray(this.datasets, 'name');

        this.getAttributeMapping();
      }

      // for node as function
      if (this.nodemapping.nodetype.toLowerCase() == 'aggregatefunction') {
        this.nodetype = 'aggregatefunction';
      }

      this.getInputParameters();
      this.getOutputParameters();
    }

    this.changeDetectorRefs.detectChanges();
  }

  getAttributeMapping() {

    this.attributemapping = [];

    //get basenode attributes
    if (!this.utilities.IsNull(this.nodemapping.basenodeattributes) && this.nodemapping.basenodeattributes.length > 0) {
      for (let i = 0; i < this.nodemapping.basenodeattributes.length; i++) {
        if (!this.utilities.IsNull(this.nodemapping.basenodeattributes[i].name)) {
          this.attributemapping.push({
            nodeattr: this.nodemapping.basenodeattributes[i].name,
            connectorattr: ''
          });
        }
      }
    }

    //if previously mapped with selected dataset then pull the mapping
    if (!this.utilities.IsNull(this.nodemapping.connectordatasets) && this.nodemapping.connectordatasets.length > 0) {
      for (let i = 0; i < this.nodemapping.connectordatasets.length; i++) {
        if (!this.utilities.IsNull(this.nodemapping.connectordatasets[i].attributemapping)
          && !this.utilities.IsNull(this.nodemapping.connectordatasets[i].dataset)
          && this.nodemapping.connectordatasets[i].dataset.datasetid == this.selectedDatasetId) {

          var mappedDataset = this.nodemapping.connectordatasets[i];
          for (let j = 0; j < this.attributemapping.length; j++) {
            var dsmapping = mappedDataset.attributemapping.find(map => map.nodeattr == this.attributemapping[j].nodeattr);
            if (!this.utilities.IsNull(dsmapping)) {
              this.attributemapping[j].connectorattr = dsmapping.connectorattr;
            }
          }
        }
      }
    }

  }

  getInputParameters() {
    this.inputparams = [];

    //get basenode attributes
    if (this.nodemapping.nodetype.toLowerCase() == 'entity') {
      if (!this.utilities.IsNull(this.nodemapping.basenodeattributes) && this.nodemapping.basenodeattributes.length > 0) {
        for (let i = 0; i < this.nodemapping.basenodeattributes.length; i++) {
          if (!this.utilities.IsNull(this.nodemapping.basenodeattributes[i].name)) {
            this.inputparams.push({
              "name": this.nodemapping.basenodeattributes[i].name,
              "sourcetype": "",
              "sourcename": "",
              "sourcevalue": "",
              "sourcegroupby": [],
            });
          }
        }
      }
    }
    else {
      this.inputparams.push({
        "name": "AggregateValue",
        "sourcetype": "",
        "sourcename": "",
        "sourcevalue": "",
        "sourcegroupby": [],
      });
    }


    if (!this.utilities.IsNull(this.nodemapping.nodeproperties) &&
      !this.utilities.IsNull(this.nodemapping.nodeproperties.inputparameters)) {
      for (let i = 0; i < this.nodemapping.nodeproperties.inputparameters.length; i++) {
        var inparam = this.inputparams.find(p => p.name == this.nodemapping.nodeproperties.inputparameters[i].name);
        if (!this.utilities.IsNull(inparam)) {
          inparam.sourcetype = this.nodemapping.nodeproperties.inputparameters[i].sourcetype;
          inparam.sourcename = this.nodemapping.nodeproperties.inputparameters[i].sourcename;
          inparam.sourcevalue = this.nodemapping.nodeproperties.inputparameters[i].sourcevalue;
          inparam.sourcegroupby = this.nodemapping.nodeproperties.inputparameters[i].sourcegroupby;

          let attr = this.nodemappings.find(nd => nd.node.nodeid == inparam.sourcename);
          this.basenodeattributes[inparam.name] = (!this.utilities.IsNull(attr)) ? attr.basenodeattributes : [];
        }
      }
    }

  }

  getOutputParameters() {
    this.outputparams = [];
    this.outputAllCols = true;

    //get basenode attributes
    if (this.nodemapping.nodetype.toLowerCase() == 'entity') {
      if (!this.utilities.IsNull(this.nodemapping.basenodeattributes) && this.nodemapping.basenodeattributes.length > 0) {
        for (let i = 0; i < this.nodemapping.basenodeattributes.length; i++) {
          if (!this.utilities.IsNull(this.nodemapping.basenodeattributes[i].name)) {
            this.outputparams.push({
              "name": this.nodemapping.basenodeattributes[i].name,
              "selected": false
            });
          }
        }
      }
    }
    else {
      this.outputparams.push({ "name": "AggregateValue", "selected": false });
    }

    if (!this.utilities.IsNull(this.nodemapping.nodeproperties) &&
      !this.utilities.IsNull(this.nodemapping.nodeproperties.output)) {
      for (let i = 0; i < this.nodemapping.nodeproperties.output.length; i++) {
        var outparam = this.outputparams.find(p => p.name == this.nodemapping.nodeproperties.output[i]);
        if (!this.utilities.IsNull(outparam)) {
          outparam.selected = true;
          this.outputAllCols = false;
        }
      }
    }

  }

  showDataPreview(popupwindow: any) {

    this.previewstatus = 'Fetching top row...';
    this.previewresult = '';
    this.selecteddatasetname = '';

    if (this.selectedDatasetId != '') {
      let dstoshow = this.connectordatasets.find(ds => ds.datasetid == this.selectedDatasetId);
      if (!this.utilities.IsNull(dstoshow)) {
        this.selecteddatasetname = dstoshow.name;
        popupwindow.open();
        this.dataService.getdata(dstoshow.datasetid,dstoshow.name,{},1).subscribe(resp => {
          console.log('data for preview...', resp);
          this.previewstatus = 'Result...';
          this.previewresult = JSON.stringify(resp.data);
        },
          err => {
            this.previewstatus = 'Error...';
           // this.previewresultstyle = 'mos-u-resize--vertical preview-result preview-result-error';
            this.previewresult = JSON.stringify(err);
          }
        );
      }
    }
  }

  onDatasetChange() {
    // this.nodemapping.mappedsource = this.selectedDatasetId;
    if (this.selectedDatasetId != '') {
      this.nodemapping.mappedsource = this.connectordatasets.find(ds => ds.datasetid == this.selectedDatasetId);
      this.nodemapping.mappedsourcename = this.nodemapping.mappedsource.name;
      this.nodemapping.connimagesrc = '../assets/images/database.svg';
      this.nodemapping.ismapped = 'Yes';
      this.nodemapping.mappedclass = "status-mapped";
    }
    else {
      this.nodemapping.mappedsource = null;
      this.nodemapping.mappedsourcename = '';
      this.nodemapping.connimagesrc = '';
      this.nodemapping.ismapped = 'No';
      this.nodemapping.mappedclass = "status-unmapped";

    }

    this.getAttributeMapping();
    this.onAttributeValueChange(null, null);
    this.changeDetectorRefs.detectChanges();
    this.onChange.emit(this.nodemapping);
  }

  onAttributeValueChange(nodeattribute, event) {
    var updatedAttributeMapping = [];

    if (!this.utilities.IsNull(event.target) && !this.utilities.IsNull(event.target.value) 
        && !this.utilities.IsNull(nodeattribute)) {
      console.log('nodeattribute - connattribute => ', nodeattribute, event.target.value);

      for (let i = 0; i < this.attributemapping.length; i++) {
        if (this.attributemapping[i].nodeattr.toLowerCase() == nodeattribute.toLowerCase()) {
          this.attributemapping[i].connectorattr = event.target.value;
        }

        if (!this.utilities.IsNull(this.attributemapping[i].connectorattr) &&
          this.attributemapping[i].connectorattr.trim() != '') {
          updatedAttributeMapping.push(this.attributemapping[i]);
        }
      }
    }
    else {
      updatedAttributeMapping = this.attributemapping;
    }

    var datasetAttributeMappingfound = false;
    if (!this.utilities.IsNull(this.nodemapping.connectordatasets) && this.nodemapping.connectordatasets.length > 0) {
      for (let i = 0; i < this.nodemapping.connectordatasets.length; i++) {
        if (!this.utilities.IsNull(this.nodemapping.connectordatasets[i].attributemapping)
          && !this.utilities.IsNull(this.nodemapping.connectordatasets[i].dataset)
          && this.nodemapping.connectordatasets[i].dataset.datasetid == this.selectedDatasetId) {
          this.nodemapping.connectordatasets[i].attributemapping = updatedAttributeMapping;
          datasetAttributeMappingfound = true;
        }
      }
    }

    if (!datasetAttributeMappingfound) {
      console.log('new dataset attr mapping => ', updatedAttributeMapping);
      var newdatset = this.datasets.find(ds => ds.datasetid == this.selectedDatasetId);
      this.nodemapping.connectordatasets.push({
        "dataset": newdatset,
        "default": true,
        "attributemapping": updatedAttributeMapping
      });
    }

    this.onChange.emit(this.nodemapping);
  }

  onInputParamsChange(attrname, paramcol, oldvalue, event) {
    var updatedParams = [];
    console.log('paramcol, oldvalue, event =>', paramcol, oldvalue, event);
    for (let i = 0; i < this.inputparams.length; i++) {
      if (this.inputparams[i]["name"].toLowerCase() == attrname.toLowerCase()
        && this.inputparams[i][paramcol] == oldvalue) {
        this.inputparams[i][paramcol] = (paramcol != 'sourcegroupby') ? event.target.value
          : ((event.target.value != '') ? [event.target.value] : []);
      }

      if (this.inputparams[i].sourcetype.trim() != '' &&
        this.inputparams[i].sourcename.trim() != '' &&
        this.inputparams[i].sourcevalue.trim() != '') {
        updatedParams.push(this.inputparams[i]);
      }

      //if sourcetype is selected as intententity and sourcevalue is blank then show it as 'value' by default
      if (paramcol == "sourcetype" && event.target.value.toLowerCase() == "intententity" && this.inputparams[i].sourcevalue.trim() == '') {
        this.inputparams[i].sourcevalue = 'value';
      }


      //On sourcename changes, set the sourcevalue nodeattributes
      if (paramcol == "sourcename" && this.inputparams[i]["name"].toLowerCase() == attrname.toLowerCase()) {
        let mapping = this.nodemappings.find(nd => nd.node.nodeid == this.inputparams[i].sourcename);
        if (!this.utilities.IsNull(mapping)) {
          this.basenodeattributes[attrname] = (mapping.nodetype.toLowerCase() == 'entity')
            ? mapping.basenodeattributes : [{ "name": "AggregateValue" }];
        }
      }
    }

    // this.inputparams = updatedParams;
    if (this.nodemapping.nodeproperties == null || typeof this.nodemapping.nodeproperties == 'undefined') {
      this.nodemapping.nodeproperties = {};
    }

    this.nodemapping.nodeproperties.inputparameters = updatedParams;

    this.onChange.emit(this.nodemapping);
  }

  onOutputParamsAllCols(event) {
    if (this.outputAllCols) {
      for (let i = 0; i < this.outputparams.length; i++) {
        this.outputparams[i].selected = false;
      }
      this.nodemapping.nodeproperties.output = [];
      this.onChange.emit(this.nodemapping);
    }
  }

  // (change)="onOutputParamsChange(param.name,$event)"
  onOutputParamsChange(outputcol, newvalue) {
    var updatedParams = [];
    console.log('onOutputParamsChange - outputcol, newvalue =>', outputcol, newvalue, this.outputparams);


    for (let i = 0; i < this.outputparams.length; i++) {
      if (this.outputparams[i].name == outputcol) {
        this.outputparams[i].selected = newvalue;
      }

      if (this.outputparams[i].selected) {
        updatedParams.push(this.outputparams[i].name);
        this.outputAllCols = false;
      }
    }

    if (this.outputAllCols) {
      this.nodemapping.nodeproperties.output = [];
    }
    else {
      this.nodemapping.nodeproperties.output = updatedParams;
    }

    this.onChange.emit(this.nodemapping);
  }

  getInputParamPropValue(attrname, propname) {
    for (let i = 0; i < this.inputparams.length; i++) {
      if (this.inputparams[i]["name"].toLowerCase() == attrname.toLowerCase()) {
        return this.inputparams[i][propname];
      }
    }
    return '';
  }



}