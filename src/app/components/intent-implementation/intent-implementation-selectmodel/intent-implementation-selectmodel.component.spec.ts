import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntentImplementationSelectModelComponent } from './intent-implementation-selectmodel.component';

describe('IntentImplementationSelectModelComponent', () => {
  let component: IntentImplementationSelectModelComponent;
  let fixture: ComponentFixture<IntentImplementationSelectModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntentImplementationSelectModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentImplementationSelectModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
