import {
  Component, OnInit, ChangeDetectorRef, ViewEncapsulation, AfterViewInit, ElementRef,
  Input, Output, EventEmitter, ViewChild, ChangeDetectionStrategy, OnChanges
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService, AlertService } from '../../../shared/service';
import { User } from '../../../shared/models';
import { Utilities } from '../../../shared/utils';

const gojs = require('../../../shared/libs/gojs/go');

@Component({
  selector: 'mercer-intent-implementation-selectmodel',
  templateUrl: './intent-implementation-selectmodel.component.html',
  styleUrls: ['./intent-implementation-selectmodel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class IntentImplementationSelectModelComponent implements OnInit, AfterViewInit, OnChanges {

  @ViewChild('intentFulfillmentGraph') divIntentFulfillmentGraph;
  @ViewChild('txtmodel') txtmodel: ElementRef;

  @Input() counter: number;
  @Input() modelid: String;
  @Input() models: any[];


  @Output() onChange: EventEmitter<any> = new EventEmitter<any>();


  currentUser: User;
  username: String = '';

  public filtermodels: any[];
  public filtermodelname: String;
  private utilities: Utilities;

  public selectedmodelid: String;

  goJSIntentFulfillmentGraph: any;

  constructor(
    private authService: AuthenticationService,
    private alertService: AlertService,
    private changeDetectorRefs: ChangeDetectorRef
  ) {
    this.utilities = new Utilities();
    this.currentUser = this.authService.getUserInfo();
    this.username = (this.currentUser != null && this.currentUser.username !== 'undefined') ? this.currentUser.username : 'none';

    this.filtermodelname = '';
    this.filtermodels = [];

    this.selectedmodelid = this.modelid;
  }

  ngOnInit() {
    this.filtermodelname = '';
    this.selectedmodelid = this.modelid;
    this.alertService.clear();
  }

  ngAfterViewInit() {
    this.filtermodelname = '';
    this.selectedmodelid = this.modelid;
    this.initializeGraphCanvas();
  }

  ngOnChanges() {
    //console.log('this.models =>', this.models);
    this.filtermodelname = '';
    this.selectedmodelid = this.modelid;
    this.filtermodels = this.models;
    
    var selectedmodel = this.models.find(md => md._id == this.selectedmodelid);
    console.log('selected model =>', selectedmodel);
    this.onModelSelection(selectedmodel);
  }

  applyfilter(event: any) {
    this.filtermodels = [];
    let searchname = this.filtermodelname.toLowerCase();

    for (let i = 0; i < this.models.length; i++) {
      let modelname = this.models[i].name.toLowerCase();
      if (modelname.indexOf(searchname) > -1) {
        this.filtermodels.push(this.models[i]);
      }
    }

  }

  onModelSelection(selectedIntentffModelGraph: any) {
    // console.log('onModelSelection =>', selectedIntentffModelGraph, event);

    if (!this.utilities.IsNull(selectedIntentffModelGraph) && !this.utilities.IsNull(selectedIntentffModelGraph._id)) {
      console.log('onModelSelection');
      this.selectedmodelid = selectedIntentffModelGraph._id;
      this.onChange.emit(selectedIntentffModelGraph);

      if (selectedIntentffModelGraph != null && selectedIntentffModelGraph.graphjson != null
        && selectedIntentffModelGraph.graphjson != 'undefined') {

        var graphJson = selectedIntentffModelGraph.graphjson;
        console.log('selet model - selectedIntentffModelGraph.graphjson =>', graphJson);
        this.goJSIntentFulfillmentGraph.model = new gojs.GraphLinksModel(graphJson.node, graphJson.link);
        this.txtmodel.nativeElement.focus();
      }
    }
  }

  initializeGraphCanvas() {
    var $ = gojs.GraphObject.make;

    var intentFulfillmentDiv = this.divIntentFulfillmentGraph.nativeElement;

    // var goJSIntentFulfillmentGraphRef = this.goJSIntentFulfillmentGraph;
    this.goJSIntentFulfillmentGraph = $(gojs.Diagram, intentFulfillmentDiv,
      {
        initialContentAlignment: gojs.Spot.Center,

        allowDragOut: false,
        allowDrop: false,
        "commandHandler.archetypeGroupData": { isGroup: true },
        // handle undo or redo maybe changing Diagram properties controlled by check boxes
        "ModelChanged": function (e) {
          // if (e.isTransactionFinished) allChecks(true);
        }
      });

    // define a simple Node template
    this.goJSIntentFulfillmentGraph.nodeTemplate =
      $(gojs.Node, "Auto",  // the Shape will go around the TextBlock
        $(gojs.Shape, "RoundedRectangle",//Circle
          { portId: "", fromLinkable: true, toLinkable: true, cursor: "pointer", strokeWidth: 0, fill: "#00c0ef" },
          // Shape.fill is bound to Node.data.color
          new gojs.Binding("fill", "color")),
        $(gojs.TextBlock,
          { margin: 8 },  // some room around the text
          // TextBlock.text is bound to Node.data.key
          new gojs.Binding("text", "text"))
      );

    this.goJSIntentFulfillmentGraph.model = new gojs.GraphLinksModel([], []);
    this.goJSIntentFulfillmentGraph.allowDragOut = this.goJSIntentFulfillmentGraph.allowDrop = this.goJSIntentFulfillmentGraph.allowCopy = this.goJSIntentFulfillmentGraph.allowDelete = this.goJSIntentFulfillmentGraph.allowInsert = false;
    this.goJSIntentFulfillmentGraph.allowMove = true;
    this.goJSIntentFulfillmentGraph.isReadOnly = true;
    this.goJSIntentFulfillmentGraph.model.undoManager.isEnabled = true;

  }
}  