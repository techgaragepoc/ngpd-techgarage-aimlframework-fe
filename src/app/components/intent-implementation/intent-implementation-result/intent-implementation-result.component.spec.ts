import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntentImplementationResultComponent } from './intent-implementation-result.component';

describe('IntentImplementationResultComponent', () => {
  let component: IntentImplementationResultComponent;
  let fixture: ComponentFixture<IntentImplementationResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntentImplementationResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntentImplementationResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
