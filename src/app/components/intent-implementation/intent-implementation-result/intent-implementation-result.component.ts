import {
    Component, OnInit, ChangeDetectorRef, ViewEncapsulation, AfterViewInit,
    Input, Output, EventEmitter, ViewChild, ChangeDetectionStrategy, OnChanges
} from '@angular/core';

import { AuthenticationService, AlertService } from '../../../shared/service';
import { User } from '../../../shared/models';
import { Utilities } from '../../../shared/utils';


@Component({
    selector: 'mercer-intent-implementation-result',
    templateUrl: './intent-implementation-result.component.html',
    styleUrls: ['./intent-implementation-result.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class IntentImplementationResultComponent implements OnInit, AfterViewInit, OnChanges {

    @Input() processedmodel: any;

    @Output() onChange: EventEmitter<any> = new EventEmitter<any>();

    currentUser: User;
    username: String = '';

    private utilities: Utilities;

    public resultDataTable: any = {};

    constructor(
        private authService: AuthenticationService,
        private alertService: AlertService,
        private changeDetectorRefs: ChangeDetectorRef
    ) {
        this.utilities = new Utilities();
        this.currentUser = this.authService.getUserInfo();
        this.username = (this.currentUser != null && this.currentUser.username !== 'undefined') ? this.currentUser.username : 'none';
        this.resultDataTable = {};
    }

    ngOnInit() {
        this.resultDataTable = {};
    }

    ngAfterViewInit() {

    }

    ngOnChanges() {
        console.log('processmodel =>', this.processedmodel);

        if (!this.utilities.IsNull(this.processedmodel)) {
            this.resultDataTable = this.getRowCols(this.processedmodel.resultrows);
            console.log('this.resultDataTable =>', this.resultDataTable);
        }
    }

    getRowCols(data): any {
        var cols = [];
        var rows = [];

        if (!this.utilities.IsNull(data)) {
            for (let i = 0; i < data.length; i++) {
                var row = data[i];
                var rowToPrint = {};

                for (let j = 0; j < Object.keys(row).length; j++) {
                    var colname = Object.keys(row)[j];
                    var colval = row[colname];
                    //collect result columns
                    if (i == 0) {
                        cols.push(colname);
                    }
                    rowToPrint[colname] = (this.utilities.IsArray(colval) || typeof colval === 'object')
                                            ?JSON.stringify(colval): colval;
                }
                rows.push(rowToPrint);
            }
        }

        return {
            columns: cols,
            rows: rows
        };
    }

}