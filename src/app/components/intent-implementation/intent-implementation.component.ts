import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy, OnInit, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReplaySubject } from '../../../../node_modules/rxjs';
import { AuthenticationService, AlertService, IntentImplementationService } from '../../shared/service';


@Component({
  selector: 'intent-implementation',
  styleUrls: ['./intent-implementation.component.scss'],
  moduleId: module.id.toString(),
  templateUrl: 'intent-implementation.component.html'
})
export class IntentImplementationComponent implements OnInit, OnChanges {

  public currentroute: String = '';
  public projectname: String = 'Health BOT';
  public projectcategory: String = 'HEALTH';

  public nlpmodelid: String = '';
  public nlpintentid: String = '';
  public intentmodelgraphid: String = '';

  public navigationdata = [
    { label: '', icon: 'home', url: '/' },
    { label: 'Project Category', icon: '', url: '/' },
    { label: 'Project Name', icon: '', url: '/' },
    { label: 'NLP Model & Intent Category', icon: '', url: '/' },
    { label: 'Intentfulfillment Model', icon: '', url: '/' },
  ];

  constructor(private router: Router, 
    private route: ActivatedRoute,
  private intentImplementationService: IntentImplementationService) {
    this.currentroute = this.router.url.replace('/', '');
  }

  ngOnInit() {
    this.currentroute = this.router.url.replace('/', '');
    this.getParams();
  }

  ngOnChanges() {
    this.getParams();
  }

  getParams() {
    this.route.queryParamMap.subscribe(qpMap => {
      // console.log('params => ', pMap["params"]);
      var params = qpMap["params"];

      this.nlpmodelid = params["nlpmodel"];
      this.nlpintentid = params["nlpintent"];
      this.intentmodelgraphid = params["intentffmodelgraphid"];

      console.log('params received =>', this.nlpmodelid, this.nlpintentid, this.intentmodelgraphid);
      var projectid = '';
      this.intentImplementationService.getProjectdetail(projectid).subscribe(
        project => console.log('project detail =>', project)
      )
    });

  }
}
