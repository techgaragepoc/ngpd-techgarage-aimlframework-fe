import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutMercerComponent } from './about-mercer.component';

describe('AboutMercerComponent', () => {
  let component: AboutMercerComponent;
  let fixture: ComponentFixture<AboutMercerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutMercerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutMercerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
