import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../../shared/service';

@Component({
  selector: 'mercer-terms-of-use',
  moduleId: module.id.toString(),
  templateUrl: './terms-of-use.component.html',
  styleUrls: ['./terms-of-use.component.scss']
})

export class TermsOfUseComponent {

  headerImageToShow: any;
  imageClickUrl = '';
  noImageUrl = 'assets/img/bannerbg.png';
  constructor(
    private _router: Router,
    private _activeroute: ActivatedRoute,
    private _renderer: Renderer2,
    private sharedService: SharedService
  ) { }

  getHeaderImage(countryCode: string): void {

    const imageKey = 'TopBanner';
    this.imageClickUrl = '';
    // this.imageService.getImage('Common', countryCode, imageKey)
    //   .subscribe(response => {
    //     this.imageClickUrl = response.headers.get('ImageUrl');

    //     const file = new Blob([response.blob()], { type: 'image/jpeg' });

    //     const reader = new FileReader();
    //     reader.addEventListener('load', () => {
    //       this.headerImageToShow = reader.result;
    //     }, false);

    //     if (file) {
    //       reader.readAsDataURL(file);
    //     }
    //   },
    //   (err) => {
    //     console.log(err)
    //   }
    //   );
  }

  HeaderImageClick() {
    if (this.imageClickUrl !== '') {
      window.open('https://' + this.imageClickUrl, '_blank');
    }
  }
}
