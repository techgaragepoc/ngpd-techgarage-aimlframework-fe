import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, OnInit } from '@angular/core';
import { XHRBackend, RequestOptions } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';

import { MercerOSModule } from 'merceros-ui-components';

import { AppRoutingModule } from './app-routing.module';

import {
  HeaderComponent, FooterComponent, ModalComponent,
  AlertComponent, SideMenuComponent, PageMenuComponent,
  BreadCrumbComponent, IntentAnalysisComponent, HighlightedTextComponent,
  DynamicContentComponent
} from './shared/component';

import { NoContentComponent } from './components/no-content';
import { NLPDashboardComponent, TextClassificationComponent, AddUtteranceComponent } from './components/nlp';

import { LoginComponent, RegisterComponent, ChangePasswordComponent, ForgotPasswordComponent } from './components/user';

import { IntentComponent } from './components/Intent/intent.component';
import { IntentService } from './components/Intent/intent.service';

import { TermsOfUseComponent, AboutMercerComponent, PrivacyPolicyComponent } from './components/quick-links';
import { ModelMasterComponent, ModelDashboardComponent, ModelHeaderComponent, ModelEditorComponent } from './components/model';
import { CreateProjectComponent, ProjectCatalogComponent, ProjectHeaderComponent } from './components/project';

import {
  IntentFulfillmentStep2Component,
  IntentFulfillmentStep3Component,
  IntentFulfillmentMainComponent,
  IntentFulfillDashboardComponent
} from './components/intent-fulfillment';

import {
  EnterpriseConnectorComponent, EcConnectorComponent, EcConnectortypeComponent,
  EcDatasetComponent, EcDatasetSrcComponent, EcDatasetSrcRDBMSComponent,
  EcDatasetSrcMongoComponent, EcDatasetSrcRestAPIComponent, EcDatasetSrcSoapAPIComponent,
  EcContypeSqlServerComponent, EcContypeMongDBComponent,
  EcContypeRestAPIComponent, EcContypeOracleComponent, EcContypeSoapAPIComponent,
  EcContypeDefaultComponent,
} from './components/enterprise-connector';

import {
  IntentImplementationComponent, IntentImplementationNodeMappingComponent,
  IntentImplementationPropertyComponent, IntentImplementationSelectModelComponent,
  IntentImplementationResultComponent
} from './components/intent-implementation';

import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { AuthGuard } from './guards';

import {
  SharedService, AlertService, AuthenticationService, UserService, ModelService,
  ConnectorService, ConnectorTypeService, DataSetService, DataService, CommonService,
  TextClassificationService, NlpService, ProjectService, IntentModelDataService,
  IntentFulfillmentModelService, IntentImplementationService, DomainModelService
} from './shared/service';

// import { AppHttpService } from './shared/loader/apphttp.service';
// import { HttpServiceFactory } from './shared/loader/httpservice-factory';

import { reducers, metaReducers } from './reducers';
import { CustomRouterStateSerializer } from './shared/utils';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './shared/interceptor/token.interceptor';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MercerOSModule,
    CommonModule,
    AppRoutingModule,
    BrowserModule,
    /**
     * StoreModule.forRoot is imported once in the root module, accepting a reducer
     * function or object map of reducer functions. If passed an object of
     * reducers, combineReducers will be run creating your application
     * meta-reducer. This returns all providers for an @ngrx/store
     * based application.
     */
    StoreModule.forRoot(reducers, { metaReducers }),

    /**
     * @ngrx/router-store keeps router state up-to-date in the store.
     */
    StoreRouterConnectingModule.forRoot({
      /*
        They stateKey defines the name of the state used by the router-store reducer.
        This matches the key defined in the map of reducers
      */
      stateKey: 'router',
    }),

    /**
     * Store devtools instrument the store retaining past versions of state
     * and recalculating new states. This enables powerful time-travel
     * debugging.
     *
     * To use the debugger, install the Redux Devtools extension for either
     * Chrome or Firefox
     *
     * See: https://github.com/zalmoxisus/redux-devtools-extension
     */
    StoreDevtoolsModule.instrument({
      name: 'NgRx Store DevTools',
      logOnly: environment.production,
    }),

    /**
     * EffectsModule.forRoot() is imported once in the root module and
     * sets up the effects class to be initialized immediately when the
     * application starts.
     *
     * See: https://github.com/ngrx/platform/blob/master/docs/effects/api.md#forroot
     */
    EffectsModule.forRoot([]),
  ],
  providers: [
    /**
     * The `RouterStateSnapshot` provided by the `Router` is a large complex structure.
     * A custom RouterStateSerializer is used to parse the `RouterStateSnapshot` provided
     * by `@ngrx/router-store` to include only the desired pieces of the snapshot.
     */
    { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer },
    AuthGuard,
    SharedService,
    AuthenticationService,
    UserService,
    TextClassificationService,
    NlpService,
    IntentService,
    AlertService,
    ModelService,
    ConnectorService,
    ConnectorTypeService,
    DataService,
    DataSetService,
    ProjectService,
    IntentModelDataService,
    DomainModelService,
    CommonService,
    IntentFulfillmentModelService,
    IntentImplementationService,
    DomainModelService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }

    // { provide: AppHttpService, useFactory: HttpServiceFactory, deps: [XHRBackend, RequestOptions] }
  ],
  declarations:
  [
    AppComponent, NoContentComponent, AlertComponent, ModalComponent,
    HeaderComponent, FooterComponent, SideMenuComponent, PageMenuComponent,
    BreadCrumbComponent, IntentAnalysisComponent, IntentComponent,
    HighlightedTextComponent, DynamicContentComponent,

    AboutMercerComponent, PrivacyPolicyComponent, TermsOfUseComponent,
    ModelMasterComponent, ModelDashboardComponent, ModelHeaderComponent, ModelEditorComponent,
    NLPDashboardComponent, TextClassificationComponent, AddUtteranceComponent,
    CreateProjectComponent, ProjectCatalogComponent, ProjectHeaderComponent,
    LoginComponent, RegisterComponent, ChangePasswordComponent, ForgotPasswordComponent,

    IntentFulfillmentMainComponent, IntentFulfillmentStep2Component, IntentFulfillmentStep3Component,
    IntentImplementationComponent, IntentImplementationNodeMappingComponent, IntentImplementationPropertyComponent,
    IntentImplementationSelectModelComponent, IntentFulfillDashboardComponent,
    IntentImplementationResultComponent,

    EnterpriseConnectorComponent, EcConnectorComponent, EcConnectortypeComponent,
    EcDatasetComponent, EcDatasetSrcComponent, EcDatasetSrcRDBMSComponent,
    EcDatasetSrcMongoComponent, EcDatasetSrcRestAPIComponent, EcDatasetSrcSoapAPIComponent,
    EcContypeSqlServerComponent, EcContypeMongDBComponent, EcContypeDefaultComponent,
    EcContypeRestAPIComponent, EcContypeOracleComponent, EcContypeSoapAPIComponent,
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
