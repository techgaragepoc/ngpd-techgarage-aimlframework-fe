import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent, RegisterComponent, ChangePasswordComponent, ForgotPasswordComponent } from './components/user';
import { NoContentComponent } from './components/no-content';
import { TermsOfUseComponent, AboutMercerComponent, PrivacyPolicyComponent } from './components/quick-links';
import { ModelMasterComponent, ModelDashboardComponent, ModelEditorComponent } from './components/model';

import {
  IntentFulfillmentStep2Component,
  IntentFulfillmentStep3Component, IntentFulfillmentMainComponent,
  IntentFulfillDashboardComponent
} from './components/intent-fulfillment';

import {
  EnterpriseConnectorComponent,
  EcConnectorComponent,
  EcConnectortypeComponent,
  EcDatasetComponent
} from './components/enterprise-connector';

import {
  IntentImplementationComponent,
  IntentImplementationNodeMappingComponent
} from './components/intent-implementation';

import { NLPDashboardComponent } from './components/nlp';
import { IntentComponent } from './components/Intent/intent.component';

import { TextClassificationComponent } from './components/nlp/text-classification/text-classification.component';
import { AddUtteranceComponent } from './components/nlp/add-Utterances/add-Utterances.component';

import {
  CreateProjectComponent,
  ProjectCatalogComponent
} from './components/project';

import { AuthGuard } from './guards/index';

const ROUTES: Routes = [
  // Default page
  { path: '', component: NoContentComponent },

  // Shared features
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'changepassword', component: ChangePasswordComponent, canActivate: [AuthGuard] },
  { path: 'resetpassword', component: ForgotPasswordComponent },

  { path: 'aboutmercer', component: AboutMercerComponent },
  { path: 'termsofuse', component: TermsOfUseComponent },
  { path: 'privacypolicy', component: PrivacyPolicyComponent },

  // Feature specific components start from here

  // Project Catalog
  { path: 'createproject', component: CreateProjectComponent, canActivate: [AuthGuard] },
  { path: 'projectcatalog', component: ProjectCatalogComponent, canActivate: [AuthGuard] },

  // NLP and its sub options
  { path: 'nlp', component: NLPDashboardComponent, canActivate: [AuthGuard] },
  { path: 'intent', component: IntentComponent, canActivate: [AuthGuard] },
  { path: 'intentclassification', component: TextClassificationComponent, canActivate: [AuthGuard] },
  { path: 'addutterance', component: AddUtteranceComponent, data: [{ isProd: true }], canActivate: [AuthGuard] },

  // Intent Fulfillment
  { path: 'intentfulfillment2', component: IntentFulfillmentStep2Component, canActivate: [AuthGuard] },
  { path: 'intentfulfillment3', component: IntentFulfillmentStep3Component, canActivate: [AuthGuard] },
  { path: 'intentfulfillment', component: IntentFulfillmentMainComponent, canActivate: [AuthGuard] },
  { path: 'intentlist', component: IntentFulfillDashboardComponent, canActivate: [AuthGuard] },

  { path: 'modelmaster', component: ModelMasterComponent, canActivate: [AuthGuard] },
  { path: 'modeldashboard', component: ModelDashboardComponent, canActivate: [AuthGuard] },
  { path: 'modeleditor', component: ModelEditorComponent },

  { path: 'intentimplementation', component: IntentImplementationComponent, canActivate: [AuthGuard] },

  { path: 'enterpriseconnector', component: EnterpriseConnectorComponent, canActivate: [AuthGuard] },
  // { path: 'ec-connector', component: EcConnectorComponent },
  // { path: 'ec-connectortype', component: EcConnectortypeComponent },
  // { path: 'ec-dataset', component: EcDatasetComponent },
  // { path: 'ec-domainentity', component: EcDomainEntityComponent },

  // { path: '', redirectTo: '/modelmaster', pathMatch: 'full' },
  { path: '', redirectTo: '/nocontent', pathMatch: 'full' },
  { path: '**', component: NoContentComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES, { useHash: false, preloadingStrategy: PreloadAllModules })
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
