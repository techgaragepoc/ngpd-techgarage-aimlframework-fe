import { RouterStateSerializer } from '@ngrx/router-store';
import { RouterStateSnapshot, Params, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
/**
 * The RouterStateSerializer takes the current RouterStateSnapshot
 * and returns any pertinent information needed. The snapshot contains
 * all information about the state of the router at the given point in time.
 * The entire snapshot is complex and not always needed. In this case, you only
 * need the URL and query parameters from the snapshot in the store. Other items could be
 * returned such as route parameters and static route data.
 */

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}

export class CustomRouterStateSerializer implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    let route = routerState.root;

    while (route.firstChild) {
      route = route.firstChild;
    }

    const { url, root: { queryParams } } = routerState;
    const { params } = route;

    // Only return an object including the URL, params and query params
    // instead of the entire snapshot
    return { url, params, queryParams };
  }
}

import * as CryptoJS from 'crypto-js';
import { JwtHelper } from 'angular2-jwt';
import { isNull } from 'util';
import { environment } from '../../environments/environment';
import { AuthenticationService } from './service';
import { User } from './models';

export class Utilities {
  // constructor(private authenticationService: AuthenticationService) { }

  public getRequestHeaders(allowCredentials: boolean): HttpHeaders {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    if (allowCredentials) {
      // headers.append('Authorization', 'Bearer ' + this.authenticationService.token);
      headers.append('Access-Control-Allow-Credentials', 'true');
    }

    return headers;
  }

  public toHttpParams(obj: Object): HttpParams {
    return Object.getOwnPropertyNames(obj).reduce((p, key) => p.set(key, obj[key]), new HttpParams());
  }

  // enCrypt test/string data using below Function
  public encryptData(value): string {
    const cryptoKey = environment.cryptoSecretKey;

    try {
      const encrypted = CryptoJS.AES.encrypt(value, cryptoKey);
      // console.log('encrypted Data:' + encrypted);
      return encrypted.toString();
    } catch (error) {
      console.log('encryptData:error:' + error);
      return null;
    }
  }

  // enCrypt test/string data using below Function
  public decryptData(value): string {
    const cryptoKey = environment.cryptoSecretKey;

    try {
      const decrypted = CryptoJS.AES.decrypt(value, cryptoKey).toString(CryptoJS.enc.Utf8);
      // console.log('decrypted Data:' + decrypted);
      return decrypted;
    } catch (error) {
      console.log('decryptData:error:' + error);
      return null;
    }
  }

  // Encrypt Local Storages
  public encryptLocalStore(key, value): void {
    const encryptedData = this.encryptData(value);
    window.localStorage.setItem(key, encryptedData);
  }

  // Decrypt Local Storage
  public decryptLocalStore(key): string {
    const encryptedData = window.localStorage.getItem(key);

    if (!isNull(encryptedData)) {
      return this.decryptData(encryptedData);
    }

    return null;
  }

  public ValidateUserToken(user: User): boolean {
    let isTokenValid = false;

    if (!isNull(user) && user.token) {
      let jwtHelper = new JwtHelper();

      // If the token is expired, this function will return true
      isTokenValid = (jwtHelper.isTokenExpired(user.token) === false);

      jwtHelper = null;
    }

    return isTokenValid;
  }

  public GetQueryStringParam(route: ActivatedRoute, paramName: string) {
    if (route == null) {
      return null;
    }

    return route.snapshot.queryParamMap.get(paramName);
  }

  public IsNull(value: any) {
    if (value == null || value === '' || value === 'undefined' ||
      (typeof value === 'object' && this.IsEmpty(value))) {
      return true;
    } else {
      return false;
    }
  }

  public IsEmpty(obj: any) {

    for (const key in obj) {
      if (obj.hasOwnProperty(key)) { return false; }
    }
    return true;
  }

  public ReplaceAll(source: string, search: string, replacement: string) {
    return source.replace(new RegExp(search, 'g'), replacement);
  }

  public IsArray(value: any) {
    return value && value !== 'undefined' && typeof value === 'object' && value.constructor === Array;
  }

  public SortArray(array: Array<any>, prop: any) {
    if (!array || array === undefined || array.length === 0) return null;

    array.sort((a: any, b: any) => {
      let x = (typeof a[prop] != 'undefined') ? a[prop] : '';
      let y = (typeof b[prop] != 'undefined') ? b[prop] : '';

      x = (typeof x === 'string' && x.indexOf('.') > -1) ? x.replace('.', '').toLowerCase() : x;
      y = (typeof y === 'string' && x.indexOf('.') > -1) ? y.replace('.', '').toLowerCase() : y;

      if (x < y) {
        return -1;
      } else if (x > y) {
        return 1;
      } else {
        return 0;
      }
    });

    return array;
  }

  public GetKeyValue(obj, searchkey) {
    if (obj != null && obj !== 'undefined' && typeof obj === 'object') {
      for (let i = 0; i < Object.keys(obj).length; i++) {
        const key = Object.keys(obj)[i];
        if (key.toLowerCase() === searchkey.toLowerCase()) {
          return obj[key];
        }
      }
    }
    return null;
  }

  public isEditingAllowed(model: any, user: User) {
    if (model == null) {
      return false;
    }

    return user.admin || model.updatedBy._id === user._id || (model.shared === false && model.sysdefined === false);
  }
}
