import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { User } from '../../shared/models/User';
import { Utilities } from '../utils';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(environment.apiEndpoint + '/users');
    }

    getById(_id: string) {
        return this.http.get(environment.apiEndpoint + '/users/' + _id);
    }

    create(user: User) {
        if (user && user.password) {
            user.password = new Utilities().encryptData(user.password);
        }

        return this.http.post(environment.apiEndpoint + '/users/register', user);
    }

    update(user: User) {
        return this.http.put(environment.apiEndpoint + '/users/' + user._id, user);
    }

    delete(_id: string) {
        return this.http.delete(environment.apiEndpoint + '/users/' + _id);
    }

    changePassword(_id: string, currpwd: string, newpwd: string) {
        let utils = new Utilities();
        const body = { currpwd: utils.encryptData(currpwd), newpwd: utils.encryptData(newpwd) };
        // console.log('changepassword: ' + JSON.stringify(body) + ":" + currpwd);
        utils = null;

        return this.http.put(environment.apiEndpoint + '/users/changepassword/' + _id, body);
    }

    resetPassword(user: User) {
        const body = { username: user.username, email: user.email };
        return this.http.post(environment.apiEndpoint + '/users/resetpassword', body);
    }
}
