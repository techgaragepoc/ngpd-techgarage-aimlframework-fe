import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { environment } from '../../../environments/environment';
import { Category, Culture } from '../../shared/models';
import { Utilities } from '../utils';

@Injectable()
export class CommonService {
    constructor(private http: HttpClient) { }

    getAllCultures() {
        return this.http.get<any>(environment.apiEndpoint + '/common/culture');
    }

    getCultureByCode(code) {
        return this.http.get<any>(environment.apiEndpoint + '/common/culture/' + code);
    }
}
