import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Connector } from '../../shared/models/ec.connector';

@Injectable()
export class ConnectorService {
    connectorServiceEndpoint: string;
    // getheaders: HttpHeaders;
    // postheaders: HttpHeaders;

    constructor(
        private http: HttpClient
        // private authenticationservice: AuthenticationService
    ) {
        this.connectorServiceEndpoint = environment.apiEndpoint + '/ec/connector/';
    }

    /*
        initializeHeaders() {
            // this.getheaders = this.authenticationservice.GetAuthenticationSecurityHeader();
            this.getheaders.append('content-type', 'application/json');

            // this.postheaders = this.authenticationservice.GetAuthenticationSecurityHeader();
            this.postheaders.append('content-type', 'application/x-www-form-urlencoded');
        }*/

    createConnector(connector: Connector) {
        return this.http.post(this.connectorServiceEndpoint, connector);
    }

    updateConnector(connector: Connector) {
        return this.http.put(this.connectorServiceEndpoint, connector);
    }

    deleteConnector(connectorid: String) {
        return this.http.delete(this.connectorServiceEndpoint + connectorid);
    }

    getConnectorById(connectorid: String) {
        return this.http.get<any>(this.connectorServiceEndpoint + connectorid);
    }

    getConnectorByName(connectorname: String) {
        return this.http.get<any>(this.connectorServiceEndpoint + 'name/' + connectorname);
    }

    getAllConnectors() {
        return this.http.get<any>(this.connectorServiceEndpoint);
    }
}
