import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { DataSet } from '../../shared/models/ec.dataset';

@Injectable()
export class DataSetService {
    datasetServiceEndpoint: string;

    // getheaders: HttpHeaders;
    // postheaders: HttpHeaders;

    constructor(
        private http: HttpClient
        // private authenticationservice: AuthenticationService
    ) {
        this.datasetServiceEndpoint = environment.apiEndpoint + '/ec/dataset/';
    }

    /* initializeHeaders() {
        //this.getheaders = this.authenticationservice.GetAuthenticationSecurityHeader();
        this.getheaders.append('content-type', 'application/json');

        //this.postheaders = this.authenticationservice.GetAuthenticationSecurityHeader();
        this.postheaders.append('content-type', 'application/x-www-form-urlencoded');
    } */

    createDataSet(dataset: DataSet) {
        return this.http.post(this.datasetServiceEndpoint, dataset);
    }

    updateDataSet(dataset: DataSet) {
        return this.http.put(this.datasetServiceEndpoint, dataset);
    }

    deleteDataSet(datasetid: String): any {
        return this.http.delete(this.datasetServiceEndpoint + datasetid);
    }

    getDataSetById(datasetid: String): any {
        return this.http.get(this.datasetServiceEndpoint + datasetid);
    }

    getDataSetByName(datasetname: String): any {
        return this.http.get(this.datasetServiceEndpoint + 'name/' + datasetname);
    }

    getAllDataSet(): any {
        return this.http.get(this.datasetServiceEndpoint);
    }
}
