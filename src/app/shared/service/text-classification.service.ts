import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { IntentCategory, IntentCategoryUtterance } from '../models/Intents';
import 'rxjs/add/operator/do';

@Injectable()
export class TextClassificationService {
    constructor(private http: HttpClient) { }

    getAll(): Observable<any> {
        return this.http.get(environment.apiEndpoint + '/Intent/getAll'); 
    }
    getByModel(modelName: string): Observable<any> {
        return this.http.get<any>(environment.apiEndpoint + '/Intent/intentCategories?modelName=' + encodeURIComponent(modelName));
    }
    createIntent(intentData: IntentCategory) {
        return this.http.post<any>(environment.apiEndpoint + '/Intent/createIntent', intentData);
    }
    deleteIntent(id: string) {
        return this.http.delete<any>(environment.apiEndpoint + '/Intent/' + id);
    }
    addIntentUtterance(intentData: IntentCategoryUtterance) {
        return this.http.post<any>(environment.apiEndpoint + '/Intent/addIntentUtterance', intentData);
    }
    getIntentUtteranceById(_id: string) {
        return this.http.get<any>(environment.apiEndpoint + '/Intent/utterance/' + _id);
    }
    getIntentUtterances(modelName: string, categoryName: string): Observable<any> {
        let url = environment.apiEndpoint + '/Intent/getIntentUtterance?categoryName=' + encodeURIComponent(categoryName);
        url += '&modelName=' + encodeURIComponent(modelName);

        return this.http.get(url);
    }
    getUtterancesByModel(modelName: string) {
        const url = environment.apiEndpoint + '/Intent/utterances/' + encodeURIComponent(modelName);
        return this.http.get<any>(url);
    }
    deleteIntentUtterance(_id: string, modelName: string) {
        return this.http.delete<any>(environment.apiEndpoint + '/Intent/IntentUtterance/' + _id + '/' + encodeURIComponent(modelName));
    }
    // getIntents(intentData: string, modelName: string): Observable<any> {
    //     return this.http.get(environment.apiEndpoint + '/Intent/getIntents?intentData=' + intentData + '&modelName=' + modelName);
    // }
    getIntentsByUser(intentData: string, modelName: string, userName: string): Observable<any> {
        let url = environment.apiEndpoint + '/Intent/getIntentsByUser?intentData=';
        url += encodeURIComponent(intentData) + '&modelName=' + encodeURIComponent(modelName) + '&userName=' + encodeURIComponent(userName);

        return this.http.get(url);
    }
    // getTree(intentData: string, modelName: string): Observable<any> {
    //     return this.http.get(environment.apiEndpoint + '/Intent/getTree?intentData=' + intentData + '&modelName=' + modelName);
    // }
    getTreeByUser(intentData: string, modelName: string, userName: string): Observable<any> {
        let url = environment.apiEndpoint + '/Intent/getTreeByUser?intentData=';
        url += encodeURIComponent(intentData) + '&modelName=' + encodeURIComponent(modelName) + '&userName=' + encodeURIComponent(userName);

        return this.http.get(url);
    }
    trainModel(trainModelString: string, modelName: string): Observable<any> {
        let url = environment.apiEndpoint + '/Intent/trainModel?modelName=' + encodeURIComponent(modelName);
        url += '&trainModelString=' + encodeURIComponent(trainModelString);

        return this.http.post(url, modelName);
    }
    trainEntireModel(modelName: string, userName: string): Observable<any> {
        return this.http.post(environment.apiEndpoint + '/Intent/trainEntireModel', { modelName: modelName, userName: userName });
    }
    createEntity(entityModel: any, modelName: string): Observable<any> {
        const url = environment.apiEndpoint + '/Intent/createEntity'
        const payload = { modelName: encodeURIComponent(modelName), entityData: entityModel };

        return this.http.post(url, payload);
    }
    updateUtteranceScores(modelName: string, scores: any[]) {
        const url = environment.apiEndpoint + '/Intent/utteranceScores';
        return this.http.put<any>(url, { modelName: encodeURIComponent(modelName), utterances: scores });
    }
    updateUtterance(modelName: string, model: any) {
        const url = environment.apiEndpoint + '/Intent/utterance';
        return this.http.put<any>(url, { modelName: encodeURIComponent(modelName), info: model });
    }
}
