import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { Project, Category, ProjectOption } from '../../shared/models';
import { Utilities } from '../utils';

@Injectable()
export class ProjectService {
    constructor(private http: HttpClient) { }

    getCategories() {
        // console.log('projService called');
        return this.http.get<any>(environment.apiEndpoint + '/project/category');
    }

    createCategory(cat: Category) {
        return this.http.post<any>(environment.apiEndpoint + '/project/category', cat);
    }

    getProjectOptions() {
        return this.http.get<any>(environment.apiEndpoint + '/project/options');
    }

    getById(_id: string) {
        return this.http.get<any>(environment.apiEndpoint + '/project/' + _id);
    }

    getByUserId(_userId: string) {
        return this.http.get<any>(environment.apiEndpoint + '/project/user/' + _userId);
    }

    getByName(_name: string) {
        return this.http.get<any>(environment.apiEndpoint + '/project/name/' + _name);
    }

    update(proj: Project) {
        return this.http.put<any>(environment.apiEndpoint + '/project/' + proj._id, proj);
    }

    delete(_id: string) {
        return this.http.delete(environment.apiEndpoint + '/project/' + _id);
    }

    getAll() {
        return this.http.get<any>(environment.apiEndpoint + '/project');
    }

    create(proj: Project) {
        return this.http.post(environment.apiEndpoint + '/project', proj);
    }
}
