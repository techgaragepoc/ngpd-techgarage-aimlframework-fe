import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { DataSet, Connector, ConnectorType } from '../../shared/models';
import * as clone from 'clone';

@Injectable()
export class DataService {
    dataServiceEndpoint: string;

    // getheaders: HttpHeaders;
    // postheaders: HttpHeaders;

    constructor(
        private http: HttpClient
        // private authenticationservice: AuthenticationService
    ) {
        this.dataServiceEndpoint = environment.apiEndpoint + '/ec/data/';
    }

    testconnection(connectiondtl: Connector): any {
        let testconnectiondetail = clone(connectiondtl);
        testconnectiondetail.connectiondetail = JSON.parse(connectiondtl.connectiondetail.toString());
       
        const url = this.dataServiceEndpoint + 'testconnection';
        return this.http.post(url, testconnectiondetail);
    }

    getdata(entityid: String, entityname: String, entityparameters: any, maxrows: Number): any {
        const url = this.dataServiceEndpoint;
        const dataparams = {
            entityid: entityid,
            entity: entityname,
            parameters: entityparameters,
            maxrows: maxrows
        };

        const getdataHeaders = new HttpHeaders()
            .set('reqdata', JSON.stringify(dataparams))
            .set('content-type', 'application/json');

        const options = {
            headers: getdataHeaders
        };

        console.log('url and options....', url, options);
        return this.http.get<any>(url, options);
    }
}
