import { User, IFormData, IProject, ITextClassification } from '../../shared/models';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { AsyncSubject } from 'rxjs/AsyncSubject';

@Injectable()
export class SharedService {
  protected formData: IFormData;

  isLoading = new Subject<boolean>();

  loggedInUser = new AsyncSubject<User>();
  isAuth = new AsyncSubject<boolean>();

  constructor() {
    this.formData = { ProjectForm: {}, TextClassificationForm: {} } as IFormData;
  }

  get SiteLinks() {
    let siteLinks = [];
    siteLinks.push({ name: 'Project Catalog', url: '/projectcatalog', isPublic: true, showInHeader: true });
    siteLinks.push({ name: 'Create Project', url: '/createproject', isPublic: true, showInHeader: false });
    siteLinks.push({ name: 'Skill Areas', url: '/nlp', isPublic: false, showInHeader: true });
    siteLinks.push({ name: 'Intent Fulfillment', url: '/intentlist', isPublic: false, showInHeader: true });
    siteLinks.push({ name: 'Intent Implementation', url: '/intentimplementation', isPublic: false, showInHeader: true });
    return siteLinks;
  }

  get FormData() {
    return this.formData;
  }

  set FormData(value) {
    // console.log('setting form data:' + JSON.stringify(value));
    this.formData = value;
  }

  assignUserInfo(user: User) {
    // console.log('assignUserInfo:' + JSON.stringify(user));
    this.loggedInUser.next(user);
    this.loggedInUser.complete();
    this.isAuth.next(user != null && user.email != null && user.email !== 'NA');
    this.isAuth.complete();
  }

  clearUserInfo() {
    this.loggedInUser.next(null);
    this.loggedInUser.complete();
    this.isAuth.next(false);
    this.isAuth.complete();
  }

  getUserInfo(): Observable<User> {
    this.isAuth.next(this.loggedInUser != null);
    // console.log('getUserInfo:' + JSON.stringify(this.loggedInUser.asObservable()));
    return this.loggedInUser.asObservable();
  }

  isAuthenticated(): Observable<boolean> {
    return this.isAuth.asObservable();
  }
}
