import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { IntentFulfillmentModel } from '../../shared/models';

@Injectable()
export class IntentImplementationService {

    domainmodelServiceEndpoint: string;
    intentffmodelServiceEndpoint: string;

    nodedatasetmappingServiceEndpoint: string;
    functionsServiceEndpoint: string;
    synonymsServiceEndpoint: string;
    intentffmodelexecutionServiceEndpoint: string;

    domainmodelgraphServiceEndpoint: string;
    intentffmodelgraphServiceEndpoint: string;

    nlpIntentffModelMappingEndpoint: string;

    constructor(
        private http: HttpClient

    ) {
        this.domainmodelServiceEndpoint = environment.apiEndpoint + '/intentfulfillment/domainmodel/';
        this.intentffmodelServiceEndpoint = environment.apiEndpoint + '/intentfulfillment/model/';
        this.nodedatasetmappingServiceEndpoint = environment.apiEndpoint + '/intentfulfillment/nodedatasetmappings/';
        this.functionsServiceEndpoint = environment.apiEndpoint + '/intentfulfillment/functions/';
        this.synonymsServiceEndpoint = environment.apiEndpoint + '/intentfulfillment/synonyms/';
        this.intentffmodelexecutionServiceEndpoint = environment.apiEndpoint + '/intentfulfillment/engine/execute/';

        this.domainmodelgraphServiceEndpoint = environment.apiEndpoint + '/domainmodelgraph/';
        this.intentffmodelgraphServiceEndpoint = environment.apiEndpoint + '/intentfulfillmentgraph/';

        this.nlpIntentffModelMappingEndpoint = environment.apiEndpoint + '/nlpintentfulfillmentmapping/';
    }

    getIntentFulfillmentModelGraphById(intentfulfillmentModelGraphid: String) {
        return this.http.get<any>(this.intentffmodelgraphServiceEndpoint + intentfulfillmentModelGraphid);
    }

    getIntentFulfillmentModelGraphByName(intentfulfillmentModelname: String) {
        return this.http.get<any>(this.intentffmodelgraphServiceEndpoint + 'name/' + intentfulfillmentModelname);
    }

    getAllIntentFulfillmentGraphModels() {
        return this.http.get<any>(this.intentffmodelgraphServiceEndpoint);
    }

    getAllDomainModels() {
        return this.http.get<any>(this.domainmodelServiceEndpoint);
    }

    getIntentFulfillmentModelByGraphId(intentfulfillmentModelGraphid: String) {
        return this.http.get<any>(this.intentffmodelServiceEndpoint + 'graphid/' + intentfulfillmentModelGraphid);
    }

    getNodeDatasetMappingByModelNode(domainmodelgraphid: String, domainmodelnodeid: String) {
        return this.http.get<any>(this.nodedatasetmappingServiceEndpoint + 'modelnode/' + domainmodelgraphid + '/' + domainmodelnodeid);
    }

    getAllNodeDatasetMappings() {
        return this.http.get<any>(this.nodedatasetmappingServiceEndpoint);
    }

    updateNodeDatasetMappings(mapping: any) {
        return this.http.post<any>(this.nodedatasetmappingServiceEndpoint, mapping);
    }


    updateImplementationStatus(intentffmodelgraphid: String, implementationStatus: String, updatedBy: String) {
        let intentffMapping = {
            intentfulfillmentmodelgraphid: intentffmodelgraphid,
            implementationstatus: implementationStatus,
            lastmodifiedby: updatedBy
        };
        return this.http.put(this.nlpIntentffModelMappingEndpoint + 'status/', intentffMapping);
    }


    getAllFunctionsMetaData() {
        return this.http.get<any>(this.functionsServiceEndpoint);
    }

    updateIntentFulfillmentNodeProperties(intentffmodel: any) {
        return this.http.put<any>(this.intentffmodelServiceEndpoint + 'nodeproperties', intentffmodel);
    }

    getProjectdetail(projectid: String) {
        return this.http.get<any>(this.intentffmodelServiceEndpoint + 'project/' + projectid);
    }

    executeIntentFulfillmentModel(intentffmodel: any, nlpPOS: any) {
        const execinput = {
            "intentffmodel": intentffmodel,
            "nlpPOS": nlpPOS
        };

        return this.http.post<any>(this.intentffmodelexecutionServiceEndpoint, execinput);
    }

    getExecutionResult(sessionId: String, level: String = '0') {
        return this.http.get<any>(this.intentffmodelexecutionServiceEndpoint + sessionId + '/' + level);
    }

}
