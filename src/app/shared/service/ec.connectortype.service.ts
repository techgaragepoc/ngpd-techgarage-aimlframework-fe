import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ConnectorType } from '../../shared/models/ec.connectortype';

@Injectable()
export class ConnectorTypeService {
    connectortypeServiceEndpoint: string;
    // getheaders: HttpHeaders;
    // postheaders: HttpHeaders;

    constructor(
        private http: HttpClient
        // private authenticationservice: AuthenticationService
    ) {
        this.connectortypeServiceEndpoint = environment.apiEndpoint + '/ec/connectortype/';
    }

    /*
    initializeHeaders() {
        //this.getheaders = this.authenticationservice.GetAuthenticationSecurityHeader();
        this.getheaders.append('content-type', 'application/json');

        //this.postheaders = this.authenticationservice.GetAuthenticationSecurityHeader();
        this.postheaders.append('content-type', 'application/x-www-form-urlencoded');
    }*/

    createConnectorType(connectortype: ConnectorType) {
        return this.http.post(this.connectortypeServiceEndpoint, connectortype);
    }

    updateConnectorType(connectortype: ConnectorType) {
        return this.http.put(this.connectortypeServiceEndpoint, connectortype);
    }

    deleteConnectorType(connectortypeid: String) {
        return this.http.delete(this.connectortypeServiceEndpoint + connectortypeid);
    }

    getConnectorTypeById(connectortypeid: String) {
        return this.http.get<any>(this.connectortypeServiceEndpoint + connectortypeid);
    }

    getConnectorTypeByName(connectortypename: String) {
        return this.http.get<any>(this.connectortypeServiceEndpoint + 'name/' + connectortypename);
    }

    getConnectorTypeByCatgSubCatg(connectortypecategory: String, connectortypesubcategory: String) {
        let url = this.connectortypeServiceEndpoint + 'category/' + connectortypecategory;
        url += '/subcategory/' + connectortypesubcategory;

        return this.http.get<any>(url);
    }

    getAllConnectorType() {
        return this.http.get<any>(this.connectortypeServiceEndpoint);
    }
}
