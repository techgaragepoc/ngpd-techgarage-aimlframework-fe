import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { NlpLangaugeModel } from '../models/nlp';
import 'rxjs/add/operator/do';

@Injectable()
export class NlpService {
    constructor(private http: HttpClient) { }

    getByName(_modelName: string): Observable<any> {
        return this.http.get<any>(environment.apiEndpoint + '/nlp/name/' + _modelName);
    }
    getById(_id: string): Observable<any> {
        return this.http.get<any>(environment.apiEndpoint + '/nlp/' + _id);
    }
    getAll(): Observable<any> {
        return this.http.get<any>(environment.apiEndpoint + '/nlp');
    }
    getByUserId(_userId: string) {
        return this.http.get<any>(environment.apiEndpoint + '/nlp/user/' + _userId);
    }
    createNlpLanguageModel(intentData: NlpLangaugeModel) {
        // console.log('Model with date', JSON.stringify(intentData));
        return this.http.post<any>(environment.apiEndpoint + '/nlp', intentData);
    }
    updateLanguageModel(modelData: NlpLangaugeModel) {
        return this.http.put<any>(environment.apiEndpoint + '/nlp', modelData);
    }
    createModel(modelName: string) {
        // console.log('Model with date', modelName);
        return this.http.post<any>(environment.apiEndpoint + '/nlp/model?modelName=' + modelName, modelName);
    }
    deleteNlpLanguageModel(_id: string) {
        // console.log('Model with date', intentData);
        return this.http.delete<any>(environment.apiEndpoint + '/nlp/' + _id);
    }
}
