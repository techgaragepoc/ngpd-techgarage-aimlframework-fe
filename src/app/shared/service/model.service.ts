import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { Model } from '../../shared/models/model';
import { Utilities } from '../utils';

@Injectable()
export class ModelService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<any>(environment.apiEndpoint + '/models');
    }

    getById(id: string) {
        return this.http.get<any>(environment.apiEndpoint + '/models/' + id);
    }

    create(model: Model) {
        return this.http.post<any>(environment.apiEndpoint + '/models', model);
    }

    update(model: Model) {
        return this.http.put<any>(environment.apiEndpoint + '/models/' + model._id, model);
    }

    delete(_id: string) {
        return this.http.delete<any>(environment.apiEndpoint + '/models/' + _id);
    }
}
