import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { environment } from '../../../environments/environment';
import { User } from '../../shared/models';
import { Utilities } from '../utils';
// import { isNull } from 'util';

@Injectable()
export class AuthenticationService {
    public token: string;

    constructor(private http: HttpClient) {
        // set token if saved in local storage
        const decrypted = new Utilities().decryptLocalStore('currentUser');
        const localUser = JSON.parse(decrypted);
        this.token = localUser && localUser.token;
    }

    login(username: string, password: string) {
        const utils = new Utilities();
        const encPwd = utils.encryptData(password);

        return this.http.post<any>(environment.apiEndpoint + '/users/authenticate', { username: username, password: encPwd })
            .map(output => {
                // login successful if there's a jwt token in the response
                if (output && output.success && output.result) {
                    const userInfo = output.result;


                    if (output.token) {
                        // set token property
                        this.token = output.token;
                        userInfo.token = output.token;
                    }

                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    // console.log('authservice:login:' + JSON.stringify(output.result));
                    utils.encryptLocalStore('currentUser', JSON.stringify(userInfo));
                    // console.log(utils.encryptLocalStore('currentUser', JSON.stringify(output.result)));
                    return output;
                }

                return null;
            });
    }

    logout() {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }

    getUserInfo(): User {
        let localUser = {} as User;

        try {
            const decrypted = new Utilities().decryptLocalStore('currentUser');
            // console.log('authService:getUserInfo:' + isNull(decrypted) ? 'NA' : decrypted);
            localUser = JSON.parse(decrypted);
        } catch {
            localUser = null;
        }

        // console.log('authService:getUserInfo:' + localUser);
        return localUser;
    }
}
