import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { IntentFulfillmentModel } from '../../shared/models';

@Injectable()
export class IntentFulfillmentModelService {

    intentffmodelgraphServiceEndpoint: string;
    intentffmodelServiceEndpoint: string;
    functionmetadataEndpoint: string;
    nlpIntentffModelMappingEndpoint: string;

    constructor(
        private http: HttpClient

    ) {
        this.intentffmodelgraphServiceEndpoint = environment.apiEndpoint + '/intentfulfillmentgraph/';
        this.intentffmodelServiceEndpoint = environment.apiEndpoint + '/intentfulfillment/';
        this.functionmetadataEndpoint = environment.apiEndpoint + '/intentfulfillment/functions/';
        this.nlpIntentffModelMappingEndpoint = environment.apiEndpoint + '/nlpintentfulfillmentmapping/';
    }


    addIntentFulfillmentModel(nlpmodelid: String, nlpintentid: String, intentfulfillmentModel: IntentFulfillmentModel) {
        let modelgraph = {
            intentffmodelgraph: intentfulfillmentModel,
            nlpmodelid: nlpmodelid,
            nlpintentid: nlpintentid
        };
        return this.http.post(this.intentffmodelgraphServiceEndpoint, modelgraph);
    }

    updateIntentFulfillmentModel(nlpmodelid: String, nlpintentid: String, intentfulfillmentModel: IntentFulfillmentModel) {
        let modelgraph = {
            intentffmodelgraph: intentfulfillmentModel,
            nlpmodelid: nlpmodelid,
            nlpintentid: nlpintentid
        };
        return this.http.put(this.intentffmodelgraphServiceEndpoint, modelgraph);
    }

    deleteIntentFulfillmentModel(intentfulfillmentModelGraphid: String) {
        return this.http.delete(this.intentffmodelgraphServiceEndpoint + intentfulfillmentModelGraphid);
    }

    getIntentFulfillmentModelById(intentfulfillmentModelGraphid: String) {
        return this.http.get<any>(this.intentffmodelgraphServiceEndpoint + intentfulfillmentModelGraphid);
    }

    getIntentFulfillmentModelByName(intentfulfillmentModelname: String) {
        return this.http.get<any>(this.intentffmodelgraphServiceEndpoint + 'name/' + intentfulfillmentModelname);
    }

    getIntentffMappingByNLPModelIntent(nlpmodelid: String, nlpintentid: String) {
        return this.http.get<any>(this.nlpIntentffModelMappingEndpoint + 'nlp/' + nlpmodelid + '/' + nlpintentid);
    }

    getIntentFulfillmentTransformModelById(intentfulfillmentModelGraphid: String) {
        return this.http.get<any>(this.intentffmodelServiceEndpoint + 'model/graphid/' + intentfulfillmentModelGraphid);
    }


    getAllIntentFulfillmentModels() {
        return this.http.get<any>(this.intentffmodelgraphServiceEndpoint);
    }

    getAllIntentFulfillmentMappings() {
        return this.http.get<any>(this.nlpIntentffModelMappingEndpoint);
    }

    getAllFunctionMetaData() {
        return this.http.get<any>(this.functionmetadataEndpoint);
    }

}
