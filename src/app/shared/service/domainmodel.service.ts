import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { DomainModel } from '../models';

@Injectable()
export class DomainModelService {

    domainModelServiceEndpoint: string;

    constructor(
        private http: HttpClient

    ) {
        this.domainModelServiceEndpoint = environment.apiEndpoint + '/domainmodelgraph/';
    }


    addDomainModel(domainModel: DomainModel) {
        return this.http.post(this.domainModelServiceEndpoint, domainModel);
    }

    updateDomainModel(domainModel: DomainModel) {
        return this.http.put(this.domainModelServiceEndpoint, domainModel);
    }

    getDomainModelById(domainModelId: String) {
        return this.http.get<any>(this.domainModelServiceEndpoint + domainModelId);
    }

    getAllDomainModels() {
        return this.http.get<any>(this.domainModelServiceEndpoint);
    }


}
