export class ConnectorType {
  connectortypeid?: String;
  name?: String;
  category?: String = '';
  subcategory?: String = '';
  version?: String = '';
  provider?: String = '';
  sysdefined?: Boolean = false;
  createdby?: String;
  createdon?: Date;
  lastmodifiedby?: String;
  lastmodifiedon?: String;
}
