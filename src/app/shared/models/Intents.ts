export class IntentCategory {
    _Id: string;
    CategoryName: string;
    ModelName: string;
}

export class IntentCategoryUtterance {
    _id: string;
    intentCategoryName: string;
    UtteranceText: string;
    isNew: boolean;
    DateCreate: Date;
    DateModified: Date;
    isDeleted: boolean;
}
