export interface IFormData {
    ProjectForm: IProject;
    TextClassificationForm: ITextClassification;
    FulfillmentForm: IFulfillment
}

export interface IProject {
    projid: string;
    projname: string;
    projtype: string;
    projDesc: string;

    nlp_selected: boolean;

    nlp_conv_context_selected: boolean;
    nlp_sentiments_selected: boolean;
    nlp_feedback_selected: boolean;

    nlp_ui_selected: boolean;
    nlp_ui_type: string;

    nlp_ka_selected: boolean;
    nlp_ka_model: any;

    if_selected: boolean;
    // if_dom_selected: boolean;
    if_dom_models: any[];

    ec_selected: boolean;
}

export interface ITextClassification {
    modelname: string;
    intentname: string;
}

export interface IFulfillment {
    projname: string;
    modelname: string;
    modelid: string;
    intentname: string;
    intentid: string,
    intentcatname: string
    intentcatid: string
    utteranceid: string
}
