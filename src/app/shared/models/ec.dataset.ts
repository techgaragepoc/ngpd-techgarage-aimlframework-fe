import { Connector } from './ec.connector';

export class DataSet {
  datasetid?: String;
  name?: String;
  connectorid?: String;
  source?: String;
  ispublic?: Boolean = false;
  sysdefined?: Boolean = false;
  createdby?: String;
  createdon?: Date;
  lastmodifiedby?: String;
  lastmodifiedon?: String;

  sourceAsObject?: any;
  createdbyname?: String;
  connector?: Connector;
}
