export class SoapAPIConnection {
    baseurl?: String = '';
    proxy?: String = '';
    credentials?: Credentials;
}

export class Credentials {
    userid?: String = '';
    password?: String = '';
}

export class SqlServerConnection {
    server?: String = '';
    instancename?: String = '';
    database?: String = '';
    port?: String = '';
    user?: String = '';
    password?: String = '';
}

export class OracleConnection {
    connectstring?: String = '';
    user?: String = '';
    password?: String = '';
}

export class MongodbConnection {
    dbname?: String = '';
    url?: String = '';
    user?: String = '';
    password?: String = '';
}

export class HadoopConnection {
    baseurl?: String = '';
    headers?: String = '';
    proxy?: String = '';
    sessioninfo?: String = '';
    tokenkey?: String = '';
}
