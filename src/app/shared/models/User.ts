export interface User {
    _id: string;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    admin: boolean;
    displayname: string;
    email: string;
    avatar: string;
    forcePwdChange: boolean;
    modified: Date;
    token: string;
}
