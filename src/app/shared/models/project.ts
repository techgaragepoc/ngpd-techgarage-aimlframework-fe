import { ProjectOption } from './projectoption';

export interface Project {
    _id: string;
    name: string;
    description: string;
    category: string;
    options: UIOption[];
    updatedBy: string;
    modified: Date;
}

export interface UIOption {
    id: string;
    values: string[];
}
