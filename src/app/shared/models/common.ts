export interface Category {
    _id: String;
    name: String;
    updatedBy: String;
    modified: Date;
}

export interface Culture {
    _id: String;
    name: String;
    code: String;
    updatedBy: String;
    modified: Date;
}
