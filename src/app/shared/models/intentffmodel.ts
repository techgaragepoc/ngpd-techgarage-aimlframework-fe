export class IntentFulfillmentModel {

  constructor() {
  }

  _id?: String;
  name?: String;
  pos?: any = {};
  graphjson?: any = {};
  nodeproperties?: any = {};
  createdby?: String;
  createdon?: Date;
  lastmodifiedby?: String;
  lastmodifiedon?: String;

  transformgraphjson?: any = {};
  transformnodeproperties?: any = {};
}
