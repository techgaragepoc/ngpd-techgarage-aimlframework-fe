export interface ProjectOption {
    _id: string;
    name: string;
    datatype: string;
    multichoice: boolean;
    modified: Date;
    values: any[];
}
