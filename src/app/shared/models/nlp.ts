export class NlpLangaugeModel {
    name: String;
    cultureCode: String;
    description: String;
    endpointsHitCount: number;
    sysdefined: Boolean;
    shared: Boolean;
    istrained: Boolean;
    updatedBy: String;
    userName: String;
    publishdate: Date;
    modified: Date;
}
