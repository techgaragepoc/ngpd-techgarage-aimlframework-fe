export interface Model {
    _id: string;
    name: string;
    domain: string;
    body: Object;
    modified: Date;
    updatedBy: string;
    sysdefined: boolean;
}
