export class PaginationInfo {
    offset?: number;
    limit?: number;
    limits?: number[];
    totalCount?: number;
}
