import { ConnectorType } from './ec.connectortype';

export class TestConnection {
  isConnected?: Boolean = false;
  details?: String = '';
}

export class Connector {

  constructor() {
    this.testconnectionresult = new TestConnection();
  }

  connectorid?: String;
  name?: String;
  connectortypeid?: String = '';
  connectiondetail?:  String = '';
  sysdefined?: Boolean = false;
  createdby?: String;
  createdon?: Date;
  lastmodifiedby?: String;
  lastmodifiedon?: String;

  connectortype?: ConnectorType;
  connectiondetailAsObject?: any = {};
  testconnectionresult?: TestConnection;
}
