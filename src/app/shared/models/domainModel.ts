
export class DomainModel
{
 constructor() {
 }
  name?: String;
  nodes?: DomainModelNode[];
 createdby?: String;
 createdon?: Date;
 lastmodifiedby?: String;
 lastmodifiedon?: String;

}

export class DomainModelNode
 {
  constructor() {
  }

  _id?: String;
  name?: String;
  graphjson?: any = {};
  nodeProperties?: NodeAttributeModel[];
  createdby?: String;
  createdon?: Date;
  lastmodifiedby?: String;
  lastmodifiedon?: String;

}

//This needs to be added and corrected.
export class NodeAttributeModel {

  constructor() {
  }

  _id?: String;
  name?: String;
  description?:string;
  attributeType: string;  
  }
