import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { AuthenticationService } from '../service';
import { Observable } from 'rxjs/Observable';
import { Utilities } from '../utils';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
    constructor(public _authService: AuthenticationService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // console.log('called token interceptor');

        const currentUser = this._authService.getUserInfo();
        const isTokenValid = new Utilities().ValidateUserToken(currentUser);

        if (isTokenValid) {
            request = request.clone({
                setHeaders: { Authorization: `Bearer ${currentUser.token}` }
            });

            return next.handle(request);
        }

        return next.handle(request);
    }
}
