// import { Injectable } from '@angular/core';
// // import { Http, RequestOptions, RequestOptionsArgs, Response, Request, Headers, ConnectionBackend } from '@angular/http';
// import { Observable } from 'rxjs/Observable';
// import { environment } from '../../../environments/environment';
// import { HttpClient, HttpHeaders } from '@angular/common/http';

// @Injectable()
// export class AppHttpService extends HttpClient {
//   get(url: string, optionsArgs?: RequestOptionsArgs): Observable<Response> {
//     return super.get(url, this.InjectSecurityKey(optionsArgs)).catch(this.catchException(this));
//   }

//   put(url: string, body?: any, optionsArgs?: RequestOptionsArgs): Observable<Response> {
//     return super.put(url, body, this.InjectSecurityKey(optionsArgs)).catch(this.catchException(this));
//   }

//   post(url: string, body?: any, optionsArgs?: RequestOptionsArgs): Observable<Response> {
//     return super.post(url, body, this.InjectSecurityKey(optionsArgs)).catch(this.catchException(this));
//   }

//   delete(url: string, optionsArgs?: RequestOptionsArgs): Observable<Response> {
//     return super.delete(url, this.InjectSecurityKey(optionsArgs)).catch(this.catchException(this));
//   }

//   private catchException (self: AppHttpService) {
//     return (resp: Response) => {
//       console.log(resp);
//       if (resp.status === 401 || resp.status === 403) {
//         // user not authenticated
//         console.log(resp);
//       }
//       return Observable.throw(resp);
//     };
//   }

//   private InjectSecurityKey(optionArgs: RequestOptionsArgs): RequestOptionsArgs {
//     if (!optionArgs) {
//       optionArgs = { headers: new Headers() };
//     }

//     // add the required header key to every request
//     // optionArgs.headers.set('apikey', environment.apikey);
//     return optionArgs;
//   }
// }
