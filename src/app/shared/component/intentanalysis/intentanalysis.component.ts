import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { IFormData, ITextClassification } from '../../models/formdata';
import { SharedService } from '../../service';

@Component({
    selector: 'app-intentanalysis',
    moduleId: module.id.toString(),
    templateUrl: 'intentanalysis.component.html'
})
export class IntentAnalysisComponent implements OnInit, OnDestroy {
    modelExists: boolean;
    @Input() hideIntent: boolean;
    model: any = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private sharedService: SharedService) {
        this.route = route;
    }

    ngOnInit() {
        // this.model.foods = [{}];

        console.log('modelName');
        // var modelName = localStorage.getItem('modelName');
        const formData = (this.sharedService.FormData) ? this.sharedService.FormData.TextClassificationForm : null;
        const modelName = (formData) ? formData.modelname : null;

        console.log('modelName', modelName);
        this.modelExists = (modelName != null && modelName !== '');
    }
    public getIntents(): void {
        console.log('Utterance', this.model.inputIntent);
        const Utterance = this.model.inputIntent;
        localStorage.setItem('inputIntent', Utterance);
        this.router.navigate(['/intent']);
    }
    ngOnDestroy() {
    }
}
