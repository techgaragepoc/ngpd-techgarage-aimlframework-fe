import { Component, ViewChild, Input, OnChanges, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-highlighted-text',
    templateUrl: './highlighted-text.component.html',
    styleUrls: ['./highlighted-text.component.css']
})
export class HighlightedTextComponent implements OnChanges {
    // @Input() searchText: String;
    @Input() searchText: any[];
    @Input() fullText: String;
    // @ViewChild('myDiv') divText;//: HTMLDivElement;

    // public matched;
    // public unmatched;
    highlightedText;

    constructor(private sanitizer: DomSanitizer) { }
    ngOnChanges(changes) {
        this.match();
    }

    // match() {
    //     let final = '';
    //     // console.log('highlight match called');
    //     // this.matched = undefined;
    //     // this.unmatched = this.haystack;
    //     // console.log(this.needle, this.haystack);

    //     if (this.searchText && this.fullText) {
    //         const searchText = String(this.searchText);
    //         const fullText = String(this.fullText);

    //         let parts = fullText.split(' ');

    //         let highlighted = parts.map(part => {
    //             final = part;

    //             const startIndex = part.toLowerCase().indexOf(searchText.toLowerCase());

    //             if (startIndex !== -1) {
    //                 const endLength = searchText.length;

    //                 final = part.substr(0, startIndex) + '<mark>' + part.substr(startIndex, endLength) + '</mark>';

    //                 if (startIndex + endLength != part.length) {
    //                     final += part.substr(endLength);
    //                 }
    //             }
    //             // console.log(part);

    //             return final;
    //         });

    //         this.highlightedText = highlighted.join(' ');

    //         // const startIndex = haystack.toLowerCase().indexOf(needle.toLowerCase());

    //         // // console.log('startIndex', startIndex);
    //         // if (startIndex !== -1) {
    //         //     const endLength = needle.length;
    //         //     this.matched = haystack.substr(startIndex, endLength);
    //         //     this.unmatched = haystack.substr(needle.length);
    //         // }
    //     }

    match() {
        // let final = this.fullText;
        this.highlightedText = this.fullText;
        const parts = [];
        let startIndex = 0, endIndex = 0;

        if (this.searchText && this.searchText.length > 0 && this.fullText) {
            const fullText = String(this.fullText);

            this.searchText.sort((a, b) => {
                if (a.startIndex > b.startIndex) {
                    return 1;
                } else if (a.startIndex < b.startIndex) {
                    return -1;
                } else {
                    return 0;
                }
            })
                .forEach(x => {
                    if (x.startIndex >= startIndex) {
                        // console.log(
                        //     startIndex, x.startIndex, x.endIndex,
                        //     fullText.substr(startIndex, x.startIndex - startIndex),
                        //     fullText.substr(x.startIndex, x.endIndex));

                        parts.push(fullText.substr(startIndex, x.startIndex - startIndex));
                        parts.push(`<button mercer-button type="button" mercer-tooltip mercer-tooltip-text="`
                            + x.name + `" mercer-tooltip-position="top" mercer-tooltip-delay="100" size="sm" (click)="onClick('`
                            + x.name + `')">` + fullText.substr(x.startIndex, x.endIndex - x.startIndex) + `</button>`);
                    }

                    startIndex = x.endIndex;
                    endIndex = x.endIndex;

                    // console.log('x', x, startIndex, endIndex);
                });

            if (endIndex < fullText.length) {
                // console.log('final', fullText.substr(endIndex));
                parts.push(fullText.substr(endIndex));
            }

            // console.log(final);
            // this.highlightedText = this.sanitizer.bypassSecurityTrustHtml(final.toString());
            // this.highlightedText = final.toString();
            this.highlightedText = parts.join('');
        }
    }

    onClicked(entityName: string) {
        console.log('onClicked($event)', entityName);
    }
}
