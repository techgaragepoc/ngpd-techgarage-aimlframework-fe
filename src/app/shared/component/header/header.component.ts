import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';

import { environment } from '../../../../environments/environment';

import { User } from '../../../shared/models/User';
import { SharedService, AuthenticationService } from '../../service';

@Component({
  selector: 'app-header',
  moduleId: module.id.toString(),
  templateUrl: 'header.component.html'
})
export class HeaderComponent extends MosHeaderComponent implements OnDestroy {

  public mosConfig: MosConfigurationService;
  public loggedInUser: User;

  public userAvatar: string;
  public userFullName: string;
  public userEmail: string;

  userSubscription: Subscription;
  authSubscription: Subscription;

  private isAuthenticated: boolean;

  constructor
    (
    mosConfigParam: MosConfigurationService,
    private _router: Router,
    private _http: HttpClientModule,
    private _sharedService: SharedService,
    private _authService: AuthenticationService
    ) {
    super();
    this.mosConfig = mosConfigParam;
    this.subscribeUser();
    this.subscribeAuthFlag();
    // console.log('header constructor');
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.authSubscription.unsubscribe();
  }

  subscribeUser(): any {
    // console.log('subscribe User before data call');
    this.userSubscription = this._sharedService.getUserInfo().subscribe(
      res => {
        this.loggedInUser = res;
        // console.log('header:subscribeUser after data call:' + JSON.stringify(res));
        this.loadUserDetails(res);
      }
    );
  }

  subscribeAuthFlag(): any {
    this.authSubscription = this._sharedService.isAuthenticated().subscribe(res => {
      this.isAuthenticated = res;
      // console.log('header:subscribeAuthFlag:' + this.isAuthenticated);
    });
  }

  isUserAuthenticated(): boolean {
    return this.isAuthenticated;
  }

  doLogout() {
    this._authService.logout();
    this._sharedService.clearUserInfo();
    window.location.href = '/';
  }

  doLogin() {
    // const result = this._authService.signInUser().then((res) => {
    //   if (res) {//} && res.REDIRECTION_URL) {
    //     window.location.href = res.REDIRECTION_URL;
    //   }
    // });
  }

  private loadUserDetails(user: User) {
    this.userAvatar = null;
    this.userEmail = null;
    this.userFullName = null;
    this.isAuthenticated = false;

    if (user && user.email && user.email !== 'NA') {
      this.userAvatar = this.getUserAvatar(user.firstname, user.lastname);
      this.userEmail = user.email;
      this.userFullName = user.firstname + '' + (user.lastname ? ' ' + user.lastname : '');
      this.isAuthenticated = true;
      // console.log('loaduserdetails:' + user.EXT_USER);
    }
  }

  private getUserAvatar(firstname: string, lastname: string) {
    let avatar = '';

    if (firstname != null && firstname.length > 0) {
      avatar = firstname.substring(0, 1);
    }

    if (lastname != null && lastname.length > 0) {
      avatar += lastname.substring(0, 1);
    }

    return (avatar != null && avatar.length > 0) ? avatar : '?';
  }

  doChangePassword() {
    // console.log('_id:' + id);
    this._router.navigate(['/changepassword']);
  }

  getHeaderlinks() {
    if (this.loggedInUser && this.loggedInUser.admin === false) {
      return this._sharedService.SiteLinks.filter(x => x.isPublic && x.showInHeader);
    }

    return this._sharedService.SiteLinks.filter(x => x.showInHeader);
  }
}
