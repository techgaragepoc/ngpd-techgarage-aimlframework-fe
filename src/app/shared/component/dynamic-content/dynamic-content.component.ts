import {
    Component, OnChanges, OnInit,
    Input, Output, EventEmitter,
    Compiler, SimpleChanges, NgModule, NgModuleFactory
} from '@angular/core';

import { MercerOSModule } from 'merceros-ui-components';

@Component({
    selector: 'dynamic-content',
    template: `<ng-container *ngComponentOutlet="dynamicComponent;
                            ngModuleFactory: dynamicModule;"></ng-container>`
})
export class DynamicContentComponent implements OnInit, OnChanges {

    dynamicComponent: any;
    dynamicModule: NgModuleFactory<any>;

    @Input('htmlContent')
    html: string;
    // @Output() clicked = new EventEmitter<string>();

    constructor(private compiler: Compiler) {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['html'] && !changes['html'].isFirstChange()) {
            // console.log('html', this.html);

            this.dynamicComponent = this.createNewComponent(this.html);
            this.dynamicModule = this.compiler.compileModuleSync(this.createComponentModule(this.dynamicComponent));
        }
    }

    ngOnInit() {
        this.dynamicComponent = this.createNewComponent(this.html);
        this.dynamicModule = this.compiler.compileModuleSync(this.createComponentModule(this.dynamicComponent));
    }

    protected createComponentModule(componentType: any) {
        @NgModule({
            imports: [MercerOSModule],
            declarations: [componentType],
            entryComponents: [componentType]
        })
        class RuntimeComponentModule {
        }
        // a module for just this Type
        return RuntimeComponentModule;
    }

    protected createNewComponent(template: string) {
        // console.log(template);

        @Component({
            selector: 'dynamic-component',
            template: template ? template : '<div></div>'
        })
        class MyDynamicComponent {
            @Output() clicked = new EventEmitter<string>();

            onClick(name: string) {
                // console.log('name', name);
                this.clicked.next(name);
                // this.clicked.emit(name);
            }
        }

        return MyDynamicComponent;
    }

}
