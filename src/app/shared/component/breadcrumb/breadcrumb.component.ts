import { Subscription } from 'rxjs/Subscription';
import { Component, Inject, OnInit, OnDestroy, Input, OnChanges, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { MosConfigurationService } from 'merceros-ui-components/services';
// import { MosConfigurationService } from 'merceros-ui-components';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Jsonp } from '@angular/http';

import { IFormData, ITextClassification, User } from '../../models';
import { SharedService, AuthenticationService, AlertService, TextClassificationService, NlpService } from '../../service';
import { Utilities } from '../../utils';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: 'breadcrumb.component.html'
})
export class BreadCrumbComponent implements OnInit {
    public data = [];

    @Input() childUrl: string;
    // @Input() isTrained = false;
    @Input() showTrainingButton = false;
    @Output() clicked = new EventEmitter<any>();

    modelInfo: any;
    modelName: any;
    isRunning = false;
    isTrainingAllowed = false;
    trained = false;
    // showTrainButton = false;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private sharedService: SharedService,
        private alertService: AlertService,
        private authService: AuthenticationService,
        private nlpService: NlpService,
        private textClassificationService: TextClassificationService,
        private changeDetectorRef: ChangeDetectorRef) {
        // console.log('Url');
    }

    @Input() set isTrained(value) {
        this.trained = value;
    }

    get isTrained() {
        return this.trained;
    }

    ngOnInit(): void {
        const formData = (this.sharedService.FormData) ? this.sharedService.FormData.TextClassificationForm : null;
        const projForm = (this.sharedService.FormData) ? this.sharedService.FormData.ProjectForm : null;
        const fulfillmentForm = (this.sharedService.FormData) ? this.sharedService.FormData.FulfillmentForm : null;

        this.modelName = this.getParamValue([
            (formData) ? formData.modelname : null,
            (fulfillmentForm) ? fulfillmentForm.modelname : null,
            new Utilities().GetQueryStringParam(this.route, 'modelName'),
            new Utilities().GetQueryStringParam(this.route, 'nlpModel')
        ]);

        let intentName = this.getParamValue([
            (formData) ? formData.intentname : null,
            (fulfillmentForm) ? fulfillmentForm.intentcatname : null,
            new Utilities().GetQueryStringParam(this.route, 'categoryName')
        ]);

        let projectName = this.getParamValue([
            (projForm) ? projForm.projname : null,
            (fulfillmentForm) ? fulfillmentForm.projname : null,
            new Utilities().GetQueryStringParam(this.route, 'projectName')
        ]);

        // const projectId = (projForm) ? projForm.projid : null;
        // const projectType = (projForm) ? projForm.projtype : null;
        let modelId = (projForm) ? projForm.nlp_ka_model : null;
        const utteranceid = (fulfillmentForm) ? fulfillmentForm.utteranceid : null;

        // console.log(modelName == null || modelName.length === 0);
        // console.log(this.modelName, intentName, projectName, modelId);
        // console.log('this.showTrainingButton', this.showTrainingButton);

        const utils = new Utilities();

        if (this.modelName != null && this.modelName.length > 0) {
            this.nlpService.getByName(this.modelName).subscribe(model => {
                if (model) {
                    this.data = this.prepareData(model.name, projectName, intentName, (projectName !== null));
                    // this.isTrainingAllowed = this.showTrainingButton && utils.isEditingAllowed(model, this.authService.getUserInfo());
                    this.isTrainingAllowed = utils.isEditingAllowed(model, this.authService.getUserInfo());
                }
            });
        } else if (modelId != null) {
            this.nlpService.getById(modelId).subscribe(model => {
                if (model) {
                    this.modelName = model.name;
                    this.data = this.prepareData(model.name, projectName, intentName, true);
                    // this.isTrainingAllowed = this.showTrainingButton && utils.isEditingAllowed(model, this.authService.getUserInfo());
                    this.isTrainingAllowed = utils.isEditingAllowed(model, this.authService.getUserInfo());
                }
            });
        } else {
            this.data = this.prepareData(this.modelName, projectName, intentName);
        }

        // console.log('this.isTraini', this.isTrainingAllowed);
    }

    public trainModel(): void {
        const loggedUser = this.authService.getUserInfo();
        const userName = loggedUser ? loggedUser.username : 'NA';

        this.isRunning = true;
        // this.changeDetectorRef.detectChanges();
        this.alertService.clear();

        // console.log('username', userName);
        this.textClassificationService.trainEntireModel(this.modelName, userName).subscribe(data => {
            if (data && data.success) {
                this.alertService.success('Model trained successfully');
                this.isTrained = true;
                this.clicked.emit(data);
            }

            this.isRunning = false;
        },
            error => {
                this.isRunning = false;
                this.alertService.error(error.error.message);
            });

        // this.changeDetectorRef.detectChanges();
    }

    testModel() {
        this.router.navigate(['/intent'], { queryParams: { modelName: this.modelName } });
    }

    prepareData(modelName, projectName, intentName, fromProject = false) {
        const breadcrumbData = [];

        // console.log(modelName, projectName, fromProject);

        breadcrumbData.push({ label: '', icon: 'home', url: '/' });
        // this.breadcrumbData.push({ label: , url: '/projectcatalog' });
        breadcrumbData.push(
            {
                label: (projectName == null || projectName.length === 0) ? 'Project Name' : projectName,
                url: '/projectcatalog'
            });

        if (this.childUrl === 'nlp') {
            breadcrumbData.push({ label: 'Skill Area ', url: '/nlp' });
        } else if (this.childUrl === 'intentlist') {
            if (fromProject === false) {
                breadcrumbData.push({ label: 'Skill Area ', url: '/nlp' });
            }
            else {
                breadcrumbData.push({ label: modelName, url: '/nlp?modelName=' + modelName });
            }
            breadcrumbData.push({ label: 'Intent Listing', url: '/intentlist' });
        } else if (this.childUrl === 'intentfulfillment') {
            // if (fromProject === false) {
            //     breadcrumbData.push({ label: 'Skill Area ', url: '/nlp' });
            //     breadcrumbData.push({ label: 'Intent Fulfillment', url: '/intentfulfillment' });
            // } else {
            if (modelName == null) {
                breadcrumbData.push({ label: 'Skill Area ', url: '/nlp' });
            } else {
                breadcrumbData.push({ label: modelName, url: '/nlp?modelName=' + modelName });
            }

            if (intentName == null) {
                breadcrumbData.push({ label: 'Intent Fulfillment', url: '/intentfulfillment' });
            } else {
                breadcrumbData.push({ label: 'Intent Fulfillment (' + intentName + ')', url: '/intentfulfillment' });
            }
        } else if (this.childUrl === 'intentclassification') {
            breadcrumbData.push({ label: 'Skill Area ', url: '/nlp' });
            breadcrumbData.push({ label: modelName, url: '/intentclassification?modelName=' + modelName });
            breadcrumbData.push({ label: 'Intent Classification ', url: '/intentclassification' });
        } else if (this.childUrl === 'utterances') {
            // Don't show NLP link on the breadcrumb
            if (fromProject === false) {
                breadcrumbData.push({ label: 'Skill Area ', url: '/nlp' });
            }
            breadcrumbData.push({ label: modelName, url: '/nlp?modelName' + modelName });

            // breadcrumbData.push({ label: modelName, url: '/intentclassification?modelName=' + modelName });
            // this.breadcrumbData.push({ label: intentName, url: '/addutterance' });
        } else if (this.childUrl === 'intentimplementation') {
            if (modelName == null) {
                breadcrumbData.push({ label: 'Skill Area ', url: '/nlp' });
            } else {
                breadcrumbData.push({ label: modelName, url: '/nlp?modelName=' + modelName });
            }

            breadcrumbData.push({ label: 'Intent Fulfillment', url: '/intentfulfillment' });

            if (intentName == null) {
                breadcrumbData.push({ label: 'Intent Implementation', url: '/intentimplementation' });
            } else {
                breadcrumbData.push({ label: 'Intent Implementation (' + intentName + ')', url: '/intentimplementation' });
            }
        }

        // console.log(JSON.stringify(breadcrumbData));
        return JSON.parse(JSON.stringify(breadcrumbData));
    }

    getParamValue(possibleValues: string[]) {
        // setParamValue(value, possibleValues: string[]) {
        // if (value != null && value.length > 0) {
        //     return value;
        // }

        if (possibleValues == null || possibleValues.length === 0) {
            return null;
        }

        for (let index = 0; index < possibleValues.length; index++) {
            if (possibleValues[index] != null && possibleValues[index].length > 0) {
                // console.log('possibleValue', index);
                return possibleValues[index];
            }
        }

        return null;
    }
}
