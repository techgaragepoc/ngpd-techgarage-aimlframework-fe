import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy, OnInit, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';

import { environment } from '../../../../environments/environment';

import { User } from '../../../shared/models/User';
import { SharedService, AuthenticationService } from '../../service';

@Component({
    selector: 'mercer-page-menu',
    moduleId: module.id.toString(),
    templateUrl: './page-menu.component.html',
    styleUrls: ['./page-menu.component.scss']
})
export class PageMenuComponent implements OnInit, OnChanges, OnDestroy {

    @Input() currentroute: string;

    @Input() projectname: string;
    @Input() projectcategory: string;

    public menuoptions: any[] = [
        { route: 'projectcatalog', displayname: 'Project Catalog', class: 'other-page' },
        { route: 'modeleditor', displayname: 'NLP', class: 'other-page' },
        { route: 'intentlist', displayname: 'Intent Fulfillment', class: 'other-page' },
        { route: 'intentimplementation', displayname: 'Intent Implementation', class: 'other-page' }
    ];
    public pagetitle: String = '';

    constructor(private router: Router) { }

    ngOnInit() {
        this.setmenu();
    }

    ngOnChanges() {
        this.setmenu();
    }

    setmenu() {

        console.log('Menu - Input Values...', this.currentroute, this.projectname, this.projectcategory);
        if (this.currentroute !== 'undefined') {

            for (let i = 0; i < this.menuoptions.length; i++) {
                if (this.menuoptions[i].route.toString().toLowerCase() === this.currentroute.toLowerCase()) {                    
                    this.pagetitle = this.menuoptions[i].displayname;
                    this.menuoptions[i].class = 'current-page';
                    break;
                }
            }
        }
    }

    navigateto(routelink) {
        this.router.navigate([routelink]);
    }

    ngOnDestroy() { }
}
