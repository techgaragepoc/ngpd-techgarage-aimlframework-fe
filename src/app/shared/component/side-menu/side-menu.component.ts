import { HttpClientModule } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, ResponseContentType, Request, RequestMethod } from '@angular/http';
import { Component, Inject, OnDestroy, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { MosConfigurationService } from 'merceros-ui-components/services';
import { MosHeaderComponent } from 'merceros-ui-components/components';
// import { MosConfigurationService, MosHeaderComponent } from 'merceros-ui-components';

import { environment } from '../../../../environments/environment';

import { User } from '../../../shared/models/User';
import { SharedService, AuthenticationService } from '../../service';

@Component({
    selector: 'app-side-menu',
    moduleId: module.id.toString(),
    templateUrl: 'side-menu.component.html'
})
export class SideMenuComponent implements OnInit, OnDestroy {
    @Input() menulevel: string;

    public nlpmenu: Boolean = false;
    public intentffmenu: Boolean = false;
    public ecmenu: Boolean = false;
    public ecSelected: String = '';
    public intentffSelected: String = '';
    public nlpSelected: String = '';

    ngOnInit() {
        this.nlpmenu = false; this.nlpSelected = '';
        this.intentffmenu = false; this.intentffSelected = '';
        this.ecmenu = false; this.ecSelected = '';

        switch (this.menulevel.toString().toLowerCase()) {
            case 'nlp': this.nlpmenu = true; this.nlpSelected = 'selected'; break;
            case 'intentff': this.intentffmenu = true; this.intentffSelected = 'selected'; break;
            case 'ec': this.ecmenu = true; this.ecSelected = 'selected'; break;
        }
    }

    ngOnDestroy() { }
}
