import { Subscription } from 'rxjs/Subscription';
import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
// import { MosConfigurationService } from 'merceros-ui-components';
import { MosConfigurationService } from 'merceros-ui-components/services';
// import { AppHttpService } from '../../loader/apphttp.service';

@Component({
  selector: 'app-footer',
  moduleId: module.id.toString(),
  templateUrl: 'footer.component.html'
})
export class FooterComponent {
  public mosConfig: MosConfigurationService;
  constructor
    (
    @Inject(MosConfigurationService) private mosConfigParam,
    // private _http: AppHttpService,
  ) {
    this.mosConfig = mosConfigParam;
  }
}
