import { Component, AfterViewInit } from '@angular/core';

@Component({
    selector: 'app-modal',
    template: './modal.component.html'
})
export class ModalComponent implements AfterViewInit {
    visible = false;
    visibleAnimate = false;

    public show(): void {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true);
    }

    public hide(): void {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }

    ngAfterViewInit() {
        const script = document.createElement('script');
        script.type = 'text/javascript';

        let scriptText = '$(document).ready(function(){';
        scriptText += this.BuildTree('elementTree', 'treeSource');
        scriptText += this.BuildTree('bodyTree', 'bodySource');
        scriptText += '\n});';
        script.innerHTML = scriptText;

        // Append to the head
        document.head.appendChild(script);
    }

    BuildTree(targetElementNamePrefix: string, dataElementNamePrefix: string) {
        const script = `\n\n$('div[id*='` + targetElementNamePrefix + `']').each(function (i, el) {

            //Check if there is already a tree view created
                var treeElements = $(el).find('[class^='json-view']');

                //For some reason, the tree view is getting reloaded. So once the tree view is built, exit.
                if(treeElements !=null && treeElements.length > 0)
                {
                    return;
                }

                //Get hold of the hidden data
                var dataElement = $('span[id='` + dataElementNamePrefix + `' + i + '']');

                if(dataElement != null)
                {
                    try {
                        var treeData = JSON.parse(dataElement.text());
                        $(el).jsonView(treeData); //Format the data in a tree structure
                    }
                    catch(e)
                    {
                        console.error(e);
                    }
                }
            });`;

        return script;
    }
}
