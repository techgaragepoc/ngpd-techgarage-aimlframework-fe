import { APP_BASE_HREF } from '@angular/common';
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

// import { Ng2PaginationModule } from 'ng2-pagination';
// import { PaginatePipe, PaginationService } from 'ng2-pagination';

// import { TrackerService } from '../../services/tracker.service';
// import { GlobalProfileService } from '../../services/globalprofile.service';

import { AppComponent } from '../../../app.component';

// import { NavBarComponent } from '../navbar/navbar.component';
// import { JumbotronComponent } from '../jumbotron/jumbotron.component';
// import { SearchFilterComponent } from '../search/search-filter.component';
// import { ModalComponent } from '../modaldialog/modal.component';
// import { EventMasterComponent } from '../eventmaster/eventmaster.component';

import { AboutComponent } from '../pages/about.component';
import { HomeComponent } from '../pages/home.component';
import { ConfigureEventsComponent } from '../pages/configureevents.component';

import { Configuration } from '../../configuration/app.configuration';
import { routing } from '../../app.routing';

describe('Modal Component Test', () => {
    const modalView: ModalComponent;
    const trackerSvc: TrackerService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent, NavBarComponent, JumbotronComponent,
                SearchFilterComponent, ModalComponent, EventMasterComponent,
                ConfigureEventsComponent, AboutComponent, HomeComponent
            ],
            imports: [BrowserModule, FormsModule, HttpModule, JsonpModule, routing, Ng2PaginationModule],
            providers: [
                TrackerService,
                GlobalProfileService,
                { provide: APP_BASE_HREF, useValue: '/' },
                PaginationService,
                PaginatePipe,
                Configuration
            ]
        });
    });

    it('Should Initialize', () => {
        const appComponent = TestBed.createComponent(ModalComponent);
        appComponent.componentInstance.show();
        expect(appComponent.componentInstance.visible).toBeTruthy();

        appComponent.componentInstance.hide();
        expect(appComponent.componentInstance.visibleAnimate).toBeFalsy();
        // expect(appComponent.componentInstance instanceof ModalComponent).toBeTruthy('Should create ModalComponent');
    });
});
