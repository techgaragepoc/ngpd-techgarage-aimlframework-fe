import { environment as devEnvironment } from './environment.dev';

export const environment = {
  ...devEnvironment,
  production: true,
  hmr: false,
  apiEndpoint: 'http://inggned8ng4rg2.us.mrshmc.com:82/api',
};
