import * as path from 'path';

export const ROOT_PATH = path.join(__dirname, '../..');
export const SRC_PATH = path.join(ROOT_PATH, 'src');
export const ENV_FILE_PATH = path.join(SRC_PATH, 'environments/environment');

export function rootRelative(p) {
  return path.join(ROOT_PATH, p);
}

export function srcRelative(p) {
  return path.join(SRC_PATH, p);
}

