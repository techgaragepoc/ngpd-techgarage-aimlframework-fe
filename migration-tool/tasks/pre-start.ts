import * as gulp from 'gulp';
import * as readline from 'readline';

gulp.task('pre-start', (cb) => {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question(`You have started the MercerOS UI replatform migration tool which will transform your project
into an Angular CLI project and help you migrate to Angular 5 as well.

Before continuing, ensure that your MercerOS application satisfies the following criterion:

[ ] Ensure that you don't have any uncommitted changes.
[ ] Complete the "Before updating" steps here: https://angular-update-guide.firebaseapp.com/
[ ] Ensure that your application passes lint. Passing lint is now required.
[ ] Upgrade to the latest MercerOS components library
[ ] Ensure that you are using the correct import format for esnext

The migration script will perform the following changes/operations in the following order:

1. Remove files not relevant in a Angular CLI setup.
2. Modify your package.json file to only include your project specific dependencies.
3. Replace the tsconfig files in the project. If you had made custom modifications to them,
   you will have a chance to reconcile these changes.
4. Copy your current dev and prod environment configurations into the location where Angular CLI stores them.
5. Copy your current dev and prod meta data configuration into the .angular-cli.json file.
6. Replace the current index.html file with one that is compliant.
7. Remove src/custom-typings.d.ts and replace with src/typings.d.ts.
8. Introduce Angular CLI compliant hot module reload and refactor src/app/app.module.ts to remove old hot module reload strategy.
9. Replace src/config.ts to re-export the angular environment files.
10. Replace .gitignore file with Angular CLI compliant version.
11. Remove the config directory.
12. Remove the node_modules directory.

Things to do after:

[ ] Port copy configuration from config/webpack.common.js to .angular-cli.json
[ ] Replace conditional checks to ENV to check environment.production instead (Note: the migration
    tool will attempt to do this for you)
[ ] npm install and run test suite to verify the unit tests are passing with the new setup
[ ] Port custom changes to polyfills file to new polyfill files
[ ] Upgrade to ngrx 4.x: https://github.com/ngrx/platform/blob/master/MIGRATION.md
    ngrx 2.x does not support Angular 5: https://github.com/ngrx/platform/issues/538
[ ] Update your dependencies to Angular 5
[ ] Update your .gitignore with entries specific to your project

The following process will modify your MercerOS project, please save any work before continuing.
Do you wish to continue? [y/n] `, (answer) => {
    if (answer.match(/^y(es)?/i)) {
      cb();
    } else {
      return process.exit(0);
    }
    rl.close();
  });
});
