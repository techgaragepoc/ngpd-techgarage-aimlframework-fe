import * as gulp from 'gulp';
import { readFileSync } from 'fs';
import * as ts from 'typescript';
import { srcRelative, ENV_FILE_PATH, SRC_PATH } from '../helpers/root';
import { relative, dirname } from 'path';
import * as through from 'through2';
import { Buffer } from 'buffer';

function createRefactorStream(transformer: (sourceFile: ts.SourceFile, fileContents: string, filePath: string) => string) {
  const transform = function (file, encoding, callback) {
    file.contents = new Buffer(transformer(
      ts.createSourceFile(file.path, file.contents.toString(), ts.ScriptTarget.ES2015, true),
      file.contents.toString(),
      file.path
    ), encoding);
    callback(null, file);
  };

  return through.obj(transform);
}

function typesForNode(node: ts.Node) {
  for (const key in ts.SyntaxKind) {
    if (ts.SyntaxKind.hasOwnProperty(key)) {
      const element = ts.SyntaxKind[key] as any;
      if (node.kind === element) {
        return key;
      }
    }
  }
}

function getTypesForChildrenNodes(node: ts.Node) {
  const types = [];
  ts.forEachChild(node, (n: ts.Node) => {
    types.push(typesForNode(n));
  });
  return types;
}

function findTypesForString(node: ts.Node, str: string) {
  const text = node.getFullText();

  if (text.indexOf(str) > -1) {
    return [ typesForNode(node) ];
  }

  return [];
}

function hasStringAndType(node: ts.Node, str: string, type?: ts.SyntaxKind) {
  if (typeof type !== 'undefined' && node.kind !== type) {
    return false;
  }
  return node.getFullText().indexOf(str) > -1;
}

function getRelativePathToEnv(filePath: string) {
  return relative(dirname(filePath), ENV_FILE_PATH).replace(/\\/g, '/');
}

function refactorENV(sourceFile: ts.SourceFile, fileContents: string, filePath: string) {

  const environmentReplace = {
    'ENV !== \'production\'': '!environment.production',
    'ENV === \'production\'': 'environment.production',
    '\'production\' !== ENV': '!environment.production',
    '\'production\' === ENV': 'environment.production',
    'ENV !== \'development\'': 'environment.production',
    'ENV === \'development\'': '!environment.production',
    '\'development\' !== ENV': 'environment.production',
    '\'development\' === ENV': '!environment.production',
  };

  function envString() {
    return `import { environment } from '${getRelativePathToEnv(filePath)}';\n`;
  }

  function doRefactor(node: ts.Node) {
    ts.forEachChild(node, doRefactor);
    const text = node.getFullText();

    let foundOne = false;
    for (const replace in environmentReplace) {
      if (environmentReplace.hasOwnProperty(replace)) {
        const replacement = environmentReplace[replace];
        if (hasStringAndType(node, replace, ts.SyntaxKind.BinaryExpression)) {
          fileContents = fileContents.replace(replace, replacement);
          foundOne = true;
        }

      }
    }
    if (foundOne && fileContents.indexOf('environments/environment') < 0) {
      fileContents = envString() + fileContents;
    }
  }

  doRefactor(sourceFile);
  return fileContents;
}

function refactorHmr(sourceFile: ts.SourceFile, fileContents: string, filePath: string) {

  function doRefactor(node: ts.Node) {
    ts.forEachChild(node, doRefactor);
    const text = node.getFullText();
    if (node.kind === ts.SyntaxKind.SourceFile) {
      return;
    }

    // remove appRef
    if (hasStringAndType(node, 'appRef', ts.SyntaxKind.Parameter)) {
      fileContents = fileContents.replace(new RegExp(`${text}(\\s*,)?`), '');
    }

    if (hasStringAndType(node, 'ENV_PROVIDERS', ts.SyntaxKind.ImportDeclaration)) {
      fileContents = fileContents.replace(text, '');
    }

    // remove ENV_PROVIDERS from decorators
    if (hasStringAndType(node, 'ENV_PROVIDERS', ts.SyntaxKind.ArrayLiteralExpression)) {
      const newText = text
        .replace(new RegExp(`ENV_PROVIDERS.*?,?`), '')
        .split(/\r\n|\r|\n/)
        .filter((l) => !l.match(/^\s*$/))
        .join('\n');

      fileContents = fileContents.replace(text, newText);
    }

    // TODO: don't be lazy and put this in its own refactor function
    // remove NoContentComponent from routes file
    if (
      hasStringAndType(node, 'NoContentComponent', ts.SyntaxKind.ObjectLiteralExpression)
      && typesForNode(node.parent) === 'ArrayLiteralExpression'
    ) {
      fileContents = fileContents.replace(new RegExp(`${text.replace('*', '\\*')}.*?,?`), '');
    }

    // remove NoContentComponent from decorators
    if (hasStringAndType(node, 'NoContentComponent', ts.SyntaxKind.ArrayLiteralExpression)) {
      const newText = text
        .replace(new RegExp(`NoContentComponent.*?,?`), '')
        .split(/\r\n|\r|\n/)
        .filter((l) => !l.match(/^\s*$/))
        .join('\n');

      fileContents = fileContents.replace(text, newText);
    }

    if (
      // remove store type interface
      hasStringAndType(node, 'StoreType', ts.SyntaxKind.InterfaceDeclaration)
      // remove DEPLOY_URL
      || hasStringAndType(node, 'DEPLOY_URL', ts.SyntaxKind.ExpressionStatement)
      // remove styles.scss import
      || hasStringAndType(node, 'styles.scss', ts.SyntaxKind.ImportDeclaration)
      // remove ENV_PROVIDERS import from no longer used environments file
      || hasStringAndType(node, 'ENV_PROVIDERS', ts.SyntaxKind.ImportDeclaration)
      // remove NoContentComponent placeholder if it exists
      || hasStringAndType(node, 'NoContentComponent', ts.SyntaxKind.ImportDeclaration)
      // remove any instance of hmr
      || (node.kind === ts.SyntaxKind.MethodDeclaration || node.kind === ts.SyntaxKind.ImportDeclaration) && text.indexOf('hmr') > -1
    ) {
      fileContents = fileContents.replace(text, '');
    }
  }

  doRefactor(sourceFile);
  return fileContents;
}

gulp.task('refactor', ['refactor:hmr', 'refactor:environment', 'refactor:no-content']);

gulp.task('refactor:hmr', () => {
  return gulp.src(['app/app.module.ts'], { cwd: SRC_PATH, base: './' })
    .pipe(createRefactorStream(refactorHmr))
    .pipe(gulp.dest('./'));
});

gulp.task('refactor:environment', ['refactor:hmr'], () => {
  return gulp.src(['**/*.ts'], { cwd: SRC_PATH, base: './' })
  .pipe(createRefactorStream(refactorENV))
  .pipe(gulp.dest('./'));
});

gulp.task('refactor:no-content', (cb) => {
  return gulp.src(['app/app.routes.ts'], { cwd: SRC_PATH, base: './' })
    .pipe(createRefactorStream(refactorHmr))
    .pipe(gulp.dest('./'));
});
