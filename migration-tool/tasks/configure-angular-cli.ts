import * as gulp from 'gulp';
import * as fs from 'fs';
import * as mkdirp from 'mkdirp';
import { blueprintRelative } from '../helpers/blueprint';
import { rootRelative } from '../helpers/root';

import * as metaDevConfig from '../../config/environment/meta-dev';
import * as metaProdConfig from '../../config/environment/meta-prod';

/**
 * Adds in current configuration
 * @param {any} config
 * @param {string} envFile
 * @returns {string} updated env file contents
 */
function replaceVars(config, envFile) {
  const replacement = [];
  for (const key in config) {
    if (config.hasOwnProperty(key)) {
      const value = config[key];
      replacement.push(`  '${key}': '${value}',\n`);
    }
  }
  return envFile.replace('// REPLACE_VARS_PLACEHOLDER', replacement.join().trim());

}

gulp.task('configure-angular-cli:environment', (cb) => {
  const devConfig = require('../../config/environment/dev');
  const prodConfig = require('../../config/environment/prod');
  let devEnvironment = fs.readFileSync(blueprintRelative('src/environments/environment.dev.ts')).toString();
  let prodEnvironment = fs.readFileSync(blueprintRelative('src/environments/environment.prod.ts')).toString();

  devEnvironment = replaceVars(devConfig, devEnvironment);
  prodEnvironment = replaceVars(prodConfig, prodEnvironment);
  mkdirp(rootRelative('src/environments'), (err) => {
    if (err) {
      return cb(err);
    }
    fs.writeFileSync(rootRelative('src/environments/environment.dev.ts'), devEnvironment);
    fs.writeFileSync(rootRelative('src/environments/environment.prod.ts'), prodEnvironment);
    return cb();
  });
});

gulp.task('configure-angular-cli:meta', (cb) => {
  // open and replace contents of index.html placeholder
  const indexHtmlContents = fs.readFileSync(blueprintRelative('src/index.html')).toString();
  fs.writeFileSync(rootRelative('src/index.html'), indexHtmlContents.replace('PROJECT_TITLE', metaProdConfig.title));

  // copy deploy url
  const angularCliBaseConfig = require('../blueprint/.angular-cli.json');
  const appConfig = angularCliBaseConfig.apps[0];
  if (metaDevConfig.baseUrl !== '/') {
    appConfig.baseHref = metaDevConfig.baseUrl;
  }
  if (metaDevConfig.deployUrl !== '/') {
    appConfig.deployUrl = metaDevConfig.deployUrl;
  }

  fs.writeFileSync(rootRelative('.angular-cli.json'), JSON.stringify(angularCliBaseConfig, null, '  '));
  cb();
});

gulp.task('configure-angular-cli', ['configure-angular-cli:environment', 'configure-angular-cli:meta']);
